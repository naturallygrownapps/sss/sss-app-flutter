package de.naturallygrownapps.gallerygambling;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Window;
import android.view.WindowManager;
import io.flutter.BuildConfig;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "naturallygrownapps.de/sss";
  private static final String SAVED_INSTANCE_STATE_CONSUMED_INTENT = "SAVED_INSTANCE_STATE_CONSUMED_INTENT";

  private boolean hasConsumedIntent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

    // Solution from https://stackoverflow.com/a/25535915/
    // Save if we have consumed the intent to prevent it from processing with every start.
    if (savedInstanceState != null) {
      hasConsumedIntent = savedInstanceState.getBoolean(SAVED_INSTANCE_STATE_CONSUMED_INTENT);
    }

    if (!BuildConfig.DEBUG) {
      // Prevent screenshots on release builds.
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }

    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
      (call, result) -> {
        try {
          // Note: this method is invoked on the main thread.
          if (call.method.equals("getImage")) {
              result.success(getImage());
          }
          else if (call.method.equals("getAndClearIntent")) {
            // Only get the intent once to not process it multiple times.
            if (hasConsumedIntent) {
              result.success(null);
            }
            else {
              hasConsumedIntent = true;
              Intent intent = getIntent();
              if (intent == null) {
                result.success(null);
              } else {
                Map<String, Object> intentMap = new HashMap<>();
                intentMap.put("action", intent.getAction());
                Bundle extras = intent.getExtras();
                if (extras != null) {
                  Map<String, String> extrasMap = new HashMap<>();
                  intentMap.put("extras", extrasMap);
                  for (String key : extras.keySet()) {
                    // We only support strings for now as we don't need other types.
                    Object value = extras.get(key);
                    if (value instanceof String) {
                      extrasMap.put(key, (String)value);
                    }
                  }
                }
                result.success(intentMap);
              }
            }
          }
          else {
            result.notImplemented();
          }
        }
        catch (Exception ex) {
          result.error(ex.getClass().getName(), ex.getMessage(), null);
        }
      });
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putBoolean(SAVED_INSTANCE_STATE_CONSUMED_INTENT, hasConsumedIntent);
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    setIntent(intent);
    // We must process this intent.
    hasConsumedIntent = false;
  }

  byte[] getImage() throws IOException {
    ContentResolver cr = getContentResolver();

    Bitmap originalBitmap = null;
    Bitmap downscaledBitmap = null;
    // Try to get an image that was taken by the user's camera first.
    String imagePath = getRandomImagePath(cr, "camera");
//    if (imagePath == null || imagePath.isEmpty()) {
//      // If there is none, just try to pick any other image.
//      imagePath = getRandomImagePath(cr, null);
//    }

    // In case no image was found at all, we cannot continue.
    if (imagePath == null || imagePath.isEmpty())
      return null;

    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    options.inMutable = true;

    originalBitmap = BitmapFactory.decodeFile(imagePath, options);
    if (originalBitmap == null)
      throw new IOException("Could not decode file: " + imagePath);

    Bitmap bitmapToSave;

    int MaxLongSideSize = 1024;
    float width = originalBitmap.getWidth();
    float height = originalBitmap.getHeight();
    if (width > MaxLongSideSize || height > MaxLongSideSize) {
      float downscaleRatio = width > height ? width / MaxLongSideSize : height / MaxLongSideSize;
      downscaledBitmap = Bitmap.createScaledBitmap(originalBitmap, (int)(width / downscaleRatio), (int)(height / downscaleRatio), false);
      bitmapToSave = downscaledBitmap;
    }
    else {
      bitmapToSave = originalBitmap;
    }

    try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
      bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 70, stream);
      return stream.toByteArray();
    }
  }

  private String getRandomImagePath(ContentResolver cr, String bucketName) {
    String[] columns = new String[] {
      MediaStore.Images.ImageColumns.DATA
    };
    String selection = null;
    String[] selectionArgs = null;
    String sortOrder = "RANDOM() LIMIT 1";

    boolean withBucket = bucketName != null && bucketName.length() > 0;
    if (withBucket) {
      selection = MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME + " = ? COLLATE NOCASE";
      selectionArgs = new String[] { bucketName };
    }

    String imagePath = null;
    boolean tryExternalContentFirst = Math.random() > 0.5;

    try (Cursor cur = cr.query(
      tryExternalContentFirst ?
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI :
        MediaStore.Images.Media.INTERNAL_CONTENT_URI,
      columns,
      selection,
      selectionArgs,
      sortOrder)) {
      if (cur != null && cur.moveToFirst()) {
        imagePath = cur.getString(0);
      }
    }

    if (imagePath == null) {
      try (Cursor cur = cr.query(
        tryExternalContentFirst ?
            MediaStore.Images.Media.INTERNAL_CONTENT_URI :
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        columns,
        selection,
        selectionArgs,
        sortOrder)) {
        if (cur != null && cur.moveToFirst()) {
          imagePath = cur.getString(0);
        }
      }
    }

    return imagePath;
  }
}
