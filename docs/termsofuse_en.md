﻿# Gallery Gambling Terms of Use
These terms of use describe the rules and conditions for using the game application "Gallery Gambling" ("the app"). By creating a user account within the app and using it, you agree to these terms of use.

**Please carefully read the section "How the app works" in particular.**

If you don't agree with the concept of the game, please don't play it.

## User account
To use the app, you are required to create a user account. A user account consists of a freely choosable username and a secret password. By signing up for a user account, you agree to the present terms of use.

You can delete your user account by logging in to the app and selecting the option "Account Settings" -> "Delete Account" in the sidebar menu. This also deletes all of your active games.

I reserve the right to delete user accounts or deny the creation of accounts if required (for example in case of service abuse).

## How the app works
The app is about playing with another player for an image taken from your device. You select the people you play with.

**To enable this functionality, the app picks a random image from your device's camera folders whenever you start a new game or accept a game invitation. This image is sent in encrypted form to the Gallery Gambling servers.  
You cannot influence which image is being taken.**

The pick happens automatically when a game starts and is kept secret. That is, you do not know which image was chosen and for which image you play as your bet. You also cannot restrict the choice. The app can use every image that is found in camera folders on your device. There is no chance to change the image during a game.  
The image will be encrypted before being uploaded in a way that only you and the person you play with can potentially see it (especially me as the app's operator will not be able to see it).

When the game ends, the image of the game's loser will be shown to both players of the game. The image will only be displayed once and cannot be saved. Both player's images are deleted from the servers when the game is over and both players have seen the loser's image.

**By using the app, you agree to this behavior.**

## Data privacy and security
I highly respect the privacy and security of personal data. Thus, the app conforms to the rules of the General Data Protection Regulation (GDPR). For further information, please consult the app's data privacy statement (which can for example be found in the app's "About" page).
    

## Disclaimer
Due to end-to-end encryption of media being uploaded to the app's servers, I cannot control this data. I am not liable for any data uploaded to the app's servers by its users.

I do not guarantee the error-free functionality and availability of the app and its services.

If you have any questions, please contact me via email to [support@naturallygrownapps.de](mailto:support@naturallygrownapps.de).

These terms of use were last updated on 30 May 2020.
