﻿# Gallery Gambling Nutzungsvereinbarung
Diese Nutzungsvereinbarung beschreibt Bedingungen und Regelungen für die Nutzung der Spielanwendung "Gallery Gambling" (nachfolgend "die App"). Mit der Registrierung eines Benutzerkontos für die App und dessen Nutzung stimmst du dieser Nutzungsvereinbarung zu.

**Bitte lies insbesondere den Absatz "Funktionsweise der App" aufmerksam durch.**

Wenn du mit dem Konzept des Spiels nicht einverstanden bist, spiele es bitte nicht.

## Benutzerkonto
Um die App nutzen zu können, musst du ein Benutzerkonto anlegen. Das Benutzerkonto besteht aus einem frei wählbaren Benutzernamen und einem nur dir bekannten Passwort. Mit der Registrierung eines Benutzerkontos stimmst du dieser Nutzungsvereinbarung zu. 

Du kannst dein Benutzerkonto kündigen, indem du dich in die App einloggst und im Seitenmenü die Option "Kontoeinstellungen" -> "Konto löschen" wählst. Dadurch werden auch alle zugehörigen Spiele gelöscht.

Ich behalte mir das Recht vor, Benutzerkonten bei Bedarf zu kündigen, oder das Anlegen eines Kontos zu verweigern, beispielsweise bei Missbrauch des Dienstes.

## Funktionsweise der App
In der App spielst du mit einem von dir ausgewählten Mitspieler pro Spiel um ein Bild, welches sich auf deinem Gerät befindet.

**Dazu wählt die App bei Beginn eines Spiels oder wenn du eine Aufforderung zu einem Spiel akzeptierst ein zufälliges Bild aus, welches sich in einem Kameraordner
deines Geräts befindet und sendet es in verschlüsselter Form an die Gallery Gambling Server.  
Du hast keinen Einfluss auf diese Auswahl.**

Die Auswahl geschieht automatisch bei Beginn eines Spiels und ist verborgen. Das heißt, du siehst nicht, welches Bild als Einsatz hochgeladen wird und um welches Bild du somit spielst. Die Auswahl des Bildes lässt sich nicht einschränken. Es kann jedes Bild verwendet werden, welches sich in einem Kameraordner deines Geräts befindet. Es besteht keine Möglichkeit, das Bild im Spielverlauf zu ändern.  
Das hochgeladene Bild wird so verschlüsselt abgelegt, dass es außer dir und deinem Mitspieler niemand entschlüsseln und damit sehen kann (auch nicht ich als Betreiber der App).

Am Ende eines Spiels wird beiden Spielern das Bild des Verlierers angezeigt. Dieses Bild wird nicht gespeichert, sondern nur einmalig zur Anzeige gebracht. Das Bild des Gewinners wird nie gezeigt. Beide Bilder werden abschließend von den Servern gelöscht.  

**Durch die Nutzung der App erklärst du dich mit diesem Spielablauf einverstanden.**

## Datenschutz und Datensicherheit
Der Schutz und die Sicherheit von personenbezogenen Daten haben bei mir hohe Priorität. Daher halte ich mich strikt an die Regeln der Datenschutz-Grundverordnung (DSGVO). Ausführliche Informationen zum Datenschutz findest du in der Datenschutzerklärung (beispielsweise unter dem Menüpunkt "Über" in der App).
    
## Haftungsausschluss
Da alle von der App hochgeladenen Mediendaten vor dem Transfer auf dem Endgerät verschlüsselt werden, kann keine Kontrolle der Daten stattfinden. Ich als Betreiber der App hafte nicht für Inhalte, welche durch Nutzer auf die Server geladen werden.

Ich übernehme keine Garantie für korrekte Funktion und Verfügbarkeit des Dienstes.

Bei Fragen zur App wende dich bitte per E-Mail an [support@naturallygrownapps.de](mailto:support@naturallygrownapps.de) an mich.

Diese Nutzungsvereinbarung wurde aktualisiert am 30. Mai 2020.
