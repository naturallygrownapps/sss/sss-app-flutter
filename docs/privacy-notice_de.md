# Gallery Gambling Datenschutzerklärung
Gallery Gambling ("die App") ist eine kostenlose Spielanwendung von Rufus Linke ("ich"), bei der du gegen einen Mitspieler Stein Schere Papier um die Fotos auf deinem Smartphone spielst - das Interessante daran ist, dass die als Wetteinsatz eingesetzten Fotos automatisch und verdeckt von der App aus deinem Kameraspeicher gewählt werden.

Auch wenn sich das Spiel selbst darum dreht, persönliche Daten in Form von Fotos aufzudecken, berücksichtigt es selbstverständlich deine Datenschutzrechte und sammelt niemals andere Daten als solche, die für den Spielbetrieb notwendig sind.  

Kurz zusammengefasst:
* Fotos werden nur mit Personen geteilt, mit denen du spielst (und dann auch nur, wenn du verlierst)
* Fotos werden Ende-zu-Ende verschlüsselt (so, dass ich sie nicht einsehen kann)
* Fotos werden sofort von den Servern gelöscht, sobald ein Spiel für beide Spieler beendet ist
* Fotos werden nicht auf den Geräten der Spieler gespeichert und Screenshots werden unterbunden

Im Folgenden wird diese Datenschutzerklärung nun genauer erläutern, welche persönlichen Daten die App sammelt.

## Erfasste Daten
Dieser Abschnitt beschreibt die Daten, welche von der App beim Spielen erfasst werden. In jedem Unterabschnitt wird eine Kategorie benannt und dazu erklärt, wie und warum diese Daten erfasst werden.

### Benutzerregistrierung
#### Welche Daten?
* Ein Benutzername, den du dir bei der Registrierung frei aussuchst. Der Benutzername ist ein von dir gewähltes Pseudonym und muss keinen Bezug zu deiner Person beinhalten, es sei denn du entscheidest dich dazu einen Bezug herzustellen (z.B. indem du deinen echten Namen als Benutzernamen verwendest).
* Ein Passwort, das zum Einloggen mit deinem Benutzernamen dient.

#### Wie werden diese Daten erfasst?
Du gibst Benutzernamen und Passwort selbst ein, wenn du dich für die App registrierst.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
* Benutzername und Passwort werden benötigt, um dich beim Einloggen in die App zu authentifizieren.
* Dein Benutzername wird auch in der Spielersuche innerhalb der App angezeigt, wenn andere Spieler nach deinem Benutzernamen oder einem ähnlichen Benutzernamen suchen. Diese Funktion ist notwendig um Mitspieler zu finden.
* Dein Benutzername wird Personen angezeigt, die du zum Spiel einlädst, damit deine Mitspieler erfahren, wer sie eingeladen hat.

### Bilder aus dem Datenspeicher deines Smartphones
#### Welche Daten?
Bei jedem Spiel, das du startest, wird ein zufällig und verdeckt gewähltes Bild aus einem Kameraordner deines Smartphones erfasst.

#### Wie werden diese Daten erfasst?
Immer wenn du ein neues Spiel mit einem Mitspieler beginnst, wird Gallery Gambling auf den Datenspeicher deines Smartphones zugreifen und ein zufälliges Bild aus einem Ordner mit Kamerabildern auswählen.

Dazu benötigt die App die Erlaubnis, auf den Speicher deines Gerätes zuzugreifen.

Dieser Auswahlprozess geschieht automatisch und verdeckt, was bedeutet, dass du weder einen Einfluss auf die Auswahl des Bildes hast, noch sie unterbrechen kannst oder sehen kannst, welches Bild gewählt wurde.
Die gewählten Bilder werden vor der Übertragung auf die Gallery Gambling Server Ende-zu-Ende verschlüsselt, so dass nur die zwei am Spiel beteiligten Spieler die Möglichkeit haben, die Bilder zu entschlüsseln und somit zu sehen.
Insbesondere habe ich als Serverbetreiber so keine Möglichkeit zu sehen, welche Bilder verwendet wurden.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
Das Spiel um ein zufällig ausgewähltes geheimes Bild ist die Kernidee des Spiels. Immer dann, wenn ein neues Spiel startet, wird ein zufälliges Bild von jedem Spieler als Spieleinsatz hinterlegt. Wenn das Spiel beendet ist, wird das Bild des Verlierers beiden Spielern gezeigt. Das Bild des Gewinners wird nicht sichtbar.
Die beiden Bilder werden, nachdem beide Spieler das Bild des Verlierers gesehen haben, direkt gelöscht.
Die Bilder werden nur einmalig angezeigt und können nicht gespeichert werden.

### Server Logs
#### Welche Daten?
Es können Informationen über Anfragen an die Gallery Gambling Server protokolliert werden (z.B. deine IP-Adresse, zugegriffener URI, Rückgabestatuscode).

#### Wie werden diese Daten erfasst?
Diese Informationen werden automatisch protokolliert, wenn eine Anfrage auf den Servern der App eingeht.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
Server Logs werden aus technischen Gründen angelegt (wie Leistungsauswertung, Wartung und Fehlerbehebung), um den Dienst stabil zu halten und zu verbessern.

## Verwendete Dienste von Drittanbietern
### Firebase Cloud Messaging
Firebase Cloud Messaging ist ein von Google angebotener Dienst, welcher es ermöglicht Pushnachrichten an Endgeräte zu senden. Genaue Angaben zu Datenschutz und Sicherheit bei Firebase findest du unter https://policies.google.com/privacy, https://firebase.google.com/support/privacy/ und https://firebase.google.com/terms/data-processing-terms. 

#### Welche Daten?
Firebase Cloud Messaging generiert eine eindeutige Id, um deinem Endgerät Pushnachrichten zustellen zu können.

#### Wie werden diese Daten erfasst?
Jedes Mal, wenn du dich in die App einloggst, oder Firebase eine neue Id generiert, wird diese Id an die Gallery Gambling Server gesendet.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
Die App verwendet Pushnachrichten, um dir den Spielfortschritt anzuzeigen, ohne dass du diesen manuell abfragen musst.
Wenn beispielsweise dein Mitspieler einen Spielzug gemacht hat, wird die App darüber mithilfe einer Pushnachricht benachrichtigt und wird dir die passende Animation anzeigen (sofern die App läuft und du dich im Spiel befindest) oder dir eine Benachrichtigung auf deinem Smartphone anzeigen (wenn die App nicht oder im Hintergrund läuft).

### Firebase Crashlytics
Firebase Crashlytics ist ein von Google angebotener Dienst, welcher Fehlerberichte aus der App erstellt und übermittelt. Genaue Angaben zu Datenschutz und Sicherheit bei Firebase findest du unter https://policies.google.com/privacy, https://firebase.google.com/support/privacy/ and https://firebase.google.com/terms/crashlytics-app-distribution-data-processing-terms.

#### Welche Daten?
* Fehlerberichte (Fehlerbeschreibung und Stacktraces)
* Installations UUID
* Geräteinformationen wie beispielsweise Hersteller, Modell, Speicherkapazität, Betriebssystemversion.

#### Wie werden diese Daten erfasst?
Fehlerberichte werden automatisch erstellt und übermittelt, wenn die App unerwartete Fehler erfährt oder abstürzt.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
Absturz und Geräteinformationen werden benötigt, um Fehler innerhalb der App zu finden und zu beheben, um schließlich ein besseres Produkt anbieten zu können.

### Firebase Dynamic Links
Firebase Dynamic Links ist ein von Google angebotener Dienst, welcher es ermöglicht Links zu generieren, die direkt zu einem bestimmten Inhalt innerhalb einer App navigieren. Solche Links ermöglichen außerdem die Installation der App, sollte die App auf dem Gerät, welches den Link öffnet, noch nicht installiert sein. Genaue Angaben zu Datenschutz und Sicherheit bei Firebase findest du unter https://policies.google.com/privacy, https://firebase.google.com/support/privacy/ und https://firebase.google.com/terms/data-processing-terms.

#### Welche Daten?
* Dein Benutzername wird in die von Gallery Gambling generierten Einladungslinks kodiert.

#### Wie werden diese Daten erfasst?
Wenn du die Einladen-Funktion in Gallery Gambling benutzt, um jemanden von außerhalb der App zum Spiel einzuladen, wird ein Firebase Dynamic Link generiert, welcher deinen Benutzernamen enthält.

#### Warum werden diese Daten erfasst und wozu werden sie benutzt?
Der Benutzername ist in dem generierten Link enthalten, um dem eingeladenen Spieler mitzuteilen, durch wen die Einladung erfolgte.

## Wo werden meine Daten gespeichert?
Die Server von Gallery Gambling werden bei Contabo in Deutschland betrieben: https://contabo.com/?show=data_centers

Alle Datenübertragungen nutzen Transportverschlüsselung.

Zu Informationen der Speicherorte von genutzten Drittanbieterdiensten, schaue bitte in die oben verlinkten Dokumente zu Drittanbietern.

## Wie lange werden meine Daten gespeichert?
* Dein Benutzerkonto wird so lange erhalten bleiben, bis du dich entschließt, dass du es nicht mehr nutzen möchtest, und es löschst. Öffne dazu den Menüpunkt "Kontoeinstellungen"->"Konto löschen" in der App (wenn du eingeloggt bist).
* Spielbezogene Daten (z.B. die beiden Mitspieler, verschlüsselte Fotos, Spielzüge) werden nur für die Dauer eines Spiels gespeichert. Sobald ein Spiel beendet ist, werden alle zugehörigen Informationen gelöscht.
* Server Logs werden für eine Woche vorgehalten und danach gelöscht.
* Für Informationen über Speicherfristen von genutzten Drittanbieterdiensten, schaue bitte in die oben verlinkten Dokumente zu Drittanbietern.

## Welche Datenschutzrechte habe ich?
Ich möchte dir hiermit erläutern, welche Rechte in Bezug auf Datenschutz du hast. Jeder Nutzer der App hat folgende Rechte:

### Auskunftsrecht
Du hast das Recht von mir zu erfahren, welche Daten ich über dich gespeichert habe und eine Kopie der Daten anzufordern.

### Recht auf Berichtigung
Du hast das Recht zu verlangen, dass ich persönliche Daten über dich berichtige oder vervollständige, sollten sie unrichtig sein.

### Recht auf Löschung
Du hast das Recht zu verlangen, dass ich alle über dich erfassten Daten lösche.

### Recht auf Einschränkung der Verarbeitung
Du hast das Recht unter bestimmten Umständen zu verlangen, dass ich die Verarbeitung deiner Daten einschränke.

### Recht auf Datenübertragbarkeit
Du hast das Recht die über dich gespeicherten Daten in einem maschinenlesbaren Format zur Datenübertragung zu erhalten.

### Widerspruchsrecht
Du hast das Recht, der Nutzung deiner Daten zu widersprechen.

## Wie du mich erreichen kannst
Falls du Fragen zu dieser Datenschutzerklärung hast oder eines deiner Datenschutzrechte mir gegenüber ausüben möchtest, schreibe mir eine E-Mail an:

[support@naturallygrownapps.de](mailto:support@naturallygrownapps.de)

## Änderungen an dieser Datenschutzerklärung
Ich werde diese Datenschutzerklärung regelmäßig prüfen und etwaige Aktualisierungen in der App und auf der Webseite zur Verfügung stellen.

Diese Datenschutzerklärung wurde aktualisiert am 30. Mai 2020.
