# Gallery Gambling

## Snippets
Update json serialization functions:
`flutter packages pub run build_runner build --delete-conflicting-outputs`
--
Generate html documents from markdown with pandoc:
```@{"termsofuse_de"="Gallery Gambling Nutzungsvereinbarung"; "termsofuse_en"="Gallery Gambling Terms of Use"; "privacy-notice_de"="Gallery Gambling Datenschutzerklärung"; "privacy-notice_en"="Gallery Gambling Privacy Notice"}.GetEnumerator() | foreach { pandoc -c pandoc.css -s -f gfm -t html5 --metadata pagetitle="$($_.value)" -o ./docs/generated/$($_.key).html ./docs/$($_.key).md }; copy-item ./docs/generated/*.html ./assets/docs/legal/```
