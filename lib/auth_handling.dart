import 'dart:developer' as dev;

import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/exceptions/authentication_exception.dart';
import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

Future<bool> setupAuthHandling() async {
  final session = getService<Session>();

  bool sessionRestored = false;
  try {
    sessionRestored = await session.restore();
  }
  catch (e) {
    dev.log("Error restoring the session.", error: e);
  }

  if (!sessionRestored) {
    session.reset();
  }

  getService<AuthService>().startTokenUpdateHandling();

  var isLoggedIn = sessionRestored;
  session.authStateChanged = (bool isLoggedInNow) {
    if (isLoggedIn != isLoggedInNow) {
      isLoggedIn = isLoggedInNow;
      final ctx = util.getMainContext();
      final navigator = Navigator.of(ctx);
      // Clear navigation stack.
      while(navigator.canPop()) {
        navigator.pop();
      }
      // Go back to root page which will decide the displayed page
      // according to the auth state.
      navigator.pushReplacementNamed("/");
    }
  };

  session.reloginRequired = () async {
    final authSvc = getService<AuthService>();
    final user = session.currentPlayer?.username;
    final pw = session.currentPlayer?.password;

    var reloginSuccessful = false;
    if (util.isNotNullOrEmpty(user) && util.isNotNullOrEmpty(pw)) {
      try {
        await authSvc.login(user, pw, isRelogin: true);
        reloginSuccessful = true;
      }
      catch (e) {
        reloginSuccessful = false;
      }
    }

    if (!reloginSuccessful) {
      // Clear login state. This changes our state accordingly.
      await authSvc.logout();

      // Bail out of the current request.
      throw new AuthenticationException(reason: AuthenticationFailReason.AutomaticReloginFailed);
    }
  };

  return sessionRestored;
}
