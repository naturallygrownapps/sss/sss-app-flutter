import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SSSLocalizations {
  SSSLocalizations(this.locale);

  final Locale locale;

  static SSSLocalizations of(BuildContext context) {
    return Localizations.of<SSSLocalizations>(context, SSSLocalizations);
  }

  static bool isSupportedLanguage(Locale locale) {
    return ['en', 'de'].contains(locale.languageCode);
  }

  static bool hasLocalizationFor(String language, String resource) {
    return _localizedValues.containsKey(resource) &&
        _localizedValues[resource].containsKey(language);
  }

  String getLocalizationFor(String resource) {
    if (hasLocalizationFor(locale.languageCode, resource)) {
      return _localizedValues[resource][locale.languageCode];
    } else {
      return null;
    }
  }

  static Map<String, Map<String, String>> _localizedValues = {
    "common_cancel": {"en": "Cancel", "de": "Abbrechen"},
    "common_error": {"en": "Error", "de": "Fehler"},
    "common_ok": {"en": "Ok", "de": "Ok"},
    "common_quit": {"en": "Exit", "de": "Beenden"},
    "login_invalidlogin": {
      "en": "Invalid login credentials.",
      "de": "Falscher Benutzername und/oder Passwort."
    },
    "login_title": {"en": "Start", "de": "Start"},
    "common_username": {"en": "Name", "de": "Name"},
    "common_password": {"en": "Password", "de": "Passwort"},
    "login_submit": {"en": "Sign in", "de": "Einloggen"},
    "login_register": {"en": "Create a new account now", "de": "Jetzt neu anmelden"},
    "login_missing_username": {"de": "Gib deinen Benutzernamen ein.", "en": "Enter your username."},
    "login_missing_password": {"de": "Gib dein Passwort ein.", "en": "Enter your password."},
    "common_appname": {"en": "Gallery Gambling", "de": "Gallery Gambling"},
    "common_yes": {"en": "yes", "de": "Ja"},
    "common_no": {"en": "no", "de": "Nein"},
    "overview_title": {"en": "Your Games", "de": "Deine Spiele"},
    "overview_startnewgame": {"en": "New Game", "de": "Neues Spiel"},
    "overview_inviteplayer": {"en": "Invite a Friend", "de": "Jemanden Einladen"},
    "overview_inviteplayer_linkmessage": {
      "en": "Let's play for our photos on Gallery Gambling! My player name is {0}. \n{1}",
      "de": "Lass uns bei Gallery Gambling um unsere Fotos spielen! Mein Spielername ist {0}. \n{1}"
    },
    "ingame_title": {"en": "Game", "de": "Spiel"},
    "opponentselection_searchplayerplaceholder": {
      "en": "Search for a player here",
      "de": "Suche nach einem Mitspieler"
    },
    "opponentselection_title": {"en": "New Game", "de": "Neues Spiel"},
    "opponentselection_searchplayer": {"en": "Search for player", "de": "Spieler suchen"},
    "opponentselection_recent": {"en": "Recent opponents", "de": "Letzte Gegner"},
    "opponentselection_startgame": {
      "en": "Starting a new game with {0}",
      "de": "Starte neues Spiel gegen {0}"
    },
    "opponentselection_nosearchresults": {
      "en": "No players found.",
      "de": "Keine Spieler gefunden."
    },
    "opponentselection_pickplayerhint": {
      "en": "Select a player to play with:",
      "de": "Wähle aus gegen wen du spielen möchtest:"
    },
    "opponentselection_randomplayer": {
      "en": "Find a random player",
      "de": "Zufälligen Spieler finden"
    },
    "opponentselection_or": {"en": "or", "de": "oder"},
    "overview_toolbar_logout": {"en": "Logout", "de": "Ausloggen"},
    "overview_signingin": {"en": "Signing you in", "de": "Du wirst angemeldet"},
    "game_status_initiatedbyyou": {
      "en": "Waiting for opponent to accept",
      "de": "Warte auf Spielannahme"
    },
    "game_status_yourmoverequired": {"en": "It's your turn", "de": "Du bist am Zug"},
    "game_status_waitingforotherplayer": {
      "en": "Waiting for opponent's turn",
      "de": "Warte auf Gegner"
    },
    "game_status_rejected": {"en": "Opponent has rejected", "de": "Gegner hat abgelehnt"},
    "game_status_youwon": {"en": "Won", "de": "Gewonnen"},
    "game_status_youlost": {"en": "Lost", "de": "Verloren"},
    "game_status_acceptancerequired": {
      "en": "You have been challenged",
      "de": "Du wurdest herausgefordert"
    },
    "overview_toolbar_refresh": {"en": "Refresh", "de": "Aktualisieren"},
    "game_status_challengerbetrequired": {"en": "It's your turn", "de": "Du bist am Zug"},
    "localgameservice_nobetexception": {
      "en": "I couldn't find any images on your device that you could place as a bet.",
      "de": "Ich konnte keine Bilder auf deinem Gerät finden, die du als Einsatz setzen könntest."
    },
    "start_menu_title": {"en": "Gallery\nGambling", "de": "Gallery\nGambling"},
    "common_menu_item_about": {"en": "About", "de": "Über"},
    "about_title": {"en": "About", "de": "Über"},
    "start_menu_item_login": {"en": "Start", "de": "Start"},
    "main_menu_item_yourgames": {"en": "Your Games", "de": "Deine Spiele"},
    "main_menu_item_newgame": {"en": "New Game", "de": "Neues Spiel"},
    "main_menu_item_logout": {"en": "Logout", "de": "Ausloggen"},
    "main_menu_item_accountsettings": {"en": "Account Settings", "de": "Kontoeinstellungen"},
    "opponentselection_norecentplayers": {
      "en": "You did not play with anyone yet.",
      "de": "Du hast noch keine Spiele gemacht."
    },
    "opponentselection_selectopponent_confirm_title": {"en": "Start Game", "de": "Spiel starten"},
    "opponentselection_selectopponent_confirm_message": {
      "en": "Start a new game with {0}?",
      "de": "Willst du ein Spiel gegen {0} starten?"
    },
    "opponentselection_selectopponent_confirm_message_invited": {
      "en": "You were invited by {0}. Start a game with her/him?",
      "de": "Du wurdest von {0} eingeladen. Ein Spiel gegen sie/ihn starten?"
    },
    "main_menu_loggedinas": {"en": "Logged in as {0}", "de": "Angemeldet als {0}"},
    "overview_header_opponentname": {"en": "Opponent", "de": "Gegner"},
    "overview_header_gamestatus": {"en": "Status", "de": "Status"},
    "overview_header_score": {"en": "Score", "de": "Spielstand"},
    "common_exitconfirm_title": {"en": "Exit", "de": "Beenden"},
    "common_exitconfirm_message": {
      "en": "Do you want to exit Gallery Gambling?",
      "de": "Willst du Gallery Gambling verlassen?"
    },
    "game_draw": {"en": "Draw!", "de": "Unentschieden!"},
    "game_newround": {"en": "Next Turn", "de": "Nächster Zug"},
    "game_youwin": {"en": "Winner!", "de": "Gewonnen!"},
    "game_youlose": {"en": "Loser!", "de": "Verloren!"},
    "game_opponentlabel_gamenotstarted": {"en": "Playing with", "de": "Spiel gegen"},
    "game_ingame_status_acceptancerequired": {
      "en": "You have been challenged! Accept the game?",
      "de": "Du wurdest herausgefordert! Spiel annehmen?"
    },
    "game_ingame_status_initiatedbyyou": {
      "en": "Waiting for opponent to accept game",
      "de": "Warte auf Spielannahme"
    },
    "game_ingame_status_rejected": {"en": "The game was rejected", "de": "Spiel wurde abgelehnt"},
    "game_ingame_status_waitingforotherplayer": {
      "en": "Waiting for opponent's turn",
      "de": "Warte auf Zug des Gegners"
    },
    "game_ingame_status_youlost": {"en": "You lost!", "de": "Du hast verloren!"},
    "game_ingame_status_youwon": {"en": "You won!", "de": "Du hast gewonnen!"},
    "game_ingame_status_yourmoverequired": {"en": "Your move", "de": "Dein Zug"},
    "game_ingame_button_claimprize": {"en": "Get prize", "de": "Gewinn abholen"},
    "game_ingame_button_showloss": {"en": "Show my loss", "de": "Verlorenen Einsatz anzeigen"},
    "error_connectivity": {
      "en": "Connection failed. Please check your internet connection.",
      "de": "Verbindung fehlgeschlagen. Bitte überprüfe deine Internetverbindung."
    },
    "game_ingame_placingbet": {"en": "Uploading your bet", "de": "Dein Einsatz wird hochgeladen"},
    "common_delete": {"en": "Delete", "de": "Löschen"},
    "main_menu_item_feedback": {"en": "Send Feedback", "de": "Feedback abgeben"},
    "intro_title": {"en": "Tutorial", "de": "Tutorial"},
    "start_menu_item_tutorial": {"en": "Tutorial", "de": "Tutorial"},
    "intro_step1_1": {
      "en": "You play rock paper scissors with a friend for a special bet.",
      "de":
          "Du spielst mit einer Freundin oder einem Freund Schere Stein Papier.\nDas Besondere ist der Spieleinsatz, um den ihr spielt."
    },
    "intro_button_next": {"en": "Next", "de": "Weiter"},
    "intro_button_previous": {"en": "Back", "de": "Zurück"},
    "intro_step1_2": {
      "en":
          "The game randomly picks an image from your device.\n\nThe image is not visible and you cannot influence the pick.",
      "de":
          "Zu Beginn des Spiels wählt die App jeweils ein zufälliges Bild von euren Smartphones aus.\n\nIhr seht nicht, um welche Bilder es sich handelt und ihr habt keinen Einfluss auf die Auswahl."
    },
    "intro_step1_3": {
      "en":
          "Both images are securely hidden as the players' bet.\n\nNobody can see any of the images.",
      "de":
          "Die beiden Bilder werden versteckt und abgesichert als Einsatz hinterlegt.\n\nNiemand kann die Bilder sehen."
    },
    "intro_step2": {
      "en": "The game begins.\n\nThe first player to win 3 rounds wins the game.",
      "de":
          "Dann beginnt das Spiel.\n\nWer zuerst 3 Runden für sich entscheiden kann, hat gewonnen."
    },
    "intro_step3_1": {
      "en": "The loser's image is revealed so that both players can see it.",
      "de":
          "Das Bild des Verlierers wird aufgedeckt.\nBeide Spieler können es sich nun einmal ansehen."
    },
    "intro_step3_2": {
      "en":
          "The winner's image remains secret.\n\nThe images are deleted and are not stored after the game.",
      "de":
          "Der Gewinner hat Glück, sein Bild bleibt geheim.\n\nDie Bilder werden nach dem Spiel nicht gespeichert."
    },
    "intro_button_finish": {"en": "Let's play", "de": "Los geht's"},
    "about_author": {"en": "A game by Rufus Linke", "de": "Ein Spiel von Rufus Linke"},
    "about_button_dataprivacy": {"en": "Data Privacy Notice", "de": "Datenschutzerklärung"},
    "about_button_termsofuse": {"en": "Terms of Use", "de": "Nutzungsvereinbarung"},
    "about_button_licenses": {"en": "Licenses", "de": "Lizenzen"},
    "licenses_title": {"en": "Licenses", "de": "Lizenzen"},
    "license_single_title": {"en": "License", "de": "Lizenz"},
    "documents_title_general": {"en": "Document", "de": "Dokument"},
    "documents_title_termsofuse": {"en": "Terms of Use", "de": "Nutzungsvereinbarung"},
    "register_title": {"en": "Sign up", "de": "Neu anmelden"},
    "register_confirm_title": {"en": "Read this carefully", "de": "Bitte lies diese Information"},
    "register_confirm_text": {
      "en":
          "This app automatically picks randomly selected images from your device and shows them to the people you play with in case you lose a game.\nYou agree to this behavior.",
      "de":
          "Diese App wählt automatisch zufällige Bilder von deinem Gerät aus und zeigt sie deinen Mitspielern, falls du verlierst.\nDamit erklärst du dich einverstanden."
    },
    "register_terms_text": {
      // These messages must contain exactly two % chars which are replaced with "terms of use" and
      // "data privacy notice".
      "en": "By pressing the \"Sign up\" button below, you agree to the % and the %.",
      "de": "Durch Drücken des \"Neu anmelden\" Buttons akzeptierst du die % und die %."
    },
    "register_dataprivacy": {"en": "data privacy notice", "de": "Datenschutzerklärung"},
    "register_termsofuse": {"en": "terms of use", "de": "Nutzungsvereinbarung"},
    "register_submit": {"en": "Sign up", "de": "Neu anmelden"},
    "register_insufficientusername": {
      "en":
          "Please enter a username containing at least three letters or numbers (no special characters or whitespaces).",
      "de":
          "Bitte gib einen Namen mit mindestens drei Buchstaben oder Zahlen ein (keine Sonderzeichen, keine Leerzeichen)."
    },
    "register_insufficientpassword": {
      "en": "Please enter a password containing at least five characters.",
      "de": "Bitte gib ein Passwort mit mindestens fünf Zeichen ein."
    },
    "register_usernametaken": {
      "en": "This username is already taken. Try a different one.",
      "de": "Dieser Name ist bereits vergeben. Bitte gib einen anderen Namen ein."
    },
    "login_newhere": {"en": "Are you a new player?", "de": "Bist du neu hier?"},
    "login_alreadyregistered": {
      "en": "Already registered? Log in here:",
      "de": "Oder hast du dich schon angemeldet?"
    },
    "game_ingame_button_endgame": {"en": "Finish game", "de": "Spiel beenden"},
    "service_error_GAME_TOOMANYPARALLELGAMES": {
      "en": "You are already playing too many games with this opponent.",
      "de": "Du hast bereits zu viele laufende Spiele mit diesem Gegner."
    },
    "opponentselection_gameexists_title": {"en": "Game", "de": "Spiel"},
    "opponentselection_gameexists_message": {
      "en": "You are already in a game with {0}",
      "de": "Du spielst bereits mit {0}"
    },
    "opponentselection_gameexists_yes": {"en": "Go to game", "de": "Zum Spiel"},
    "opponentselection_gameexists_no": {"en": "Cancel", "de": "Abbrechen"},
    "notification_local_opengames": {
      "en": "You have unfinished games.",
      "de": "Du hast noch offene Spiele."
    },
    "permissions_confirm_title": {"en": "Permissions Required", "de": "Fehlende Berechtigungen"},
    "permissions_confirm_text": {
      "en": "Please grant the required permissions. You won't be able to play otherwise.",
      "de": "Bitte erlaube die erforderlichen Berechtigungen. Du kannst sonst nicht spielen."
    },
    "accountsettings_title": {"en": "Account Settings", "de": "Kontoeinstellungen"},
    "accountsettings_delete_account_description": {
      "en": "You can delete your account here. All active games will be cancelled.",
      "de": "Du kannst hier dein Konto löschen. Damit werden alle laufenden Spiele abgebrochen."
    },
    "accountsettings_delete_account_button": {"en": "Delete Account", "de": "Konto löschen"},
    "accountsettings_delete_account_confirm_text": {
      "en": "Do you really want to completely delete your account?",
      "de": "Willst du dein Konto wirklich löschen?"
    }
  };

  String get common_cancel => _localizedValues["common_cancel"][locale.languageCode];
  String get common_error => _localizedValues["common_error"][locale.languageCode];
  String get common_quit => _localizedValues["common_quit"][locale.languageCode];
  String get common_ok => _localizedValues["common_ok"][locale.languageCode];
  String get login_invalidlogin => _localizedValues["login_invalidlogin"][locale.languageCode];
  String get login_title => _localizedValues["login_title"][locale.languageCode];
  String get common_username => _localizedValues["common_username"][locale.languageCode];
  String get common_password => _localizedValues["common_password"][locale.languageCode];
  String get login_submit => _localizedValues["login_submit"][locale.languageCode];
  String get login_register => _localizedValues["login_register"][locale.languageCode];
  String get common_appname => _localizedValues["common_appname"][locale.languageCode];
  String get common_yes => _localizedValues["common_yes"][locale.languageCode];
  String get common_no => _localizedValues["common_no"][locale.languageCode];
  String get overview_title => _localizedValues["overview_title"][locale.languageCode];
  String get overview_startnewgame =>
      _localizedValues["overview_startnewgame"][locale.languageCode];
  String get overview_inviteplayer =>
      _localizedValues["overview_inviteplayer"][locale.languageCode];
  String get overview_inviteplayer_linkmessage =>
      _localizedValues["overview_inviteplayer_linkmessage"][locale.languageCode];
  String get ingame_title => _localizedValues["ingame_title"][locale.languageCode];
  String get opponentselection_searchplayerplaceholder =>
      _localizedValues["opponentselection_searchplayerplaceholder"][locale.languageCode];
  String get opponentselection_title =>
      _localizedValues["opponentselection_title"][locale.languageCode];
  String get opponentselection_searchplayer =>
      _localizedValues["opponentselection_searchplayer"][locale.languageCode];
  String get opponentselection_recent =>
      _localizedValues["opponentselection_recent"][locale.languageCode];
  String get opponentselection_startgame =>
      _localizedValues["opponentselection_startgame"][locale.languageCode];
  String get opponentselection_nosearchresults =>
      _localizedValues["opponentselection_nosearchresults"][locale.languageCode];
  String get overview_toolbar_logout =>
      _localizedValues["overview_toolbar_logout"][locale.languageCode];
  String get overview_signingin => _localizedValues["overview_signingin"][locale.languageCode];
  String get game_status_initiatedbyyou =>
      _localizedValues["game_status_initiatedbyyou"][locale.languageCode];
  String get game_status_yourmoverequired =>
      _localizedValues["game_status_yourmoverequired"][locale.languageCode];
  String get game_status_waitingforotherplayer =>
      _localizedValues["game_status_waitingforotherplayer"][locale.languageCode];
  String get game_status_rejected => _localizedValues["game_status_rejected"][locale.languageCode];
  String get game_status_youwon => _localizedValues["game_status_youwon"][locale.languageCode];
  String get game_status_youlost => _localizedValues["game_status_youlost"][locale.languageCode];
  String get game_status_acceptancerequired =>
      _localizedValues["game_status_acceptancerequired"][locale.languageCode];
  String get overview_toolbar_refresh =>
      _localizedValues["overview_toolbar_refresh"][locale.languageCode];
  String get game_status_challengerbetrequired =>
      _localizedValues["game_status_challengerbetrequired"][locale.languageCode];
  String get localgameservice_nobetexception =>
      _localizedValues["localgameservice_nobetexception"][locale.languageCode];
  String get start_menu_title => _localizedValues["start_menu_title"][locale.languageCode];
  String get common_menu_item_about =>
      _localizedValues["common_menu_item_about"][locale.languageCode];
  String get about_title => _localizedValues["about_title"][locale.languageCode];
  String get start_menu_item_login =>
      _localizedValues["start_menu_item_login"][locale.languageCode];
  String get main_menu_item_yourgames =>
      _localizedValues["main_menu_item_yourgames"][locale.languageCode];
  String get main_menu_item_newgame =>
      _localizedValues["main_menu_item_newgame"][locale.languageCode];
  String get main_menu_item_logout =>
      _localizedValues["main_menu_item_logout"][locale.languageCode];
  String get main_menu_item_accountsettings =>
      _localizedValues["main_menu_item_accountsettings"][locale.languageCode];
  String get opponentselection_norecentplayers =>
      _localizedValues["opponentselection_norecentplayers"][locale.languageCode];
  String get opponentselection_selectopponent_confirm_title =>
      _localizedValues["opponentselection_selectopponent_confirm_title"][locale.languageCode];
  String get opponentselection_selectopponent_confirm_message =>
      _localizedValues["opponentselection_selectopponent_confirm_message"][locale.languageCode];
  String get opponentselection_selectopponent_confirm_message_invited =>
      _localizedValues["opponentselection_selectopponent_confirm_message_invited"]
          [locale.languageCode];
  String get opponentselection_pickplayerhint =>
      _localizedValues["opponentselection_pickplayerhint"][locale.languageCode];
  String get main_menu_loggedinas => _localizedValues["main_menu_loggedinas"][locale.languageCode];
  String get overview_header_opponentname =>
      _localizedValues["overview_header_opponentname"][locale.languageCode];
  String get overview_header_gamestatus =>
      _localizedValues["overview_header_gamestatus"][locale.languageCode];
  String get overview_header_score =>
      _localizedValues["overview_header_score"][locale.languageCode];
  String get common_exitconfirm_title =>
      _localizedValues["common_exitconfirm_title"][locale.languageCode];
  String get common_exitconfirm_message =>
      _localizedValues["common_exitconfirm_message"][locale.languageCode];
  String get game_draw => _localizedValues["game_draw"][locale.languageCode];
  String get game_newround => _localizedValues["game_newround"][locale.languageCode];
  String get game_youwin => _localizedValues["game_youwin"][locale.languageCode];
  String get game_youlose => _localizedValues["game_youlose"][locale.languageCode];
  String get game_opponentlabel_gamenotstarted =>
      _localizedValues["game_opponentlabel_gamenotstarted"][locale.languageCode];
  String get game_ingame_status_acceptancerequired =>
      _localizedValues["game_ingame_status_acceptancerequired"][locale.languageCode];
  String get game_ingame_status_initiatedbyyou =>
      _localizedValues["game_ingame_status_initiatedbyyou"][locale.languageCode];
  String get game_ingame_status_rejected =>
      _localizedValues["game_ingame_status_rejected"][locale.languageCode];
  String get game_ingame_status_waitingforotherplayer =>
      _localizedValues["game_ingame_status_waitingforotherplayer"][locale.languageCode];
  String get game_ingame_status_youlost =>
      _localizedValues["game_ingame_status_youlost"][locale.languageCode];
  String get game_ingame_status_youwon =>
      _localizedValues["game_ingame_status_youwon"][locale.languageCode];
  String get game_ingame_status_yourmoverequired =>
      _localizedValues["game_ingame_status_yourmoverequired"][locale.languageCode];
  String get game_ingame_button_claimprize =>
      _localizedValues["game_ingame_button_claimprize"][locale.languageCode];
  String get game_ingame_button_showloss =>
      _localizedValues["game_ingame_button_showloss"][locale.languageCode];
  String get error_connectivity => _localizedValues["error_connectivity"][locale.languageCode];
  String get game_ingame_placingbet =>
      _localizedValues["game_ingame_placingbet"][locale.languageCode];
  String get common_delete => _localizedValues["common_delete"][locale.languageCode];
  String get main_menu_item_feedback =>
      _localizedValues["main_menu_item_feedback"][locale.languageCode];
  String get intro_title => _localizedValues["intro_title"][locale.languageCode];
  String get start_menu_item_tutorial =>
      _localizedValues["start_menu_item_tutorial"][locale.languageCode];
  String get intro_step1_1 => _localizedValues["intro_step1_1"][locale.languageCode];
  String get intro_button_next => _localizedValues["intro_button_next"][locale.languageCode];
  String get intro_button_previous =>
      _localizedValues["intro_button_previous"][locale.languageCode];
  String get intro_step1_2 => _localizedValues["intro_step1_2"][locale.languageCode];
  String get intro_step1_3 => _localizedValues["intro_step1_3"][locale.languageCode];
  String get intro_step2 => _localizedValues["intro_step2"][locale.languageCode];
  String get intro_step3_1 => _localizedValues["intro_step3_1"][locale.languageCode];
  String get intro_step3_2 => _localizedValues["intro_step3_2"][locale.languageCode];
  String get intro_button_finish => _localizedValues["intro_button_finish"][locale.languageCode];
  String get about_author => _localizedValues["about_author"][locale.languageCode];
  String get about_button_dataprivacy =>
      _localizedValues["about_button_dataprivacy"][locale.languageCode];
  String get about_button_termsofuse =>
      _localizedValues["about_button_termsofuse"][locale.languageCode];
  String get about_button_licenses =>
      _localizedValues["about_button_licenses"][locale.languageCode];
  String get licenses_title => _localizedValues["licenses_title"][locale.languageCode];
  String get license_single_title => _localizedValues["license_single_title"][locale.languageCode];
  String get documents_title_general =>
      _localizedValues["documents_title_general"][locale.languageCode];
  String get register_title => _localizedValues["register_title"][locale.languageCode];
  String get register_confirm_text =>
      _localizedValues["register_confirm_text"][locale.languageCode];
  String get register_confirm_title =>
      _localizedValues["register_confirm_title"][locale.languageCode];
  String get register_terms_text => _localizedValues["register_terms_text"][locale.languageCode];
  String get register_dataprivacy => _localizedValues["register_dataprivacy"][locale.languageCode];
  String get register_termsofuse => _localizedValues["register_termsofuse"][locale.languageCode];
  String get register_submit => _localizedValues["register_submit"][locale.languageCode];
  String get register_insufficientusername =>
      _localizedValues["register_insufficientusername"][locale.languageCode];
  String get register_insufficientpassword =>
      _localizedValues["register_insufficientpassword"][locale.languageCode];
  String get register_usernametaken =>
      _localizedValues["register_usernametaken"][locale.languageCode];
  String get login_newhere => _localizedValues["login_newhere"][locale.languageCode];
  String get login_alreadyregistered =>
      _localizedValues["login_alreadyregistered"][locale.languageCode];
  String get game_ingame_button_endgame =>
      _localizedValues["game_ingame_button_endgame"][locale.languageCode];
  String get service_error_GAME_TOOMANYPARALLELGAMES =>
      _localizedValues["service_error_GAME_TOOMANYPARALLELGAMES"][locale.languageCode];
  String get opponentselection_gameexists_title =>
      _localizedValues["opponentselection_gameexists_title"][locale.languageCode];
  String get opponentselection_gameexists_message =>
      _localizedValues["opponentselection_gameexists_message"][locale.languageCode];
  String get opponentselection_gameexists_yes =>
      _localizedValues["opponentselection_gameexists_yes"][locale.languageCode];
  String get opponentselection_gameexists_no =>
      _localizedValues["opponentselection_gameexists_no"][locale.languageCode];
  String get opponentselection_randomplayer =>
      _localizedValues["opponentselection_randomplayer"][locale.languageCode];
  String get opponentselection_or => _localizedValues["opponentselection_or"][locale.languageCode];
  String get notification_local_opengames =>
      _localizedValues["notification_local_opengames"][locale.languageCode];
  String get login_missing_username =>
      _localizedValues["login_missing_username"][locale.languageCode];
  String get login_missing_password =>
      _localizedValues["login_missing_password"][locale.languageCode];
  String get permissions_confirm_title =>
      _localizedValues["permissions_confirm_title"][locale.languageCode];
  String get permissions_confirm_text =>
      _localizedValues["permissions_confirm_text"][locale.languageCode];
  String get accountsettings_title =>
      _localizedValues["accountsettings_title"][locale.languageCode];
  String get accountsettings_delete_account_description =>
      _localizedValues["accountsettings_delete_account_description"][locale.languageCode];
  String get accountsettings_delete_account_button =>
      _localizedValues["accountsettings_delete_account_button"][locale.languageCode];
  String get settings_delete_account_confirm_text =>
      _localizedValues["accountsettings_delete_account_confirm_text"][locale.languageCode];
}

class SSSLocalizationsDelegate extends LocalizationsDelegate<SSSLocalizations> {
  const SSSLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => SSSLocalizations.isSupportedLanguage(locale);

  @override
  Future<SSSLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<SSSLocalizations>(SSSLocalizations(locale));
  }

  @override
  bool shouldReload(SSSLocalizationsDelegate old) => false;
}
