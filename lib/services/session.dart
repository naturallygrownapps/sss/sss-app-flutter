import 'dart:typed_data';

import 'package:gallery_gambling/services/auth_helper.dart';
import 'package:gallery_gambling/services/service_client.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:gallery_gambling/util_functions.dart';

class Session {
  final ServiceClient _serviceClient;
  final Settings _settings;
  final AuthHelper _authHelper;

  PlayerCredentials _currentPlayer;
  PlayerCredentials get currentPlayer => _currentPlayer;
  set currentPlayer(PlayerCredentials p) {
    _currentPlayer = p;
    _authHelper.serializePlayerCredentials(p).then(
      (serialized) => _settings.currentPlayerState = serialized
    );
  }

  bool hasCheckedAuthState = false;
  bool _isLoggedIn = false;
  bool get isLoggedIn => _isLoggedIn;
  void Function(bool isLoggedIn) authStateChanged;
  Future Function() reloginRequired;

  Session(this._serviceClient, this._settings, this._authHelper);

  Future onLogin(String authToken, String username, String password, Uint8List pk) async {
    _onLogin(authToken, await _authHelper.initExistingPlayer(username, password, pk));
  }

  void _onLogin(String authToken, PlayerCredentials playerCredentials) {
    try {
      _serviceClient.setAuthorizationToken(authToken);
      _settings.authToken = authToken;
      currentPlayer = playerCredentials;
      _notifyAuthStateChanged(true);
    }
    catch (e) {
      _settings.authToken = "";
      currentPlayer = null;
      _notifyAuthStateChanged(false);
      rethrow;
    }
  }

  void reset() {
    hasCheckedAuthState = false;
    _serviceClient.setAuthorizationToken(null);
    _settings.authToken = null;
    currentPlayer = null;
    _notifyAuthStateChanged(false);
  }

  Future<bool> restore() async {
    // Global login/authentication handler.
    _serviceClient.setAuthorizationMissingHandler(() async  {
      _serviceClient.setAuthorizationToken(null);
      _settings.authToken = "";
      final reloginHandler = this.reloginRequired;
      if (reloginHandler != null) {
        await reloginHandler();
      }
    });

    final curPlayerState = _settings.currentPlayerState;
    final hasStoredSession = isNotNullOrEmpty(_settings.authToken) && isNotNullOrEmpty(curPlayerState);
    if (hasStoredSession) {
      final playerCredentials = await _authHelper.deserializePlayerCredentials(curPlayerState);

      final username = playerCredentials.username;
      final pw = playerCredentials.password;
      final key = playerCredentials.playerKey;
      if (username == null || username.isEmpty || pw == null || pw.isEmpty || key == null) {
        _notifyAuthStateChanged(false);
        return false;
      }
      await this._onLogin(_settings.authToken, playerCredentials);
    }

    return hasStoredSession;
  }

  void _notifyAuthStateChanged(bool isLoggedInNow) {
    _isLoggedIn = isLoggedInNow;
    final authStateChangedHandler = authStateChanged;
    if (authStateChangedHandler != null) {
      authStateChangedHandler(isLoggedInNow);
    }
  }
}
