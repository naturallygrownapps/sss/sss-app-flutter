import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/services/local_game_service.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/util_functions.dart';

const notificationInterval = const Duration(hours: 36);

class LocalNotificationsService {
  final LocalGameService _localGameService;
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  LocalNotificationsService(this._localGameService) {

  }

  Future init() {
    final initializationSettings = new InitializationSettings(
      AndroidInitializationSettings('ic_stat_name'),
      IOSInitializationSettings()
    );

    return _flutterLocalNotificationsPlugin.initialize(
      initializationSettings
    );
  }

  Future disableGameReminderNotification() {
    return _flutterLocalNotificationsPlugin.cancelAll();
  }

  Future enableGameReminderNotification() async {
    final hasOpenGames = _localGameService.getLocalGames().any((g) =>
      g?.state == GameState.AcceptanceRequired ||
      g?.state == GameState.ChallengerBetRequired ||
      g?.state == GameState.YourMoveRequired
    );
    if (!hasOpenGames) {
      return;
    }

    final scheduledNotificationDateTime = DateTime.now().add(
      notificationInterval);
    final androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'de.naturallygrownapps.gallerygambling.reminders',
      'Gallery Gambling Reminders',
      'Reminds you of pending games.',
      importance: Importance.Low,
      priority: Priority.Low
    );
    final iOSPlatformChannelSpecifics = IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = new NotificationDetails(
      androidPlatformChannelSpecifics,
      iOSPlatformChannelSpecifics
    );

    final loc = SSSLocalizations.of(getMainContext());
    await _flutterLocalNotificationsPlugin.schedule(
      0,
      'Gallery Gambling',
      loc.notification_local_opengames,
      scheduledNotificationDateTime,
      platformChannelSpecifics
    );
  }
}