import 'package:gallery_gambling/services/service_client.dart';
import 'package:gallery_gambling/shared/dto/user_entity_dto.dart';

class PlayersService {
  final ServiceClient _serviceClient;

  PlayersService(this._serviceClient);

  Future<UserEntityDto> getSelf() {
    return _serviceClient.get<UserEntityDto>("/player/self", UserEntityDto.fromJson);
  }

  Future<List<UserEntityDto>> findPlayers(String username) async {
    final resultWithStatusCode = await _serviceClient.getWithStatusCodeReturnList<UserEntityDto>(
        "/player/find/$username", UserEntityDto.fromJson);
    return resultWithStatusCode.result;
  }

  Future<UserEntityDto> findRandomPlayer() {
    return _serviceClient.get<UserEntityDto>("/player/findrandom", UserEntityDto.fromJson);
  }
}
