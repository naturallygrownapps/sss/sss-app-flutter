import 'dart:async';

import 'dart:collection';

import 'package:gallery_gambling/progress.dart';
import 'package:rxdart/rxdart.dart';

enum LoadingState {
  NotLoading,
  BlockUiOnly,
  Spinner
}

class LoadingTracker {
  PublishSubject<bool> _isLoadingController = PublishSubject();

  Stream<LoadingState> _loadingState;
  Stream<LoadingState> get loadingState => _loadingState;

  BehaviorSubject<String> _busyMessageController = BehaviorSubject();
  Stream<String> get busyMessage => _busyMessageController.stream.distinct();

  StreamController<DoubleValueReportingProgress> _progressController = StreamController.broadcast();
  Stream<DoubleValueReportingProgress> get progress => _progressController.stream;
  
  int _waiting = 0;
  _MessageManager _msgManager;

  LoadingTracker() {
    _msgManager = new _MessageManager(this);

    final loadingStateObservable = _isLoadingController
      .distinct()
      .switchMap((isLoading) {
        if (!isLoading) {
          // Display as not loading when the loading flag is false.
          return Observable<LoadingState>.never().startWith(LoadingState.NotLoading);
        }
        else {
          // Block the ui immediately, but only show a spinner after 100ms when we're loading.
          return Observable.concat([
            Observable.just(LoadingState.BlockUiOnly),
            Observable.timer(
              LoadingState.Spinner, const Duration(milliseconds: 250)
            ),
            // Do not end the sequence. When isLoading changes, we subscribe to a new one.
            Observable<LoadingState>.never()
          ]);
        }
      })
      .distinct()
      .publishValueSeeded(LoadingState.NotLoading);

    _loadingState = loadingStateObservable;
    // Must connect immediately to not miss any isLoading changes as long as we don't have
    // a subscription.
    loadingStateObservable.connect();
  }

  Future<T> track<T>(Future<T> future, {String message: null}) {
    _registerTrackedAsyncAction();
    _msgManager.enqueueMessage(message, future);
    return future.whenComplete(_onAsyncActionCompleted);
  }

  Stream<T> trackStream<T>(Stream<T> stream, {String message: null}) {
    _registerTrackedAsyncAction();
    _msgManager.enqueueMessage(message, stream.last);
    stream.listen(null, onDone: _onAsyncActionCompleted);
    return stream;
  }

  Progress<double> beginProgress() {
    final p = DoubleValueReportingProgress(() {

    });
    _progressController.add(p);
    return p;
  }

  _registerTrackedAsyncAction() {
      _waiting++;
      _isLoadingController.add(_waiting != 0);
  }
  
  _onAsyncActionCompleted() {
    _waiting--;
    // Should not happen.
    if (_waiting < 0)
        _waiting = 0;
    _isLoadingController.add(_waiting != 0);
  }
}

class _QueuedMessage {
  String message;
  bool isActive = true;

  _QueuedMessage(this.message) {
  }

  finish() {
    isActive = false;
  }
}

class _MessageManager {
  LoadingTracker _tracker;
  Queue<_QueuedMessage> _messages = Queue<_QueuedMessage>();
  _QueuedMessage _currentMessage;

  _MessageManager(this._tracker) {
  }

  enqueueMessage(String message, Future f) {
    if (message == null || message.isEmpty) {
      return;
    }

    final msg = new _QueuedMessage(message);
    _onMessageStarted(msg);
    f.whenComplete(() => _onMessageFinished(msg));
  }

  _onMessageStarted(_QueuedMessage msg) {
    if (_messages.isEmpty) {
      _currentMessage = msg;
      _tracker._busyMessageController.add(msg.message);
    }
    else {
      _messages.add(msg);
    }
  }

  _onMessageFinished(_QueuedMessage msg) {
    msg.finish();
    // If the current message finished, check if there are other messages waiting.
    if (msg == _currentMessage) {
      // msg is not the current message anymore.
      _currentMessage = null;
      // Clear the queue until finding an active message.
      while (_messages.isNotEmpty && _currentMessage == null) {
        final next = _messages.removeFirst();
        if (next.isActive) {
          _currentMessage = next;
        }
      }
    }
    
    _tracker._busyMessageController.add(_currentMessage?.message);
  }
}