import 'dart:typed_data';
import 'package:gallery_gambling/native_interop.dart';

class BetService {
  Future<Uint8List> getImage() => nativeMethods.invokeMethod<Uint8List>("getImage");
}
