import 'dart:convert';
import 'dart:typed_data';

import 'package:gallery_gambling/services/crypt_helper.dart';

class PlayerCredentials {
  String username;
  String password;
  PlayerKey playerKey;

  PlayerCredentials(this.username, this.password, this.playerKey);
}

class AuthHelper {
  CryptHelper _cryptHelper;

  AuthHelper(this._cryptHelper);

  Future<PlayerCredentials> initNewPlayer(
      String username, String password) async {
    var playerKey = await _cryptHelper.createPlayerKey();
    return new PlayerCredentials(username, password, playerKey);
  }

  Future<PlayerCredentials> initExistingPlayer(
      String username, String password, Uint8List pk) async {
    var playerKey = await _cryptHelper.parsePlayerKey(password, pk);
    return new PlayerCredentials(username, password, playerKey);
  }

  Future<String> serializePlayerCredentials(
      PlayerCredentials playerCredentials) async {
    if (playerCredentials == null) {
      return "";
    } else {
      var playerKey = await _cryptHelper.exportPlayerKey(
          playerCredentials.password, playerCredentials.playerKey);
      return jsonEncode({
        "username": playerCredentials.username,
        "password": playerCredentials.password,
        "pkBase64": base64.encode(playerKey),
      });
    }
  }

  Future<PlayerCredentials> deserializePlayerCredentials(String serializedPlayerCredentials) {
    var asJsonMap = jsonDecode(serializedPlayerCredentials);
    var pk = base64.decode(asJsonMap["pkBase64"]);
    return initExistingPlayer(
      asJsonMap["username"],
      asJsonMap["password"],
      pk);
  }
}
