import 'package:gallery_gambling/native_interop.dart';
import 'package:gallery_gambling/shared/notifications.dart' as n;

class AndroidServices {
  Future<Map<dynamic, dynamic>> _getAndClearIntent() => nativeMethods.invokeMethod<Map<dynamic, dynamic>>("getAndClearIntent");

  Future<n.Notification> getNotificationFromIntent() async {
    final intentMap = await _getAndClearIntent();
    if (intentMap == null) {
      return null;
    }

    // The notification information is stored in extras.
    if (!intentMap.containsKey("extras")) {
      return null;
    }

    return n.parseNotification(intentMap["extras"]);
  }
}
