import 'package:permission_handler/permission_handler.dart';

class Permissions {
  Future<bool> checkPermissions() async {
    final permissionHandler = PermissionHandler();
    final ungrantedPermissions = List<PermissionGroup>();
    for (final permissionGroup in [PermissionGroup.storage, PermissionGroup.photos]) {
      if (!_isPermissionGranted(await permissionHandler.checkPermissionStatus(permissionGroup))) {
        ungrantedPermissions.add(permissionGroup);
      }
    }

    if (ungrantedPermissions.isNotEmpty) {
      final permissions = await PermissionHandler().requestPermissions(ungrantedPermissions);
      return permissions.values.every((status) => _isPermissionGranted(status));
    }

    return true;
  }

  static bool _isPermissionGranted(PermissionStatus status) {
    return status == PermissionStatus.granted;
  }
}
