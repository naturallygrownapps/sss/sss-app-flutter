import 'dart:io';
import 'dart:math' as math;

import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/progress.dart';
import 'package:gallery_gambling/serialization.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:gallery_gambling/util_functions.dart';
import "package:http/http.dart" as http;
import 'dart:convert';

typedef Future AuthorizationMissingHandler();

class RequestResult<T> {
  final T result;
  final int statusCode;

  RequestResult(this.result, this.statusCode);
}

class ServiceClient {
  static const int _defaultSendbufferSize = 16 * 1024;

  Settings _settings;
  String _authorizationToken;
  AuthorizationMissingHandler _authorizationMissingHandler;

  ServiceClient(this._settings) {}

  Future<T> get<T>(String requestUri, JsonDeserializer<T> deserializer,
      {bool relogin = true}) async {
    var getResult =
        await getWithStatusCode(requestUri, deserializer, relogin: relogin);
    return getResult.result;
  }

  Future<RequestResult<T>> getWithStatusCode<T>(
      String requestUri, JsonDeserializer<T> deserializer,
      {bool relogin = true}) async {
    var jsonResult = await getWithStatusCodeReturnJson(requestUri, deserializer, relogin: relogin);

    T deserialized = null;
    if (deserializer != null) {
      deserialized = deserializer(jsonResult.result);
    }

    return new RequestResult(deserialized, jsonResult.statusCode);
  }

  Future<RequestResult<List<T>>> getWithStatusCodeReturnList<T>(
    String requestUri, JsonDeserializer<T> deserializer,
    { bool relogin = true }) async {
    var jsonResult = await getWithStatusCodeReturnJson(requestUri, deserializer, relogin: relogin);

    List<T> deserialized = new List<T>();
    if (deserializer != null && jsonResult.result is List) {
      for(var i in jsonResult.result) {
        deserialized.add(deserializer(i));
      }
    }

    return new RequestResult(deserialized, jsonResult.statusCode);
  }

  Future<RequestResult<dynamic>> getWithStatusCodeReturnJson<T>(
    String requestUri, JsonDeserializer<T> deserializer,
    { bool relogin = true }) async {
    var client = new http.Client();
    try {
      var response = await _sendRequest(
        client,
        () => new http.Request("GET", Uri.parse(_settings.baseUrl + requestUri))
          ..followRedirects = false,
        relogin: relogin
      );
      var result = await _readResponseAsJson(response);
      return new RequestResult(result, response.statusCode);
    }
    finally {
      client.close();
    }
  }

  Future<T> post<T>(String requestUri, Map<String, dynamic> jsonPayload,
      JsonDeserializer<T> deserializer,
      {bool relogin = true, Progress<double> progress = null}) async {
    var postResult = await postWithStatusCode(
        requestUri, jsonPayload, deserializer,
        relogin: relogin, progress: progress);
    return postResult.result;
  }

  Future<RequestResult<T>> postWithStatusCode<T>(String requestUri,
    Map<String, dynamic> jsonPayload, JsonDeserializer<T> deserializer,
    {bool relogin = true, Progress<double> progress = null}) async {
    var jsonResult = await postWithStatusCodeReturnJson(
      requestUri,
      jsonPayload,
      relogin: relogin,
      progress: progress
    );

    T deserialized = null;
    if (deserializer != null) {
      deserialized = deserializer(jsonResult.result);
    }

    return new RequestResult(deserialized, jsonResult.statusCode);
  }

  Future<RequestResult<List<T>>> postWithStatusCodeReturnList<T>(String requestUri,
    Map<String, dynamic> jsonPayload, JsonDeserializer<T> deserializer,
    {bool relogin = true, Progress<double> progress = null}) async {
    var jsonResult = await postWithStatusCodeReturnJson(
      requestUri,
      jsonPayload,
      relogin: relogin,
      progress: progress
    );

    List<T> deserialized = new List<T>();
    if (deserializer != null && jsonResult.result is List) {
      for(var i in jsonResult.result) {
        deserialized.add(deserializer(i));
      }
    }

    return new RequestResult(deserialized, jsonResult.statusCode);
  }

  Future<RequestResult<dynamic>> postWithStatusCodeReturnJson(String requestUri,
      Map<String, dynamic> jsonPayload,
      {bool relogin = true, Progress<double> progress = null}) async {
    final client = new http.Client();
    try {
      final response = await _sendRequest<http.StreamedRequest>(
        client,
        () => new http.StreamedRequest(
          "POST", Uri.parse(_settings.baseUrl + requestUri)
        )
          ..headers["Content-Type"] = "application/json; charset=utf-8"
          ..followRedirects = false,
        relogin: relogin,
        sendBody: (request) async {
          if (jsonPayload != null) {
            final jsonString = json.encode(jsonPayload);
            final stringSink = utf8.encoder.startChunkedConversion(request.sink);

            final numCharsToSend = jsonString.length;
            var curStart,
              curEnd = 0;
            var remainingChars = numCharsToSend;
            var isLast = false;
            while (!isLast) {
              curStart = curEnd;
              curEnd =
                curStart + math.min(_defaultSendbufferSize, remainingChars);
              remainingChars -= curEnd - curStart;
              if (remainingChars == 0) {
                isLast = true;
              }

              stringSink.addSlice(jsonString, curStart, curEnd, isLast);
              if (progress != null) {
                progress.report(
                  (numCharsToSend - remainingChars) / numCharsToSend);
              }
            }
          }
          else {
            // No data to be sent.
            request.sink.close();
          }
      });

      var result = await _readResponseAsJson(response);
      return new RequestResult(result, response.statusCode);
    }
    finally {
      client.close();
    }
  }

  void setAuthorizationToken(String token) {
    _authorizationToken = token;
  }

  void setAuthorizationMissingHandler(AuthorizationMissingHandler handler) {
    _authorizationMissingHandler = handler;
  }

  Future<dynamic> _readResponseAsJson(http.StreamedResponse response) async {
    StringBuffer sb = new StringBuffer();
    await response.stream
      .transform(utf8.decoder)
      .forEach((value) => sb.write(value));
    var responseBody = sb.toString();
    if (isNotNullOrEmpty(responseBody)) {
      return json.decode(responseBody);
    }
    else {
      return responseBody;
    }
  }

  Future<T> _readResponseAs<T>(
      http.StreamedResponse response, JsonDeserializer<T> deserializer) async {
    var jsonBody = await _readResponseAsJson(response);
    return deserializer(jsonBody);
  }

  Future<http.StreamedResponse> _sendRequest<R extends http.BaseRequest>(
      http.Client client,
      R requestFac(),
      {Future sendBody(R request),
      bool relogin = true}) async {

    createAndSendRequest() async {
      final request = requestFac();
      if (_authorizationToken != null && _authorizationToken.isNotEmpty) {
        request.headers["Authorization"] = "Bearer ${_authorizationToken}";
      }
      request.headers["Accept"] = "application/json";
      final responseFuture = client.send(request);
      if (sendBody != null) {
        await sendBody(request);
      }
      return await responseFuture;
    }

    var response = await createAndSendRequest();
    if (relogin &&
        response.statusCode == 401 &&
        _authorizationMissingHandler != null) {
      await _authorizationMissingHandler();
      response = await createAndSendRequest();
    }

    if (response.statusCode == 401) {
      throw new AuthenticationException();
    }

    if (response.statusCode >= 400) {
      var err = new ServiceError();
      try {
        err = await _readResponseAs<ServiceError>(
            response, ServiceError.fromJson);
      } catch (e) {
        throw HttpException("HTTP error code ${response.statusCode}.",
            uri: response.request.url);
      }

      if (err != null && !err.isEmpty) {
        throw new ServiceException(err, response.statusCode);
      } else {
        throw HttpException("HTTP error code ${response.statusCode}.",
            uri: response.request.url);
      }
    }

    return response;
  }
}
