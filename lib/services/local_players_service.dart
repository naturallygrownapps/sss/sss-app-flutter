import 'package:gallery_gambling/persisted_collection.dart';
import 'package:gallery_gambling/services/players_service.dart';
import 'package:gallery_gambling/shared/dto/user_entity_dto.dart';

class LocalPlayersService {
  final PlayersService _playersService;
  PersistedCollection<UserEntityDto> _recentPlayers;

  LocalPlayersService(this._playersService) {
    _recentPlayers = new PersistedCollection(
        "recentPlayers", UserEntityDto.toJsonStatic, UserEntityDto.fromJson);
  }

  Future init() {
    return _recentPlayers.init();
  }

  Future<UserEntityDto> getSelf() => _playersService.getSelf();
  Future<List<UserEntityDto>> findPlayers(String username) => _playersService.findPlayers(username);
  Future<UserEntityDto> findRandomPlayer() => _playersService.findRandomPlayer();

  Future addRecentOpponentByName(String username) {
    return addRecentOpponent(new UserEntityDto()..username = username);
  }

  Future addRecentOpponent(UserEntityDto opponent) async {
    if (_recentPlayers.get((u) => u.username == opponent.username) == null) {
      await _recentPlayers.add(opponent);
    }
  }

  List<UserEntityDto> getRecentOpponents() {
    return _recentPlayers.getAll();
  }
}
