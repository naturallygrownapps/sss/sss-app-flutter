import 'dart:typed_data';

import 'package:flutter_sodium/flutter_sodium.dart';

import 'package:gallery_gambling/shared/encrypted_bet.dart';

const int _SymmetricKeySize = crypto_secretbox_KEYBYTES;
const int _KeyDerivationSaltLength = crypto_pwhash_SALTBYTES;
const int _IvLength = crypto_secretbox_NONCEBYTES;

class PlayerKey {
  final Uint8List derivationSalt;
  final Uint8List iv;
  final Uint8List publicKey;
  final Uint8List privateKey;

  PlayerKey({this.derivationSalt, this.iv, this.publicKey, this.privateKey});
}

class KeyAndIv {
  final Uint8List key;
  final Uint8List iv;

  KeyAndIv(this.key, this.iv);
}

class CryptHelper {

  static Future<Uint8List> getRandomBytes(int count) async {
    return await RandomBytes.buffer(count);
  }

  Future<EncryptedBet> encryptBet(Uint8List betData, Uint8List yourPublicKey, Uint8List opponentPublicKey) async {
    var key = await SecretBox.generateKey();
    var iv = await SecretBox.generateNonce();
    var encryptedData = await _encryptSymmetric(key, betData, iv);

    var receiverKeyAndIv = await _encryptKeyAndIv(key, iv, opponentPublicKey);
    var sourceKeyAndIv = await _encryptKeyAndIv(key, iv, yourPublicKey);
    return new EncryptedBet(
      data: encryptedData,
      sourceKeyAndIV: sourceKeyAndIv,
      receiverKeyAndIV: receiverKeyAndIv
    );
  }

  Future<Uint8List> decryptBetAsWinner(EncryptedBet bet, PlayerKey playerKey) {
    return _decryptBet(bet.receiverKeyAndIV, bet.data, playerKey);
  }

  Future<Uint8List> decryptBetAsLoser(EncryptedBet bet, PlayerKey playerKey) {
    return _decryptBet(bet.sourceKeyAndIV, bet.data, playerKey);
  }

  Future<Uint8List> _decryptBet(Uint8List keyAndIvEncrypted, Uint8List dataEncrypted, PlayerKey playerKey) async {
    var keyAndIv = await _decryptAsymmetric(keyAndIvEncrypted, playerKey.privateKey, playerKey.publicKey);

    var key = keyAndIv.sublist(0, _SymmetricKeySize);
    var iv = keyAndIv.sublist(_SymmetricKeySize);

    return _decryptSymmetric(key, dataEncrypted, iv);
  }

  Future<PlayerKey> createPlayerKey() async {
    var keyPair = await CryptoBox.generateKeyPair();

    return new PlayerKey(
      derivationSalt: await PasswordHash.generateSalt(),
      iv: await getRandomBytes(_IvLength),
      privateKey: keyPair.secretKey,
      publicKey: keyPair.publicKey
    );
  }

  Future<PlayerKey> parsePlayerKey(String password, Uint8List playerKey) async {
    // split playerKey into salt + iv + encryptedPrivateKey
    var derivationSalt = playerKey.sublist(0, _KeyDerivationSaltLength);
    var iv = playerKey.sublist(_KeyDerivationSaltLength, _KeyDerivationSaltLength + _IvLength);
    var encryptedPrivateKey = playerKey.sublist(_KeyDerivationSaltLength + _IvLength);

    var derivedKey = await _deriveSymmetricKeyFromPassword(password, derivationSalt);
    var decryptedPrivateKey = await _decryptSymmetric(derivedKey, encryptedPrivateKey, iv);

    return new PlayerKey(
      derivationSalt: derivationSalt,
      iv: iv,
      publicKey: await Sodium.cryptoScalarmultBase(decryptedPrivateKey),
      privateKey: decryptedPrivateKey
    );
  }

  Future<Uint8List> exportPlayerKey(String password, PlayerKey playerKey) async {
    var derivedKey = await _deriveSymmetricKeyFromPassword(password, playerKey.derivationSalt);
    var encryptedPrivateKey = await _encryptSymmetric(derivedKey, playerKey.privateKey, playerKey.iv);

    // concat salt + iv + encryptedPrivateKey
    var exported = new Uint8List(playerKey.derivationSalt.length + playerKey.iv.length + encryptedPrivateKey.length);
    exported.setAll(0, playerKey.derivationSalt);
    exported.setAll(playerKey.derivationSalt.length, playerKey.iv);
    exported.setAll(playerKey.derivationSalt.length + playerKey.iv.length, encryptedPrivateKey);
    return exported;
  }

  Future<Uint8List> _encryptKeyAndIv(Uint8List key, Uint8List iv, Uint8List publicKey) {
    var keyAndIvBuffer = new Uint8List(key.length + iv.length);
    keyAndIvBuffer.setAll(0, key);
    keyAndIvBuffer.setAll(key.length, iv);
    return _encryptAsymmetric(keyAndIvBuffer, publicKey);
  }

  Future<Uint8List> _encryptAsymmetric(Uint8List data, Uint8List publicKey) {
    return SealedBox.sealBytes(data, publicKey);
  }

  Future<Uint8List> _decryptAsymmetric(Uint8List data, Uint8List privateKey, Uint8List publicKey) {
    return SealedBox.sealBytesOpen(data, new KeyPair(publicKey, privateKey));
  }

  Future<Uint8List> _encryptSymmetric(Uint8List key, Uint8List data, Uint8List iv) {
    return SecretBox.encryptBytes(data, iv, key);
  }

  Future<Uint8List> _decryptSymmetric(Uint8List key, Uint8List data, Uint8List iv) {
    return SecretBox.decryptBytes(data, iv, key);
  }

  static Future<Uint8List> _deriveSymmetricKeyFromPassword(String password, Uint8List salt) {
    return PasswordHash.hash(password, salt, outlen: _SymmetricKeySize);
  }
}
