import 'package:gallery_gambling/services/android_services.dart';
import 'package:gallery_gambling/services/auth_helper.dart';
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/bet_service.dart';
import 'package:gallery_gambling/services/crypt_helper.dart';
import 'package:gallery_gambling/services/dynamic_links_service.dart';
import 'package:gallery_gambling/services/game_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/local_game_service.dart';
import 'package:gallery_gambling/services/local_notifications.dart';
import 'package:gallery_gambling/services/local_players_service.dart';
import 'package:gallery_gambling/services/permissions.dart';
import 'package:gallery_gambling/services/players_service.dart';
import 'package:gallery_gambling/services/push_notifications.dart';
import 'package:gallery_gambling/services/service_client.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:get_it/get_it.dart';

GetIt _getIt = GetIt.instance;

class SSSServices {
  static Future setup() async {
    _getIt.registerSingleton(await Settings.create());
    _getIt.registerSingleton(ServiceClient(_getIt.get<Settings>()));
    _getIt.registerSingleton(LoadingTracker());
    _getIt.registerSingleton(CryptHelper());
    _getIt.registerSingleton(AuthHelper(_getIt.get<CryptHelper>()));
    _getIt.registerSingleton(
        Session(_getIt.get<ServiceClient>(), _getIt.get<Settings>(), _getIt.get<AuthHelper>()));
    _getIt.registerSingleton(PushNotifications(getService<Settings>()));
    _getIt.registerSingleton(AuthService(_getIt.get<ServiceClient>(), _getIt.get<Session>(),
        _getIt.get<Settings>(), _getIt.get<PushNotifications>()));
    _getIt.registerSingleton(GameService(_getIt.get<ServiceClient>()));
    _getIt.registerSingleton(BetService());
    _getIt.registerSingleton(LocalGameService(
      _getIt.get<GameService>(),
      _getIt.get<Session>(),
      _getIt.get<CryptHelper>(),
      _getIt.get<BetService>(),
    ));
    _getIt.registerSingleton(PlayersService(_getIt.get<ServiceClient>()));
    _getIt.registerSingleton(LocalPlayersService(_getIt.get<PlayersService>()));
    _getIt.registerSingleton(Permissions());
    _getIt.registerSingleton(AndroidServices());
    _getIt.registerSingleton(LocalNotificationsService(_getIt.get<LocalGameService>()));
    _getIt.registerSingleton(DynamicLinksService());

    await _getIt.get<LocalGameService>().init();
    await _getIt.get<LocalPlayersService>().init();
    await _getIt.get<PushNotifications>().init();
    await _getIt.get<LocalNotificationsService>().init();
    await _getIt.get<DynamicLinksService>().init();
  }
}

T getService<T>() => _getIt.get<T>();
