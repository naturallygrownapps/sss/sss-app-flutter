import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/progress.dart';
import 'package:gallery_gambling/services/service_client.dart';
import 'package:gallery_gambling/shared/encrypted_bet.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/parameters/game_module_parameters.dart';

class GameService {
  final ServiceClient _serviceClient;

  GameService(this._serviceClient);

  Future<GameStatus> queryGameStatus(String gameId) async {
    try {
      return await _serviceClient.get<GameStatus>(
        "/game/$gameId",
        GameStatus.fromJson
      );
    }
    on ServiceException catch(e) {
      if (e.statusCode == 404) {
        return null;
      }
      else {
        rethrow;
      }
    }
  }

  Future<RequestResult<GameStatus>> challenge(ChallengeInput input) {
    return _serviceClient.postWithStatusCode<GameStatus>("/game/challenge", input.toJson(), GameStatus.fromJson);
  }

  Future<GameStatus> opponentReject(GameStatus game) {
    return _serviceClient.post<GameStatus>("/game/${game.gameId}/opponentreject", null, GameStatus.fromJson);
  }

  Future acceptRejection(GameStatus game) {
    return _serviceClient.post("/game/${game.gameId}/acceptrejection", null, null);
  }

  Future<GameStatus> opponentAccept(GameStatus game, OpponentAcceptInput input) {
    return _serviceClient.post<GameStatus>("/game/${game.gameId}/opponentaccept", input.toJson(), GameStatus.fromJson);
  }

  Future<List<GameStatus>> getMyGames(MyGamesInput input) async {
    var response = await _serviceClient.postWithStatusCodeReturnList<GameStatus>("/game/mygames", input.toJson(), GameStatus.fromJson);
    return response.result;
  }

  Future<GameStatus> makeMove(GameStatus game, MoveInput input) {
    return _serviceClient.post<GameStatus>("/game/${game.gameId}/move", input.toJson(), GameStatus.fromJson);
  }

  Future placeChallengerBet(GameStatus game, ChallengerBetInput input, { Progress<double> progress }) {
    return _serviceClient.postWithStatusCode("/game/${game.gameId}/challengerbet", input.toJson(), null,
      progress: progress
    );
  }

  Future<EncryptedBet> getBet(GameStatus game) {
    return _serviceClient.get<EncryptedBet>("/game/${game.gameId}/bet", EncryptedBet.fromJson);
  }

}