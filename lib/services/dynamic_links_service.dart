import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:gallery_gambling/error_handling.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

const _baseLink = "http://naturallygrownapps.de/gg/applink/";

class DynamicLinksService {
  BehaviorSubject<DynamicLink> _links = BehaviorSubject();
  Observable<DynamicLink> get links => _links;

  Future init() async {
    final initialLinkData = await FirebaseDynamicLinks.instance.getInitialLink();
    if (initialLinkData?.link != null) {
      _links.add(DynamicLink(initialLinkData.link));
    }

    FirebaseDynamicLinks.instance.onLink(onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final deepLink = dynamicLink?.link;
      if (deepLink != null) {
        _links.add(DynamicLink(deepLink));
      }
    }, onError: (OnLinkErrorException e) async {
      reportErrorManually(e);
    });
  }

  Future<Uri> createDynamicLink(Uri link) async {
    final parameters = DynamicLinkParameters(
      uriPrefix: 'https://gallerygambling.page.link',
      link: link,
      androidParameters: AndroidParameters(
        packageName: 'de.naturallygrownapps.gallerygambling',
        minimumVersion: 10001,
      ),
    );

    final shortened = await parameters.buildShortLink();
    return shortened.shortUrl;
  }
}

class DynamicLink {
  final Uri link;
  final Set<String> _processedBy = Set();

  DynamicLink(this.link);

  bool hasBeenProcessedBy(String name) => _processedBy.contains(name);
  void markProcessedBy(String name) => _processedBy.add(name);
  bool tryProcessBy(String name) {
    if (hasBeenProcessedBy(name)) {
      return false;
    } else {
      markProcessedBy(name);
      return true;
    }
  }
}

class InviteLink {
  static Uri create(String invitingUsername) =>
      Uri.parse('${_baseLink}invited?by=${invitingUsername}');
  static InviteLinkInfo tryParse(Uri uri) {
    if (uri.pathSegments.isNotEmpty &&
        uri.pathSegments.last == "invited" &&
        uri.queryParameters.containsKey("by")) {
      return InviteLinkInfo(uri.queryParameters["by"]);
    }

    return null;
  }
}

class InviteLinkInfo {
  final String invitingUsername;

  InviteLinkInfo(this.invitingUsername);
}
