import 'dart:typed_data';

import 'package:archive/archive.dart';
import 'package:flutter/foundation.dart';

List<int> _zipIsolated(List<int> data) {
  return BZip2Encoder().encode(data);
}

Future<Uint8List> zip(Uint8List data) async {
  // Zipping takes some time so put it in an isolate. Unzip seems to be faster.
  return Uint8List.fromList(await compute(_zipIsolated, data));
}

Uint8List unzip(Uint8List zippedData) {
  return new BZip2Decoder().decodeBytes(zippedData, verify: true);
}
