import 'dart:math';
import 'dart:typed_data';
import 'dart:developer' as dev;

import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/persisted_collection.dart';
import 'package:gallery_gambling/progress.dart';
import 'package:gallery_gambling/services/bet_service.dart';
import 'package:gallery_gambling/services/crypt_helper.dart';
import 'package:gallery_gambling/services/game_service.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/services/zip_helper.dart';
import 'package:gallery_gambling/shared/bet_capabilities.dart';
import 'package:gallery_gambling/shared/encrypted_bet.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';
import 'package:gallery_gambling/shared/parameters/game_module_parameters.dart';
import 'package:gallery_gambling/shared/player_role.dart';

class StartedGame {
  final GameStatus gameStatus;
  final bool isExistingGame;

  StartedGame(this.gameStatus, this.isExistingGame);
}

class LocalGameService {
  final GameService _gameSvc;
  final Session _session;
  final CryptHelper _cryptHelper;
  final BetService _betService;
  final Random _random = new Random();
  PersistedCollection<GameStatus> _currentGames;
  // The collection that stores that GameStatus that we previously displayed (needed for correct transition animations).
  PersistedCollection<GameStatus> _previousGameStatus;
  PersistedCollection<String> _deletedGames;

  LocalGameService(this._gameSvc, this._session, this._cryptHelper, this._betService) {
    _currentGames = new PersistedCollection("games", GameStatus.toJsonStatic, GameStatus.fromJson);
    _previousGameStatus = new PersistedCollection("previousGameStatus", GameStatus.toJsonStatic, GameStatus.fromJson);
    _deletedGames = new PersistedCollection("deletedGames");
  }

  init() {
    return Future.wait([
      _currentGames.init(),
      _previousGameStatus.init(),
      _deletedGames.init()
    ]);
  }

  Future<GameStatus> queryGameStatusById(String gameId) async {
    var newStatus = await _gameSvc.queryGameStatus(gameId);
    if (newStatus != null) {
      await _updateGameStatus(newStatus);
    }
    return newStatus;
  }

  Future<GameStatus> queryGameStatus(GameStatus game) {
    if (game == null) {
      return Future.value(null);
    }
    else {
      return queryGameStatusById(game.gameId);
    }
  }

  List<GameStatus> getLocalGames() {
    final games = _currentGames.getAll();
    _filterDeletedGames(games);
    return games;
  }

  Future<List<GameStatus>> getGames([ bool updateFromServer = true ]) async {
    List<GameStatus> games;
    if (updateFromServer && _session.currentPlayer != null) {
      games = await _syncLocalGames();
    }
    else {
      games = _currentGames.getAll();
    }

    _filterDeletedGames(games);
    return games;
  }

  GameStatus getPreviouslyDisplayedGameStatus(GameStatus currentGameStatus) {
    return _previousGameStatus.get((g) => g.gameId == currentGameStatus.gameId);
  }

  Future rememberPreviouslyDisplayedGameStatus(GameStatus gameStatus) {
    return _previousGameStatus.replace((g) => g.gameId == gameStatus.gameId, gameStatus);
  }

  Future endGame(GameStatus gameStatus) async {
    await markGameDeleted(gameStatus);
    if (gameStatus.state == GameState.Rejected &&
        gameStatus.playerRole == PlayerRole.Challenger) {
      await _gameSvc.acceptRejection(gameStatus);
    }
  }

  Future markGameDeleted(GameStatus gameStatus) async {
    await _currentGames.remove((g) => g.gameId == gameStatus.gameId);
    await _previousGameStatus.remove((g) => g.gameId == gameStatus.gameId);
    await _deletedGames.add(gameStatus.gameId);
  }

  Future<StartedGame> startGame(String challengedUsername) async {
    final param = new ChallengeInput()
      ..publicKey = _session.currentPlayer.playerKey.publicKey
      ..challengedUsername = challengedUsername
      ..betTypeCaps = [ BetCapabilities.Photo ];

    final challengeResult = await _gameSvc.challenge(param);
    final isExistingGame = challengeResult.statusCode == 303; // HTTP SeeOther

    if (!isExistingGame)
    {
      await _currentGames.add(challengeResult.result);
    }

    return new StartedGame(challengeResult.result, isExistingGame);
  }

  Future<EncryptedBet> getEncryptedBet(
    BetCapabilities type,
    Uint8List yourPublicKey,
    Uint8List opponentPublicKey) async {
    final bet = await _getBetForType(type);

    final zippedBet = await zip(bet);
    final encryptedBet = await _cryptHelper.encryptBet(
      zippedBet,
      yourPublicKey,
      opponentPublicKey
    );
    return encryptedBet;
  }

  Future placeChallengerBet(GameStatus gameStatus, [ Progress<double> progress = null ]) async {
    if (gameStatus.agreedBetType == null)
      throw new GameException("Game does not have an agreed bet type.");

    var encryptedBet = await getEncryptedBet(gameStatus.agreedBetType,
      _session.currentPlayer.playerKey.publicKey,
      gameStatus.otherPlayer.publicKey
    );

    await _gameSvc.placeChallengerBet(gameStatus, new ChallengerBetInput()
      ..bet = encryptedBet,
      progress: progress);
  }

  Future<GameStatus> acceptGame(GameStatus gameStatus) async {
    if (gameStatus.state != GameState.AcceptanceRequired)
      throw new GameException("Game has to be in ${GameState.AcceptanceRequired.toString()} state.");

    var betType = _findBetType(gameStatus.betTypeCaps);
    if (betType == null)
      throw new GameException("You don't support any of your opponents bet types.");

    var encryptedBet = await getEncryptedBet(betType,
      _session.currentPlayer.playerKey.publicKey,
      gameStatus.otherPlayer.publicKey
    );

    gameStatus = await _gameSvc.opponentAccept(gameStatus, new OpponentAcceptInput()
      ..bet = encryptedBet
      ..betType = betType
      ..publicKey = _session.currentPlayer.playerKey.publicKey
    );

    await _updateGameStatus(gameStatus);
    return gameStatus;
  }

  Future<GameStatus> rejectGame(GameStatus gameStatus) async {
    var newGameStatus = await _gameSvc.opponentReject(gameStatus);
    await _updateGameStatus(newGameStatus);
    return newGameStatus;
  }

  Future<GameStatus> makeMove(GameStatus game, Move move) async {
    var newGameStatus = await _gameSvc.makeMove(game, new MoveInput()
      ..gameMove = move
    );

    await _updateGameStatus(newGameStatus);
    return newGameStatus;
  }

  Future _updateGameStatus(GameStatus newStatus) async {
    final existingGameStatus = _currentGames.get((g) => g.gameId == newStatus.gameId);
    if (existingGameStatus == null) {
      _currentGames.add(newStatus);
    }
    else if (existingGameStatus.revision < newStatus.revision) {
      await _currentGames.replaceWithItem(existingGameStatus, newStatus);
    }
  }

  Future<List<GameStatus>> _syncLocalGames() async {
    var remoteGames = await _gameSvc.getMyGames(new MyGamesInput()
      ..gameKey = _session.currentPlayer.playerKey.publicKey
    );

    _currentGames.replaceAll(remoteGames);

    // Clear games from the deleted list that don't exist anymore.
    var deletedGameIds = _deletedGames.getAll().toSet();
    var stillExistingDeletedGames = remoteGames.where((g) => deletedGameIds.contains(g.gameId));
    _deletedGames.replaceAll(stillExistingDeletedGames.map((g) => g.gameId));

    return remoteGames;
  }

  void _filterDeletedGames(List<GameStatus> games) {
    var deletedGames = _deletedGames.getAll().toSet();
    games.removeWhere((g) => deletedGames.contains(g.gameId));
    games.sort((a, b) => a.creationDate.compareTo(b.creationDate));
  }

  BetCapabilities _findBetType(List<BetCapabilities> betTypeCaps) {
    var availableBetTypes = [
      BetCapabilities.Photo
    ];

    var commonBetTypes = betTypeCaps.toSet().intersection(availableBetTypes.toSet());
    if (commonBetTypes.length == 0)
      return null;

    return commonBetTypes.elementAt(_random.nextInt(commonBetTypes.length));
  }

  Future<Uint8List> getWinnerPrize(GameStatus game) async {
    var encryptedBet = await _gameSvc.getBet(game);
    var zippedBet = await _cryptHelper.decryptBetAsWinner(encryptedBet, _session.currentPlayer.playerKey);
    var unzipped = await unzip(zippedBet);
    // The backend only lets us download the prize once. After that, the game is completed and will be removed when both
    // players have seen the bet.
    await markGameDeleted(game);
    return unzipped;
  }

  Future<Uint8List> getLostBet(GameStatus game) async {
    var encryptedBet = await _gameSvc.getBet(game);
    var zippedBet = await _cryptHelper.decryptBetAsLoser(encryptedBet, _session.currentPlayer.playerKey);
    var unzipped = await unzip(zippedBet);
    await markGameDeleted(game);
    return unzipped;
  }

  Future<Uint8List> _getBetForType(BetCapabilities betType) async {
    Uint8List result = null;
    if (betType == BetCapabilities.Photo) {
      try {
        result = await _betService.getImage();
      }
      catch (e) {
        dev.log("Image could not be loaded.", error: e);
        throw new NoBetAvailableException("Image could not be loaded.");
      }
    }
    else {
      throw new NoBetAvailableException("BetCapability ${betType.toString()} is not supported yet.");
    }

    if (result != null) {
      return result;
    }
    else {
      throw new NoBetAvailableException("Could not collect a bet to place.");
    }
  }
}
