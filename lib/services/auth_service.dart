import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:gallery_gambling/services/crypt_helper.dart';
import 'package:gallery_gambling/services/push_notifications.dart';
import 'package:gallery_gambling/services/service_client.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:gallery_gambling/shared/notifications.dart';
import 'package:gallery_gambling/shared/parameters/auth_module_parameters.dart'
    as authParams;
import 'package:gallery_gambling/util_functions.dart';

class AuthService {
  final ServiceClient _serviceClient;
  final Session _session;
  final Settings _settings;
  final PushNotifications _pushNotifications;
  StreamSubscription<String> _notificationTokenUpdateSubscription;

  AuthService(this._serviceClient, this._session, this._settings, this._pushNotifications) {
  }

  void startTokenUpdateHandling() {
    if (_notificationTokenUpdateSubscription == null) {
      _notificationTokenUpdateSubscription =
        _pushNotifications.currentToken.listen(_onNotificationTokenUpdated);
    }
  }

  void dispose() {
    _notificationTokenUpdateSubscription?.cancel();
    _notificationTokenUpdateSubscription = null;
  }

  Future login(String username, String password, {isRelogin = false}) async {
    var result = await _serviceClient.post<authParams.LoginOutput>(
        "/auth/login",
        (new authParams.LoginInput()
              ..username = username
              ..pwHash = _hashPassword(password))
            .toJson(),
        authParams.LoginOutput.fromJson,
        // Only try to relogin if this is not already a relogin attempt.
        relogin: !isRelogin);
    await _session.onLogin(result.token, result.username, password, result.pk);

     if (!isRelogin) {
       final token = await _pushNotifications.currentToken.first;
       await _updateNotificationToken(token);
     }
  }

  Future logout() async {
    _session.reset();
    await deleteUserRelatedFiles();
    await _updateNotificationToken(null);
  }

  Future register(String username, String password) async {
    var u = username.trim();

    var crypt = getService<CryptHelper>();
    var playerKey = await crypt.createPlayerKey();
    var playerKeyExported = await crypt.exportPlayerKey(password, playerKey);

    await _serviceClient.post<authParams.RegisterOutput>(
      "/auth/register",
      (new authParams.RegisterInput()
      ..username = u
      ..pwHash = _hashPassword(password)
      ..pk = playerKeyExported)
        .toJson(),
      authParams.RegisterOutput.fromJson);
  }

  Future deleteAccount() async {
    await _serviceClient.post("/auth/deleteAccount", null, null);
    await logout();
  }

  String _hashPassword(String password) {
    var pwBytes = utf8.encode(password);
    var hash = sha256.convert(pwBytes);
    return base64.encode(hash.bytes);
  }

  void _onNotificationTokenUpdated(String newToken) {
    _updateNotificationToken(newToken);
  }

  Future _updateNotificationToken(String token) {
    if (!isNotNullOrEmpty(token)) {
      // If we don't have a token now, delete the previously set token.
      final oldToken = _settings.notificationToken;
      _settings.notificationToken = token;
      if (isNotNullOrEmpty(oldToken) && _session.isLoggedIn) {
        return _serviceClient.post<dynamic>(
          "/auth/deleteNotifications",
          (authParams.DeleteNotificationsInput()
            ..notificationId = oldToken).toJson(),
          null
        );
      }
    }
    else {
      // Apply and notify of the new token (only if we're logged in).
      _settings.notificationToken = token;

      if (_session.isLoggedIn) {
        final pubKey = _session.currentPlayer?.playerKey?.publicKey;
        final pubKeyBase64 = pubKey != null ? base64Encode(pubKey) : "";

        return _serviceClient.post<dynamic>(
          "/auth/registerNotifications",
            (authParams.RegisterNotificationsInput()
              ..notificationId = token
              ..platform = Platform.isAndroid ? NotificationPlatform.Android : throw new Exception("Running on unknown platform: ${Platform.operatingSystem}")
              ..userPublicKey = pubKeyBase64).toJson(),
            null
        );
      }
    }

    return Future.value();
  }
}
