import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:gallery_gambling/shared/notifications.dart' as n;
import 'package:rxdart/rxdart.dart';

class PushNotifications {
  final Settings _settings;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  StreamSubscription<String> _tokenRefreshSubcription;
  Stream<String> currentToken;
  StreamController<n.Notification> _notifications = StreamController.broadcast();
  Stream<n.Notification> get notifications => _notifications.stream;

  PushNotifications(this._settings);

  void init() {
    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.configure(
      onMessage: _onMessage,
    );

    final connectableTokenRefresh =
      new Observable(_firebaseMessaging.onTokenRefresh)
        .startWith(_settings.notificationToken)
        .distinct()
        .publishValue();
    _tokenRefreshSubcription = connectableTokenRefresh.connect();
    currentToken = connectableTokenRefresh;
  }

  void dispose() {
    _tokenRefreshSubcription.cancel();
  }

  Future<dynamic> _onMessage(Map<String, dynamic> message) {
    final data = message['data'] ?? message;
    final parsed = n.parseNotification(data);
    if (parsed != null) {
      _notifications.add(parsed);
    }
    return Future.value();
  }
}
