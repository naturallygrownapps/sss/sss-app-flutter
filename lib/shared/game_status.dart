﻿import "package:gallery_gambling/shared/bet_capabilities.dart";
import 'package:gallery_gambling/shared/dto/game_participant_dto.dart';
import 'package:gallery_gambling/shared/dto/round_dto.dart';
import "package:gallery_gambling/shared/player_role.dart";
import 'package:json_annotation/json_annotation.dart';

part 'game_status.g.dart';

enum GameState {
  /// <summary>
  /// The game was started, but was not accepted yet.
  /// </summary>
  InitiatedByYou,

  /// <summary>
  /// You were challenged and are requested to accept the game.
  /// </summary>
  AcceptanceRequired,

  /// <summary>
  /// You are the challenger and are requested to place your bet (your opponent already provided it when accepting).
  /// </summary>
  ChallengerBetRequired,

  /// <summary>
  /// The client has to submit a move.
  /// </summary>
  YourMoveRequired,

  /// <summary>
  /// The other player has to submit a move.
  /// </summary>
  WaitingForOtherPlayer,

  /// <summary>
  /// The game was rejected by one of the players.
  /// </summary>
  Rejected,

  /// <summary>
  /// The game is over and the client won.
  /// </summary>
  YouWon,

  /// <summary>
  /// The game is over and the other player won.
  /// </summary>
  YouLost
}

List<Map<String, dynamic>> mapRoundList(List<RoundDto> rounds) {
  if (rounds == null) {
    return null;
  }

  return rounds.map((r) => r.toJson()).toList();
}

List<RoundDto> parseRoundList(List<dynamic> jsonRounds) {
  if (jsonRounds == null) {
    return null;
  }

  return jsonRounds.map((jr) => RoundDto.fromJson(jr)).toList();
}

@JsonSerializable()
class GameStatus {
  int timestamp;
  DateTime creationDate;
  String gameId;
  GameState state;
  List<String> roundWinners;
  int requiredWinningRounds;
  BetCapabilities agreedBetType;
  List<BetCapabilities> betTypeCaps;
  @JsonKey(
    toJson: GameParticipantDto.toJsonStatic,
    fromJson: GameParticipantDto.fromJson
  )
  GameParticipantDto otherPlayer;
  @JsonKey(
    toJson: mapRoundList,
    fromJson: parseRoundList
  )
  List<RoundDto> rounds;
  PlayerRole playerRole;
  int revision;

  static GameStatus fromJson(Map<String, dynamic> json) =>
      _$GameStatusFromJson(json);
  Map<String, dynamic> toJson() => _$GameStatusToJson(this);
  static Map<String, dynamic> toJsonStatic(GameStatus o) => o.toJson();
}
