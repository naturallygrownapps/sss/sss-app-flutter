// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notification _$NotificationFromJson(Map<String, dynamic> json) {
  return Notification(
      _$enumDecodeNullable(_$NotificationTypeEnumMap, json['type']));
}

Map<String, dynamic> _$NotificationToJson(Notification instance) =>
    <String, dynamic>{'type': _$NotificationTypeEnumMap[instance.type]};

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$NotificationTypeEnumMap = <NotificationType, dynamic>{
  NotificationType.NewGame: 'NewGame',
  NotificationType.GameUpdated: 'GameUpdated',
  NotificationType.GameRejected: 'GameRejected'
};

NewGameStartedNotification _$NewGameStartedNotificationFromJson(
    Map<String, dynamic> json) {
  return NewGameStartedNotification()
    ..type = _$enumDecodeNullable(_$NotificationTypeEnumMap, json['type'])
    ..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$NewGameStartedNotificationToJson(
        NewGameStartedNotification instance) =>
    <String, dynamic>{
      'type': _$NotificationTypeEnumMap[instance.type],
      'gameId': instance.gameId
    };

GameUpdatedNotification _$GameUpdatedNotificationFromJson(
    Map<String, dynamic> json) {
  return GameUpdatedNotification()
    ..type = _$enumDecodeNullable(_$NotificationTypeEnumMap, json['type'])
    ..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$GameUpdatedNotificationToJson(
        GameUpdatedNotification instance) =>
    <String, dynamic>{
      'type': _$NotificationTypeEnumMap[instance.type],
      'gameId': instance.gameId
    };

GameRejectedNotification _$GameRejectedNotificationFromJson(
    Map<String, dynamic> json) {
  return GameRejectedNotification()
    ..type = _$enumDecodeNullable(_$NotificationTypeEnumMap, json['type'])
    ..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$GameRejectedNotificationToJson(
        GameRejectedNotification instance) =>
    <String, dynamic>{
      'type': _$NotificationTypeEnumMap[instance.type],
      'gameId': instance.gameId
    };
