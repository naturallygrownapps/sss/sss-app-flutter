// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_participant_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameParticipantDto _$GameParticipantDtoFromJson(Map<String, dynamic> json) {
  return GameParticipantDto()
    ..username = json['username'] as String
    ..publicKey = json['publicKey'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['publicKey'] as String);
}

Map<String, dynamic> _$GameParticipantDtoToJson(GameParticipantDto instance) =>
    <String, dynamic>{
      'username': instance.username,
      'publicKey': instance.publicKey == null
          ? null
          : const Base64JsonConverter().toJson(instance.publicKey)
    };
