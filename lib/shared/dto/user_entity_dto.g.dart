// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_entity_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserEntityDto _$UserEntityDtoFromJson(Map<String, dynamic> json) {
  return UserEntityDto()..username = json['username'] as String;
}

Map<String, dynamic> _$UserEntityDtoToJson(UserEntityDto instance) =>
    <String, dynamic>{'username': instance.username};
