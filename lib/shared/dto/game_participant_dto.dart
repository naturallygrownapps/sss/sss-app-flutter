﻿import 'dart:typed_data';
import 'package:gallery_gambling/serialization.dart';
import 'package:json_annotation/json_annotation.dart';

part 'game_participant_dto.g.dart';

Map<String, dynamic> gameParticipantDtoToJson(GameParticipantDto o) => o.toJson();

@JsonSerializable()
@Base64JsonConverter()
class GameParticipantDto {
  static final GameParticipantDto Nobody = new GameParticipantDto()
    ..username = "nobody";

  String username;
  Uint8List publicKey;

  GameParticipantDto() {
  }

  static GameParticipantDto fromJson(Map<String, dynamic> json) => _$GameParticipantDtoFromJson(json);
  Map<String, dynamic> toJson() => _$GameParticipantDtoToJson(this);

  static Map<String, dynamic> toJsonStatic(GameParticipantDto o) => o.toJson();
}