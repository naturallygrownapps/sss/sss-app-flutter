// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'round_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoundDto _$RoundDtoFromJson(Map<String, dynamic> json) {
  return RoundDto()
    ..yourMove = _$enumDecodeNullable(_$MoveEnumMap, json['yourMove'])
    ..otherPlayerMove =
        _$enumDecodeNullable(_$MoveEnumMap, json['otherPlayerMove']);
}

Map<String, dynamic> _$RoundDtoToJson(RoundDto instance) => <String, dynamic>{
      'yourMove': _$MoveEnumMap[instance.yourMove],
      'otherPlayerMove': _$MoveEnumMap[instance.otherPlayerMove]
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$MoveEnumMap = <Move, dynamic>{
  Move.Rock: 'Rock',
  Move.Paper: 'Paper',
  Move.Scissors: 'Scissors'
};
