﻿import 'package:json_annotation/json_annotation.dart';

part 'user_entity_dto.g.dart';

@JsonSerializable()
class UserEntityDto {
  String username;

  static UserEntityDto fromJson(Map<String, dynamic> json) => _$UserEntityDtoFromJson(json);
  Map<String, dynamic> toJson() => _$UserEntityDtoToJson(this);
  static Map<String, dynamic> toJsonStatic(UserEntityDto obj) => obj.toJson();
}
