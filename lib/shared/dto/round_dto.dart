﻿import 'package:gallery_gambling/shared/move.dart';
import 'package:json_annotation/json_annotation.dart';

part 'round_dto.g.dart';

/// <summary>
/// Represents one round of a game, i.e. a single turn in which each player provides a <see cref="Move"/> (rock, paper or scissors).
/// </summary>
@JsonSerializable()
class RoundDto {
  Move yourMove;
  Move otherPlayerMove;

  RoundDto() {}

  static RoundDto fromJson(Map<String, dynamic> json) =>
      _$RoundDtoFromJson(json);
  Map<String, dynamic> toJson() => _$RoundDtoToJson(this);
}
