// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'encrypted_bet.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EncryptedBet _$EncryptedBetFromJson(Map<String, dynamic> json) {
  return EncryptedBet(
      data: json['data'] == null
          ? null
          : const Base64JsonConverter().fromJson(json['data'] as String),
      sourceKeyAndIV: json['sourceKeyAndIV'] == null
          ? null
          : const Base64JsonConverter()
              .fromJson(json['sourceKeyAndIV'] as String),
      receiverKeyAndIV: json['receiverKeyAndIV'] == null
          ? null
          : const Base64JsonConverter()
              .fromJson(json['receiverKeyAndIV'] as String));
}

Map<String, dynamic> _$EncryptedBetToJson(EncryptedBet instance) =>
    <String, dynamic>{
      'data': instance.data == null
          ? null
          : const Base64JsonConverter().toJson(instance.data),
      'sourceKeyAndIV': instance.sourceKeyAndIV == null
          ? null
          : const Base64JsonConverter().toJson(instance.sourceKeyAndIV),
      'receiverKeyAndIV': instance.receiverKeyAndIV == null
          ? null
          : const Base64JsonConverter().toJson(instance.receiverKeyAndIV)
    };
