﻿import 'dart:typed_data';

import 'package:gallery_gambling/serialization.dart';
import 'package:gallery_gambling/shared/notifications.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth_module_parameters.g.dart';

@JsonSerializable()
class LoginInput {
  String username;
  String pwHash;

  static LoginInput fromJson(Map<String, dynamic> json) =>
      _$LoginInputFromJson(json);
  Map<String, dynamic> toJson() => _$LoginInputToJson(this);
}

@JsonSerializable()
@Base64JsonConverter()
class LoginOutput {
  String token;
  String username;
  Uint8List pk;

  static LoginOutput fromJson(Map<String, dynamic> json) =>
      _$LoginOutputFromJson(json);
  Map<String, dynamic> toJson() => _$LoginOutputToJson(this);
}

@JsonSerializable()
@Base64JsonConverter()
class RegisterInput {
  String username;
  String pwHash;
  Uint8List pk; // salt for derivation + iv + encrypted private key

  static RegisterInput fromJson(Map<String, dynamic> json) =>
      _$RegisterInputFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterInputToJson(this);
}

@JsonSerializable()
class RegisterOutput {
  String UserId;

  static RegisterOutput fromJson(Map<String, dynamic> json) =>
      _$RegisterOutputFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterOutputToJson(this);
}

@JsonSerializable()
class CheckTokenInput {
  String Token;

  static CheckTokenInput fromJson(Map<String, dynamic> json) =>
      _$CheckTokenInputFromJson(json);
  Map<String, dynamic> toJson() => _$CheckTokenInputToJson(this);
}

@JsonSerializable()
class RegisterNotificationsInput {
  String notificationId;
  String userPublicKey;
  NotificationPlatform platform;

  static RegisterNotificationsInput fromJson(Map<String, dynamic> json) =>
      _$RegisterNotificationsInputFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterNotificationsInputToJson(this);
}

@JsonSerializable()
class DeleteNotificationsInput {
  String notificationId;

  static DeleteNotificationsInput fromJson(Map<String, dynamic> json) =>
      _$DeleteNotificationsInputFromJson(json);
  Map<String, dynamic> toJson() => _$DeleteNotificationsInputToJson(this);
}
