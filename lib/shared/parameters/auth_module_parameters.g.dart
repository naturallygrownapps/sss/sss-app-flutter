// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_module_parameters.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInput _$LoginInputFromJson(Map<String, dynamic> json) {
  return LoginInput()
    ..username = json['username'] as String
    ..pwHash = json['pwHash'] as String;
}

Map<String, dynamic> _$LoginInputToJson(LoginInput instance) =>
    <String, dynamic>{'username': instance.username, 'pwHash': instance.pwHash};

LoginOutput _$LoginOutputFromJson(Map<String, dynamic> json) {
  return LoginOutput()
    ..token = json['token'] as String
    ..username = json['username'] as String
    ..pk = json['pk'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['pk'] as String);
}

Map<String, dynamic> _$LoginOutputToJson(LoginOutput instance) =>
    <String, dynamic>{
      'token': instance.token,
      'username': instance.username,
      'pk': instance.pk == null
          ? null
          : const Base64JsonConverter().toJson(instance.pk)
    };

RegisterInput _$RegisterInputFromJson(Map<String, dynamic> json) {
  return RegisterInput()
    ..username = json['username'] as String
    ..pwHash = json['pwHash'] as String
    ..pk = json['pk'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['pk'] as String);
}

Map<String, dynamic> _$RegisterInputToJson(RegisterInput instance) =>
    <String, dynamic>{
      'username': instance.username,
      'pwHash': instance.pwHash,
      'pk': instance.pk == null
          ? null
          : const Base64JsonConverter().toJson(instance.pk)
    };

RegisterOutput _$RegisterOutputFromJson(Map<String, dynamic> json) {
  return RegisterOutput()..UserId = json['UserId'] as String;
}

Map<String, dynamic> _$RegisterOutputToJson(RegisterOutput instance) =>
    <String, dynamic>{'UserId': instance.UserId};

CheckTokenInput _$CheckTokenInputFromJson(Map<String, dynamic> json) {
  return CheckTokenInput()..Token = json['Token'] as String;
}

Map<String, dynamic> _$CheckTokenInputToJson(CheckTokenInput instance) =>
    <String, dynamic>{'Token': instance.Token};

RegisterNotificationsInput _$RegisterNotificationsInputFromJson(
    Map<String, dynamic> json) {
  return RegisterNotificationsInput()
    ..notificationId = json['notificationId'] as String
    ..userPublicKey = json['userPublicKey'] as String
    ..platform =
        _$enumDecodeNullable(_$NotificationPlatformEnumMap, json['platform']);
}

Map<String, dynamic> _$RegisterNotificationsInputToJson(
        RegisterNotificationsInput instance) =>
    <String, dynamic>{
      'notificationId': instance.notificationId,
      'userPublicKey': instance.userPublicKey,
      'platform': _$NotificationPlatformEnumMap[instance.platform]
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$NotificationPlatformEnumMap = <NotificationPlatform, dynamic>{
  NotificationPlatform.Android: 'Android'
};

DeleteNotificationsInput _$DeleteNotificationsInputFromJson(
    Map<String, dynamic> json) {
  return DeleteNotificationsInput()
    ..notificationId = json['notificationId'] as String;
}

Map<String, dynamic> _$DeleteNotificationsInputToJson(
        DeleteNotificationsInput instance) =>
    <String, dynamic>{'notificationId': instance.notificationId};
