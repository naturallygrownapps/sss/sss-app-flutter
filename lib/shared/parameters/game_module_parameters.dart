﻿import 'dart:typed_data';

import 'package:gallery_gambling/serialization.dart';
import 'package:gallery_gambling/shared/bet_capabilities.dart';
import 'package:gallery_gambling/shared/encrypted_bet.dart';
import 'package:gallery_gambling/shared/move.dart';
import 'package:json_annotation/json_annotation.dart';

part 'game_module_parameters.g.dart';

@JsonSerializable()
@Base64JsonConverter()
class ChallengeInput {
  String challengedUsername;
  Uint8List publicKey;
  List<BetCapabilities> betTypeCaps;

  static ChallengeInput fromJson(Map<String, dynamic> json) =>
      _$ChallengeInputFromJson(json);
  Map<String, dynamic> toJson() => _$ChallengeInputToJson(this);
}

@JsonSerializable()
class MoveInput {
  String gameId;
  @JsonKey(name: 'move')
  Move gameMove;

  static MoveInput fromJson(Map<String, dynamic> json) =>
      _$MoveInputFromJson(json);
  Map<String, dynamic> toJson() => _$MoveInputToJson(this);
}

@JsonSerializable()
@Base64JsonConverter()
class OpponentAcceptInput {
  String gameId;
  Uint8List publicKey;
  BetCapabilities betType;
  EncryptedBet bet;

  static OpponentAcceptInput fromJson(Map<String, dynamic> json) =>
      _$OpponentAcceptInputFromJson(json);
  Map<String, dynamic> toJson() => _$OpponentAcceptInputToJson(this);
}

@JsonSerializable()
class ChallengerBetInput {
  String gameId;
  EncryptedBet bet;

  static ChallengerBetInput fromJson(Map<String, dynamic> json) =>
      _$ChallengerBetInputFromJson(json);
  Map<String, dynamic> toJson() => _$ChallengerBetInputToJson(this);
}

@JsonSerializable()
class OpponentRejectInput {
  String gameId;

  static OpponentRejectInput fromJson(Map<String, dynamic> json) =>
      _$OpponentRejectInputFromJson(json);
  Map<String, dynamic> toJson() => _$OpponentRejectInputToJson(this);
}

@JsonSerializable()
class AcceptRejectionInput {
  String gameId;

  static AcceptRejectionInput fromJson(Map<String, dynamic> json) =>
      _$AcceptRejectionInputFromJson(json);
  Map<String, dynamic> toJson() => _$AcceptRejectionInputToJson(this);
}

@JsonSerializable()
class GetBetInput {
  String gameId;

  static GetBetInput fromJson(Map<String, dynamic> json) =>
      _$GetBetInputFromJson(json);
  Map<String, dynamic> toJson() => _$GetBetInputToJson(this);
}

@JsonSerializable()
class QueryInput {
  String gameId;

  static QueryInput fromJson(Map<String, dynamic> json) =>
      _$QueryInputFromJson(json);
  Map<String, dynamic> toJson() => _$QueryInputToJson(this);
}

@JsonSerializable()
@Base64JsonConverter()
class MyGamesInput {
  Uint8List gameKey;

  static MyGamesInput fromJson(Map<String, dynamic> json) =>
      _$MyGamesInputFromJson(json);
  Map<String, dynamic> toJson() => _$MyGamesInputToJson(this);
}
