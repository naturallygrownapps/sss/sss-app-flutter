// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_module_parameters.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChallengeInput _$ChallengeInputFromJson(Map<String, dynamic> json) {
  return ChallengeInput()
    ..challengedUsername = json['challengedUsername'] as String
    ..publicKey = json['publicKey'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['publicKey'] as String)
    ..betTypeCaps = (json['betTypeCaps'] as List)
        ?.map((e) => _$enumDecodeNullable(_$BetCapabilitiesEnumMap, e))
        ?.toList();
}

Map<String, dynamic> _$ChallengeInputToJson(ChallengeInput instance) =>
    <String, dynamic>{
      'challengedUsername': instance.challengedUsername,
      'publicKey': instance.publicKey == null
          ? null
          : const Base64JsonConverter().toJson(instance.publicKey),
      'betTypeCaps': instance.betTypeCaps
          ?.map((e) => _$BetCapabilitiesEnumMap[e])
          ?.toList()
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$BetCapabilitiesEnumMap = <BetCapabilities, dynamic>{
  BetCapabilities.Photo: 'Photo',
  BetCapabilities.Location: 'Location',
  BetCapabilities.TextMessage: 'TextMessage'
};

MoveInput _$MoveInputFromJson(Map<String, dynamic> json) {
  return MoveInput()
    ..gameId = json['gameId'] as String
    ..gameMove = _$enumDecodeNullable(_$MoveEnumMap, json['move']);
}

Map<String, dynamic> _$MoveInputToJson(MoveInput instance) => <String, dynamic>{
      'gameId': instance.gameId,
      'move': _$MoveEnumMap[instance.gameMove]
    };

const _$MoveEnumMap = <Move, dynamic>{
  Move.Rock: 'Rock',
  Move.Paper: 'Paper',
  Move.Scissors: 'Scissors'
};

OpponentAcceptInput _$OpponentAcceptInputFromJson(Map<String, dynamic> json) {
  return OpponentAcceptInput()
    ..gameId = json['gameId'] as String
    ..publicKey = json['publicKey'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['publicKey'] as String)
    ..betType = _$enumDecodeNullable(_$BetCapabilitiesEnumMap, json['betType'])
    ..bet = json['bet'] == null
        ? null
        : EncryptedBet.fromJson(json['bet'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OpponentAcceptInputToJson(
        OpponentAcceptInput instance) =>
    <String, dynamic>{
      'gameId': instance.gameId,
      'publicKey': instance.publicKey == null
          ? null
          : const Base64JsonConverter().toJson(instance.publicKey),
      'betType': _$BetCapabilitiesEnumMap[instance.betType],
      'bet': instance.bet
    };

ChallengerBetInput _$ChallengerBetInputFromJson(Map<String, dynamic> json) {
  return ChallengerBetInput()
    ..gameId = json['gameId'] as String
    ..bet = json['bet'] == null
        ? null
        : EncryptedBet.fromJson(json['bet'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ChallengerBetInputToJson(ChallengerBetInput instance) =>
    <String, dynamic>{'gameId': instance.gameId, 'bet': instance.bet};

OpponentRejectInput _$OpponentRejectInputFromJson(Map<String, dynamic> json) {
  return OpponentRejectInput()..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$OpponentRejectInputToJson(
        OpponentRejectInput instance) =>
    <String, dynamic>{'gameId': instance.gameId};

AcceptRejectionInput _$AcceptRejectionInputFromJson(Map<String, dynamic> json) {
  return AcceptRejectionInput()..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$AcceptRejectionInputToJson(
        AcceptRejectionInput instance) =>
    <String, dynamic>{'gameId': instance.gameId};

GetBetInput _$GetBetInputFromJson(Map<String, dynamic> json) {
  return GetBetInput()..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$GetBetInputToJson(GetBetInput instance) =>
    <String, dynamic>{'gameId': instance.gameId};

QueryInput _$QueryInputFromJson(Map<String, dynamic> json) {
  return QueryInput()..gameId = json['gameId'] as String;
}

Map<String, dynamic> _$QueryInputToJson(QueryInput instance) =>
    <String, dynamic>{'gameId': instance.gameId};

MyGamesInput _$MyGamesInputFromJson(Map<String, dynamic> json) {
  return MyGamesInput()
    ..gameKey = json['gameKey'] == null
        ? null
        : const Base64JsonConverter().fromJson(json['gameKey'] as String);
}

Map<String, dynamic> _$MyGamesInputToJson(MyGamesInput instance) =>
    <String, dynamic>{
      'gameKey': instance.gameKey == null
          ? null
          : const Base64JsonConverter().toJson(instance.gameKey)
    };
