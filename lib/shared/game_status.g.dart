// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameStatus _$GameStatusFromJson(Map<String, dynamic> json) {
  return GameStatus()
    ..timestamp = json['timestamp'] as int
    ..creationDate = json['creationDate'] == null
        ? null
        : DateTime.parse(json['creationDate'] as String)
    ..gameId = json['gameId'] as String
    ..state = _$enumDecodeNullable(_$GameStateEnumMap, json['state'])
    ..roundWinners =
        (json['roundWinners'] as List)?.map((e) => e as String)?.toList()
    ..requiredWinningRounds = json['requiredWinningRounds'] as int
    ..agreedBetType =
        _$enumDecodeNullable(_$BetCapabilitiesEnumMap, json['agreedBetType'])
    ..betTypeCaps = (json['betTypeCaps'] as List)
        ?.map((e) => _$enumDecodeNullable(_$BetCapabilitiesEnumMap, e))
        ?.toList()
    ..otherPlayer = json['otherPlayer'] == null
        ? null
        : GameParticipantDto.fromJson(
            json['otherPlayer'] as Map<String, dynamic>)
    ..rounds =
        json['rounds'] == null ? null : parseRoundList(json['rounds'] as List)
    ..playerRole = _$enumDecodeNullable(_$PlayerRoleEnumMap, json['playerRole'])
    ..revision = json['revision'] as int;
}

Map<String, dynamic> _$GameStatusToJson(GameStatus instance) =>
    <String, dynamic>{
      'timestamp': instance.timestamp,
      'creationDate': instance.creationDate?.toIso8601String(),
      'gameId': instance.gameId,
      'state': _$GameStateEnumMap[instance.state],
      'roundWinners': instance.roundWinners,
      'requiredWinningRounds': instance.requiredWinningRounds,
      'agreedBetType': _$BetCapabilitiesEnumMap[instance.agreedBetType],
      'betTypeCaps': instance.betTypeCaps
          ?.map((e) => _$BetCapabilitiesEnumMap[e])
          ?.toList(),
      'otherPlayer': instance.otherPlayer == null
          ? null
          : GameParticipantDto.toJsonStatic(instance.otherPlayer),
      'rounds': instance.rounds == null ? null : mapRoundList(instance.rounds),
      'playerRole': _$PlayerRoleEnumMap[instance.playerRole],
      'revision': instance.revision
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$GameStateEnumMap = <GameState, dynamic>{
  GameState.InitiatedByYou: 'InitiatedByYou',
  GameState.AcceptanceRequired: 'AcceptanceRequired',
  GameState.ChallengerBetRequired: 'ChallengerBetRequired',
  GameState.YourMoveRequired: 'YourMoveRequired',
  GameState.WaitingForOtherPlayer: 'WaitingForOtherPlayer',
  GameState.Rejected: 'Rejected',
  GameState.YouWon: 'YouWon',
  GameState.YouLost: 'YouLost'
};

const _$BetCapabilitiesEnumMap = <BetCapabilities, dynamic>{
  BetCapabilities.Photo: 'Photo',
  BetCapabilities.Location: 'Location',
  BetCapabilities.TextMessage: 'TextMessage'
};

const _$PlayerRoleEnumMap = <PlayerRole, dynamic>{
  PlayerRole.Challenger: 'Challenger',
  PlayerRole.Opponent: 'Opponent'
};
