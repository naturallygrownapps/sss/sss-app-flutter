﻿import 'package:enum_to_string/enum_to_string.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'notifications.g.dart';

enum NotificationPlatform { Android }

enum NotificationType {
  NewGame,
  GameUpdated,
  GameRejected
}

Notification parseNotification(dynamic notificationData) {
  if (notificationData == null) {
    return null;
  }
  Notification notification = null;
  final type = EnumToString.fromString(NotificationType.values, notificationData["type"]);
  final gameId = notificationData["gameId"];
  if (type == NotificationType.NewGame) {
    if (isNotNullOrEmpty(gameId)) {
      notification = NewGameStartedNotification()
        ..gameId = gameId;
    }
  }
  else if (type == NotificationType.GameUpdated) {
    if (isNotNullOrEmpty(gameId)) {
      notification = GameUpdatedNotification()
        ..gameId = gameId;
    }
  }
  else if (type == NotificationType.GameRejected) {
    if (isNotNullOrEmpty(gameId)) {
      notification = GameRejectedNotification()
        ..gameId = gameId;
    }
  }
  return notification;
}

@JsonSerializable()
class Notification {
  NotificationType type;

  Notification(this.type) {}
  
  static Notification fromJson(Map<String, dynamic> json) => _$NotificationFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationToJson(this);
}

abstract class GameNotification extends Notification {
  String gameId;

  GameNotification(NotificationType type) : super(type);
}

@JsonSerializable()
class NewGameStartedNotification extends GameNotification {

  NewGameStartedNotification() : super(NotificationType.NewGame) {}

  static NewGameStartedNotification fromJson(Map<String, dynamic> json) => _$NewGameStartedNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$NewGameStartedNotificationToJson(this);
}

@JsonSerializable()
class GameUpdatedNotification extends GameNotification {

  GameUpdatedNotification() : super(NotificationType.GameUpdated) {}

  static GameUpdatedNotification fromJson(Map<String, dynamic> json) => _$GameUpdatedNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$GameUpdatedNotificationToJson(this);
}

@JsonSerializable()
class GameRejectedNotification extends GameNotification {

  GameRejectedNotification() : super(NotificationType.GameRejected) {}

  static GameRejectedNotification fromJson(Map<String, dynamic> json) => _$GameRejectedNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$GameRejectedNotificationToJson(this);
}
