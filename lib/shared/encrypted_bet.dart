﻿import 'dart:typed_data';

import 'package:gallery_gambling/serialization.dart';
import 'package:json_annotation/json_annotation.dart';

part 'encrypted_bet.g.dart';

@JsonSerializable()
@Base64JsonConverter()
class EncryptedBet {
  Uint8List data;
  Uint8List sourceKeyAndIV;
  Uint8List receiverKeyAndIV;

  EncryptedBet({this.data, this.sourceKeyAndIV, this.receiverKeyAndIV});

  static EncryptedBet fromJson(Map<String, dynamic> json) => _$EncryptedBetFromJson(json);
  Map<String, dynamic> toJson() => _$EncryptedBetToJson(this);
}
