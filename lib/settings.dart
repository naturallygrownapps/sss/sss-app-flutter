import 'package:gallery_gambling/util_functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings {
  SharedPreferences _prefs;
  Settings(this._prefs);

  static Future<Settings> create() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return new Settings(prefs);
  }

  static const String BaseUrlKey = "BaseUrl";
  String get baseUrl {
    if (isDebugMode()) {
      return getValue<String>(BaseUrlKey, "http://192.168.1.180:4420");
    } else {
      return getValue<String>(BaseUrlKey, "https://gg.nygra.de");
    }
  }

  static const String AuthTokenKey = "AuthToken";
  String get authToken => getValue<String>(AuthTokenKey, "");
  set authToken(String newToken) => setValue(AuthTokenKey, newToken);

  static const String CurrentPlayerStateKey = "CurrentPlayerState";
  String get currentPlayerState => getValue<String>(CurrentPlayerStateKey, "");
  set currentPlayerState(String playerState) => setValue(CurrentPlayerStateKey, playerState);

  static const String NotificationTokenKey = "NotificationToken";
  String get notificationToken => getValue<String>(NotificationTokenKey, "");
  set notificationToken(String newToken) => setValue(NotificationTokenKey, newToken);

  static const String AppHasRunBeforeKey = "AppHasRunBefore";
  bool get appHasRunBefore => getValue<bool>(AppHasRunBeforeKey, false);
  set appHasRunBefore(bool newValue) => setValue(AppHasRunBeforeKey, newValue);

  T getValue<T>(String key, T fallbackValue) {
    T result;
    if (T == String) {
      result = _prefs.getString(key) as T;
    } else if (T == int) {
      result = _prefs.getInt(key) as T;
    } else if (T == bool) {
      result = _prefs.getBool(key) as T;
    } else {
      throw new Exception("Unknown settings type ${T.toString()}.");
    }

    return result ?? fallbackValue;
  }

  Future setValue<T>(String key, T value) {
    if (T == String) {
      return _prefs.setString(key, value as String);
    } else if (T == int) {
      return _prefs.setInt(key, value as int);
    } else if (T == bool) {
      return _prefs.setBool(key, value as bool);
    } else {
      throw new Exception("Unknown settings type ${T.toString()}.");
    }
  }
}
