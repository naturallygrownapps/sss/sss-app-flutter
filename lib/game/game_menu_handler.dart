import 'package:gallery_gambling/shared/game_status.dart';

abstract class GameMenuHandler {
  /**
   * Shows the game menu for a specified state. The state can be null to hide the menu.
   * This does not return a future as it doesn't matter whether the menu is currently animating or not. It will simply
   * cancel its current animation and continue with the new animation when a transition is interrupted.
   */
  void showMenuForState(GameState state);
}
