import 'dart:typed_data';

import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/game/game_menu_handler.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class GameContext with ChangeNotifier implements GameMenuHandler {
  bool _isDisposed = false;

  GameStatus _game;
  GameStatus get game => _game;
  set game(GameStatus newGameStatus) {
    _game = newGameStatus;
    _notifyListenersIfNotDisposed();
  }

  GameState _menuState;
  GameState get menuState => _menuState;
  set menuState(GameState newMenuState) {
    _menuState = newMenuState;
    _notifyListenersIfNotDisposed();
  }

  Uint8List _winnerPrize;
  Uint8List get winnerPrize => _winnerPrize;
  set winnerPrize(Uint8List newWinnerPrize) {
    _winnerPrize = newWinnerPrize;
    _notifyListenersIfNotDisposed();
  }

  Uint8List _loserImage;
  Uint8List get loserImage => _loserImage;
  set loserImage(Uint8List newLoserImage) {
    _loserImage = newLoserImage;
    _notifyListenersIfNotDisposed();
  }

  GameContext(GameStatus initialGameStatus) {
    _game = initialGameStatus;
  }

  @override
  void showMenuForState(GameState state) {
    menuState  = state;
  }

  bool isGameFinished() {
    return (game != null && game.state == GameState.Rejected) || winnerPrize != null || loserImage != null;
  }

  void _notifyListenersIfNotDisposed() {
    if (!_isDisposed) {
      notifyListeners();
    }
  }

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }
}
