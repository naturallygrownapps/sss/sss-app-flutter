import 'dart:math';

const int DefaultScaleAnimDuration = 2000;
const int DefaultTranslateInAnimDuration = 1000;
const int DefaultTranslateOutAnimDuration = 1000;
const int DefaultOpacityAnimDuration = 1000;

const double ScaleHuge = 3;
const double ScaleBig = 1;
const double ScaleMedium = 0.75;
const double ScaleSmall = 0.5;

const double ScoreOpacity = 0.75;

const Point CenterAnchor = const Point<double>(0.5, 0.5);
const double CenterXPos = 0.5;
const double CenterYPos = 0.5;

const Point TopRightAnchor = const Point<double>(1, 0);
const double TopRightXPos = 0.9;
const double TopRightYPos = 0.1;
const double TopRightPlayerDescriptionLabelYPos = 0.05;

const Point TopLeftAnchor = const Point<double>(0, 0);
const double TopLeftXPos = 0.1;
const double TopLeftYPos = 0.1;
const double TopLeftPlayerLabelYPos = 0.025;

const Point TopCenterAnchor = const Point<double>(0.5, 0);
const double TopCenterXPos = 0.5;
const double TopCenterYPos = 0.2;

const Point BottomLeftAnchor = const Point<double>(0, 1);
const double BottomLeftXPos = 0.1;
const double BottomLeftYPos = 0.9;

const Point BottomCenterAnchor = const Point<double>(0.5, 1);
const double BottomCenterXPos = 0.5;
const double BottomCenterYPos = 0.8;

const Point BottomRightAnchor = const Point<double>(1, 1);
const double BottomRightXPos = 0.9;
const double BottomRightYPos = 0.9;
const double BottomRightPlayerLabelYPos = 0.975;
