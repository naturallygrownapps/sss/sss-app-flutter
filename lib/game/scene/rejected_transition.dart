import 'package:flutter/animation.dart';
import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class RejectedTransition extends GameStateTransition {
  RejectedTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) {
    showOpponentName_GameNotStarted(g);

    final rejectToken = assets.rejectedToken;
    rejectToken.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: gsconst.ScaleSmall
    );
    return rejectToken.animateScaleTo(gsconst.ScaleBig, curve: Curves.elasticOut).runOn(animCtx, gsconst.DefaultScaleAnimDuration ~/ 2);
  }
}
