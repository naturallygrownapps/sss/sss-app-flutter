import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class InitiatedByYouTransition extends GameStateTransition {
  InitiatedByYouTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) {
    showOpponentName_GameNotStarted(g);

    assets.waitOtherPlayerToken.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: gsconst.ScaleBig
    );

    return Future.value(true);
  }
}
