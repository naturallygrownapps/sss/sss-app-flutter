import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;

class AcceptanceRequiredTransition extends GameStateTransition  {

  AcceptanceRequiredTransition(GameScene scene) : super(scene);

  @override
  Future<bool> apply(GameStatus previousGameStatus, GameStatus newGameStatus) {

    showOpponentName_GameNotStarted(newGameStatus);

    var logoToken = assets.logoToken;
    logoToken.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: gsconst.ScaleBig
    );
    return Future.value(true);
  }
}