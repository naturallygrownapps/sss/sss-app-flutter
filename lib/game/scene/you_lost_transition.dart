import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class YouLostTransition extends GameStateTransition {
  YouLostTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) async {
    showPlayerNames(g);

    // Animate the last move.
    if (previousGameStatus != null &&
      (previousGameStatus.state == GameState.YourMoveRequired ||
       previousGameStatus.state == GameState.WaitingForOtherPlayer)) {
      if (!await animatePreviousMove(previousGameStatus, g)) {
        return false;
      }

      return await animateMessage(loc.game_youlose).runOn(animCtx, gsconst.DefaultScaleAnimDuration);
    }
    else {
      showScores(g);
    }

    return true;
  }
}