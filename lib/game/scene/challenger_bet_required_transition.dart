import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;

class ChallengerBetRequiredTransition extends GameStateTransition {
  ChallengerBetRequiredTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) {
    // ChallengerBetRequired processing is done in the background, the screen stays on the hourglass then with a different status text.
    assets.waitOtherPlayerToken.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: gsconst.ScaleBig
    );

    showOpponentName_GameNotStarted(g);

    return Future.value(true);
  }
}