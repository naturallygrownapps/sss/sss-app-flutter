import 'dart:typed_data';

import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_state_animation.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;

class ShowPrizeAnimation extends GameStateAnimation {
  ShowPrizeAnimation(GameScene scene) : super(scene);

  Future<bool> run(Uint8List prize) async {
    final prizeImg = assets.prizeOrLossImage;
    await prizeImg.loadImage(prize);
    prizeImg.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      scale: 0,
      opacity: 1
    );

    final giftToken = assets.giftToken;

    giftToken.animateScaleTo(gsconst.ScaleHuge).runOn(animCtx, gsconst.DefaultScaleAnimDuration);
    giftToken.animateOpacityTo(0).runOn(animCtx, gsconst.DefaultScaleAnimDuration);

    await Future.delayed(Duration(milliseconds: gsconst.DefaultScaleAnimDuration ~/ 4));
    return await prizeImg.animateScaleTo(0.9).runOn(animCtx, gsconst.DefaultScaleAnimDuration);
  }
}
