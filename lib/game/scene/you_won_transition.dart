import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class YouWonTransition extends GameStateTransition {
  YouWonTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) async {
    showPlayerNames(g);

    // Animate the winning move.
    if (previousGameStatus != null &&
      (previousGameStatus.state == GameState.YourMoveRequired ||
       previousGameStatus.state == GameState.WaitingForOtherPlayer)) {
      if (!await animatePreviousMove(previousGameStatus, g)) {
        return false;
      }
      if (!await animateMessage(loc.game_youwin).runOn(animCtx, gsconst.DefaultScaleAnimDuration)) {
        return false;
      }
    }
    else {
      showScores(g);
    }

    // Then show the gift animation.
    final giftToken = assets.giftToken;
    giftToken.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: gsconst.ScaleBig,
      opacity: 0
    );

    if (!await giftToken.animateOpacityTo(1).runOn(animCtx, gsconst.DefaultOpacityAnimDuration ~/ 4)) {
      return false;
    }
    await Future.delayed(Duration(milliseconds: 500));

    return true;
  }
}