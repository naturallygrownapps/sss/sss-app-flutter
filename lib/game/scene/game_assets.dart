import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/assets.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/drawables.dart';
import 'package:gallery_gambling/shared/dto/round_dto.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';

class GameAssets extends Assets {

  GameAssets(AnimationContext animCtx, CanvasContext canvasCtx) : super(animCtx, canvasCtx);

  ImageDrawable _waitOtherPlayerToken;
  ImageDrawable get waitOtherPlayerToken => _waitOtherPlayerToken;

  ImageDrawable _unknownOtherPlayerToken;
  ImageDrawable get unknownOtherPlayerToken => _unknownOtherPlayerToken;

  ImageDrawable _paperToken_P1;
  ImageDrawable get paperToken_P1 => _paperToken_P1;
  ImageDrawable _scissorsToken_P1;
  ImageDrawable get scissorsToken_P1 => _scissorsToken_P1;
  ImageDrawable _rockToken_P1;
  ImageDrawable get rockToken_P1 => _rockToken_P1;

  ImageDrawable _paperToken_P2;
  ImageDrawable get paperToken_P2 => _paperToken_P2;
  ImageDrawable _scissorsToken_P2;
  ImageDrawable get scissorsToken_P2 => _scissorsToken_P2;
  ImageDrawable _rockToken_P2;
  ImageDrawable get rockToken_P2 => _rockToken_P2;

  ImageDrawable _logoToken;
  ImageDrawable get logoToken => _logoToken;
  ImageDrawable _giftToken;
  ImageDrawable get giftToken => _giftToken;
  ImageDrawable _rejectedToken;
  ImageDrawable get rejectedToken => _rejectedToken;

  TextDrawable _label_CenterMessage;
  TextDrawable get label_CenterMessage => _label_CenterMessage;
  TextDrawable _label_Score_PlayerYou;
  TextDrawable get label_Score_PlayerYou => _label_Score_PlayerYou;
  TextDrawable _label_Score_PlayerOther;
  TextDrawable get label_Score_PlayerOther => _label_Score_PlayerOther;
  TextDrawable _label_Name_PlayerYou;
  TextDrawable get label_Name_PlayerYou => _label_Name_PlayerYou;
  TextDrawable _label_Name_PlayerOther;
  TextDrawable get label_Name_PlayerOther => _label_Name_PlayerOther;
  TextDrawable _label_Description_PlayerOther;
  TextDrawable get label_Description_PlayerOther => _label_Description_PlayerOther;

  ReassignableImageDrawable _prizeOrLossImage;
  ReassignableImageDrawable get prizeOrLossImage => _prizeOrLossImage;

  @override
  Stream<DrawableEntity> loadAssets() async* {
    yield _waitOtherPlayerToken = await loadImageFromAsset("game/otherPending.png");
    yield _unknownOtherPlayerToken = await loadImageFromAsset("game/otherUnknown.png");

    yield _paperToken_P1 = await loadImageFromAsset("game/paper.png");
    yield _scissorsToken_P1 = await loadImageFromAsset("game/scissors.png");
    yield _rockToken_P1 = await loadImageFromAsset("game/rock.png");

    yield _paperToken_P2 = await loadImageFromAsset("game/paper.png");
    yield _scissorsToken_P2 = await loadImageFromAsset("game/scissors.png");
    yield _rockToken_P2 = await loadImageFromAsset("game/rock.png");

    yield _logoToken = await loadImageFromAsset("game/logo.png");
    yield _giftToken = await loadImageFromAsset("game/wonToken.png");
    yield _rejectedToken = await loadImageFromAsset("game/rejectIcon.png");

    yield _label_Score_PlayerYou = createTextAsset("game/score_playerYou")..textSize = 100;
    yield _label_Score_PlayerOther = createTextAsset("game/score_playerOther")..textSize = 100;
    yield _label_CenterMessage = createTextAsset("game/centerMessage")..textSize = 60;
    yield _label_Name_PlayerYou = createTextAsset("game/name_playerYou")..textSize = 40;
    yield _label_Name_PlayerOther = createTextAsset("game/name_playerOther")..textSize = 40;
    yield _label_Description_PlayerOther = createTextAsset("game/description_playerOther")..textSize = 25;

    yield _prizeOrLossImage = new ReassignableImageDrawable(animCtx, canvasCtx, "game/prizeOrLossImage");
  }

  ImageDrawable getMoveAssetForPreviousRound(GameStatus gameStatus, Player player) {
    return getMoveAsset(gameStatus, player, gsutil.getPreviousRound(gameStatus));
  }

  ImageDrawable getMoveAsset(GameStatus gameStatus, Player player, RoundDto r) {
    if (r == null) {
      return null;
    }

    Move move = gsutil.getMove(r, player);

    ImageDrawable entity = null;
    if (move != null) {
      switch (move) {
        case Move.Paper:
          entity = player == Player.You ? paperToken_P1 : paperToken_P2;
          break;
        case Move.Rock:
          entity = player == Player.You ? rockToken_P1 : rockToken_P2;
          break;
        case Move.Scissors:
          entity = player == Player.You ? scissorsToken_P1 : scissorsToken_P2;
          break;
      }
    }

    if (entity != null) {
      if (player == Player.You) {
        entity.setAnchor(gsconst.BottomLeftAnchor);
        entity.setTranslateRelative(gsconst.BottomLeftXPos, gsconst.BottomLeftYPos);
      }
      else if (player == Player.Other) {
        entity.setAnchor(gsconst.TopRightAnchor);
        entity.setTranslateRelative(gsconst.TopRightXPos, gsconst.TopRightYPos);
      }
    }

    return entity;
  }

}