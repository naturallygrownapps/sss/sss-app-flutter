import 'dart:async';
import 'dart:collection';
import 'dart:typed_data';

import 'package:gallery_gambling/game/scene/acceptance_required_transition.dart';
import 'package:gallery_gambling/game/scene/game_assets.dart';
import 'package:gallery_gambling/game/game_menu_handler.dart';
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/game/scene/initiated_by_you_transition.dart';
import 'package:gallery_gambling/game/scene/rejected_transition.dart';
import 'package:gallery_gambling/game/scene/show_loser_image_animation.dart';
import 'package:gallery_gambling/game/scene/show_prize_animation.dart';
import 'package:gallery_gambling/game/scene/waiting_for_other_player_transition.dart';
import 'package:gallery_gambling/game/scene/you_lost_transition.dart';
import 'package:gallery_gambling/game/scene/you_won_transition.dart';
import 'package:gallery_gambling/game/scene/your_move_required_transition.dart';
import 'package:gallery_gambling/scene/scene.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;

class GameScene extends SceneWithAssets<GameAssets> {
  Queue<_UpdateQueueEntry> _updateQueue = Queue();
  HashMap<GameState, GameStateTransition> _transitions;
  Session _session;
  GameStatus _previousGameStatus = null;
  bool _isFirstUpdate = true;
  ShowPrizeAnimation _showPrizeAnim;
  ShowLoserImageAnimation _showLossAnim;
  GameMenuHandler _menuHandler;
  bool _isProcessingQueue = false;

  String get playerName => _session.currentPlayer?.username ?? "";

  GameScene(GameAssets assets, this._menuHandler) : super(assets) {
    _session = getService<Session>();

    _transitions = HashMap()
    ..[GameState.AcceptanceRequired] = AcceptanceRequiredTransition(this)
    // ChallengerBet placement is performed transparently in the background, thus, for the user, the required action
    // stays "YourMoveRequired".
    ..[GameState.ChallengerBetRequired] = YourMoveRequiredTransition(this)
    ..[GameState.InitiatedByYou] = InitiatedByYouTransition(this)
    ..[GameState.Rejected] = RejectedTransition(this)
    ..[GameState.WaitingForOtherPlayer] = WaitingForOtherPlayerTransition(this)
    ..[GameState.YouLost] = YouLostTransition(this)
    ..[GameState.YourMoveRequired] = YourMoveRequiredTransition(this)
    ..[GameState.YouWon] = YouWonTransition(this);

    _showPrizeAnim = ShowPrizeAnimation(this);
    _showLossAnim = ShowLoserImageAnimation(this);
  }

  @override
  void dispose() {
    _updateQueue.clear();
    super.dispose();
  }

  /**
   * Initializes the scene for displaying and interacting with a game.
   * The previousGameStatus is used to animate transitions. The new/current game status has to be set via [GameScene.update}.
   *
   * [previousGameStatus] is the GameStatus that was previously displayed to the user or null if it was not played yet.
   */
  void initForGame(GameStatus previousGameStatus) {
    _previousGameStatus = previousGameStatus;
  }

  Future update(GameStatus newGameStatus, { bool queueOnly = false}) {
    final runQueue = _updateQueue.length == 0;
    final queueEntry = new _UpdateQueueEntry(newGameStatus);
    _updateQueue.add(queueEntry);

    if (runQueue && !queueOnly)
      processUpdateQueue();

    return queueEntry.completer.future;
  }

  void processUpdateQueue() {
    // Prevent reentrancy to this method. If we have a task of the
    // queue being processed, this task will reset the _isProcessingQueue flag when done.
    if (_isProcessingQueue) {
      return;
    }

    if (_updateQueue.length > 0) {
      // Take the next entry to process, but don't remove it yet
      // so that incoming entries don't start processing the queue
      // until the last entry is done.
      final nextEntry = _updateQueue.first;
      _isProcessingQueue = true;
      _updateInternal(nextEntry.gameStatus)
        .whenComplete(() {
          _updateQueue.remove(nextEntry);
          _isProcessingQueue = false;
        })
        .then((_) {
          nextEntry.completer.complete(null);
        }, onError: (err) {
          nextEntry.completer.completeError(err);
        })
        .whenComplete(() {
          // See if we have additional work to do.
          // Run this asynchronously so the previous update can be drawn first.
          Timer.run(processUpdateQueue);
        });
    }
  }

  Future _updateInternal(GameStatus newGameStatus) async {
    // Skip some checks on the first update. We must update the scene then.
    final isPerformingFirstUpdate = _isFirstUpdate;

    // Ignore outdated GameStatus objects.
    if (!isPerformingFirstUpdate &&
      _previousGameStatus != null && newGameStatus != null &&
      _previousGameStatus.revision >= newGameStatus.revision) {
      return;
    }

    if (!isPerformingFirstUpdate && !gsutil.hasChanges(newGameStatus, _previousGameStatus)) {
      return;
    }
    _isFirstUpdate = false;

    final hasNewRoundStarted =
      _previousGameStatus != null &&
      _previousGameStatus.rounds.length > 0 &&
      _previousGameStatus.rounds.length < newGameStatus.rounds.length;

    final animateMenu =
      newGameStatus?.state != GameState.ChallengerBetRequired && newGameStatus?.state != GameState.YourMoveRequired ||
        isPerformingFirstUpdate ||
        _previousGameStatus == null ||
        _previousGameStatus.state == GameState.AcceptanceRequired ||
        _previousGameStatus.state == GameState.InitiatedByYou ||
        hasNewRoundStarted;

    if (animateMenu) {
      // Hide the menu first when it changes.
      showMenuForState(null);
    }

    final previousStatus = _previousGameStatus;
    _previousGameStatus = GameStatus.fromJson(newGameStatus.toJson());

    // Reset asset state.
    for (final a in assets) {
      a.isVisible = false;
      a.opacity = 1;
    }

    bool hasTransitioned = false;
    if (_transitions.containsKey(newGameStatus.state)) {
      final transition = _transitions[newGameStatus.state];
      if (!await transition.apply(previousStatus, newGameStatus)) {
        return;
      }
      hasTransitioned = true;
    }

    if (hasTransitioned && animateMenu) {
      showMenuForState(newGameStatus.state);
    }
  }

  void showMenuForState(GameState state) {
    _menuHandler.showMenuForState(state);
  }

  Future showPrize(Uint8List prize) {
    return _showPrizeAnim.run(prize);
  }

  Future showLoss(Uint8List lossImage) {
    return _showLossAnim.run(lossImage);
  }

}

class _UpdateQueueEntry  {
  GameStatus gameStatus;
  Completer completer;
  _UpdateQueueEntry(this.gameStatus) {
    completer = Completer();
  }
}