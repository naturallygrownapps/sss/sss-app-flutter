import 'dart:typed_data';

import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_state_animation.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;

class ShowLoserImageAnimation extends GameStateAnimation {
  ShowLoserImageAnimation(GameScene scene) : super(scene);

  Future<bool> run(Uint8List loss) async {
    final lossImgEntity = assets.prizeOrLossImage;
    await lossImgEntity.loadImage(loss);
    lossImgEntity.initForDisplay(
      relX: gsconst.CenterXPos,
      relY: gsconst.CenterYPos,
      scale: 0,
      opacity: 1
    );

    return await lossImgEntity.animateScaleTo(0.9).runOn(animCtx, gsconst.DefaultScaleAnimDuration);
  }
}
