import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:gallery_gambling/game/game_context.dart';
import 'package:gallery_gambling/game/scene/game_assets.dart';
import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/scene_widget.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class GameScenePainter extends ScenePainter<GameScene> {
  GameStatus _lastSeenGame;
  Uint8List _lastSeenWinnerImage;
  Uint8List _lastSeenLoserImage;
  GameContext _gameCtx;

  GameScenePainter(BuildContext ctx, this._gameCtx) {
    final animCtx = AnimationContext.of(ctx);
    animCtx.animationTick = invalidate;
    final assets = GameAssets(animCtx, this);
    scene = GameScene(assets, _gameCtx);

    _lastSeenWinnerImage = _gameCtx.winnerPrize;
    _lastSeenLoserImage = _gameCtx.loserImage;
    _gameCtx.addListener(_onGameContextChanged);
    _onGameContextChanged();
  }

  void _onGameContextChanged() {
    // Only react to changes of the game, the Changelistener notifies
    // only of changes globally, not specifially of changes on certain properties.
    final newGame = _gameCtx.game;
    if (_lastSeenGame != newGame) {
      _lastSeenGame = newGame;
      // Only update immediately when we got a valid size because much of the
      // animation update code relies on knowing the canvas size.
      // Otherwise, updates are queued until we get a valid size.
      // See onGotValidSize() below.
      scene.update(newGame,
        queueOnly: !ScenePainter.isValidSize(lastSize)
      ).whenComplete(invalidate);
    }

    final newWinnerImage = _gameCtx.winnerPrize;
    if (_lastSeenWinnerImage != newWinnerImage && newWinnerImage != null) {
      _lastSeenWinnerImage = newWinnerImage;
      scene.showPrize(newWinnerImage);
    }

    final newLoserImage = _gameCtx.loserImage;
    if (_lastSeenLoserImage != newLoserImage && newLoserImage != null) {
      _lastSeenLoserImage = newLoserImage;
      scene.showLoss(newLoserImage);
    }
  }

  @override
  void onGotValidSize() {
    Timer.run(scene.processUpdateQueue);
  }

  Future init() {
    return scene.loadAssets();
  }

  @override
  void dispose() {
    _gameCtx.removeListener(_onGameContextChanged);
    super.dispose();
  }
}