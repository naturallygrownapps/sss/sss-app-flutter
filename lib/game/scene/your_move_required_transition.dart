import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/scene/drawables.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class YourMoveRequiredTransition extends GameStateTransition  {
  YourMoveRequiredTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) {
    // We have a special case here: the challenger bet is set automatically in the background when making your first move.
    // Thus, we skip the transition ChallengerBetRequired -> YourMoveRequired -> WaitingForOtherPlayer/YourMoveRequired
    // and directly transition from ChallengerBetRequired -> WaitingForOtherPlayer/YourMoveRequired.
    // In the case that we get to WaitingForOtherPlayer, we don't have to do anything as the WaitingForOtherPlayer transition is
    // unconditionally always the same.
    // We handle the case ChallengerBetRequired -> YourMoveRequired here.
    // Additionally, we handle ChallengerBetRequired -> ChallengerBetRequired when the opponent makes his move while
    // we did not place our bet yet.

    showPlayerNames(g);

    if (previousGameStatus == null ||
      previousGameStatus.state == GameState.AcceptanceRequired ||
      previousGameStatus.state == GameState.InitiatedByYou) {
      // A new game has started.
      return _applyTransitionFromAcceptanceRequired(previousGameStatus, g);
    }
    else {
      return _animateYourOrOpponentMove(previousGameStatus, g);
    }
  }

  /**
   * Initial transition to this state:
   * * hourglass for other player
   * * no icon for you
   */
  Future<bool> _applyTransitionFromAcceptanceRequired(GameStatus previousGameStatus, GameStatus g) {
    showScores(g);

    ImageDrawable otherPlayerToken;
    var round = gsutil.getActiveRound(g);
    if (round != null && round.otherPlayerMove != null) {
      otherPlayerToken = assets.unknownOtherPlayerToken;
    }
    else {
      otherPlayerToken = assets.waitOtherPlayerToken;
    }

    otherPlayerToken.initForDisplay(
      relX: gsconst.TopRightXPos,
      relY: gsconst.TopRightYPos,
      anchor: gsconst.TopRightAnchor,
      scale: gsconst.ScaleSmall
    );

    return Future.value(true);
  }

  /**
   * You made a move, the other player also provided his move, the next round begins
   * OR
   * The other player made his move, yours is still pending.
   */
  Future<bool> _animateYourOrOpponentMove(GameStatus previousGameStatus, GameStatus g) async {
    bool hasNewRoundStarted =
        previousGameStatus != null &&
        previousGameStatus.rounds.length > 0 &&
        previousGameStatus.rounds.length < g.rounds.length;
    if (hasNewRoundStarted) {
      // Animate the game move.
      if (!await animatePreviousMove(previousGameStatus, g)) {
        return false;
      }
    }
    else {
      showScores(g);
    }

    ImageDrawable newOtherPlayerToken;
    if (!hasNewRoundStarted) {
      final round = gsutil.getActiveRound(g);
      // Change the icon.
      if (round.otherPlayerMove != null) {
        final previousOtherPlayerToken = assets.waitOtherPlayerToken;
        previousOtherPlayerToken.initForDisplay(
          relX: gsconst.TopRightXPos,
          relY: gsconst.TopRightYPos,
          anchor: gsconst.TopRightAnchor,
          scale: gsconst.ScaleSmall
        );
        previousOtherPlayerToken.animateOpacityTo(0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration);
        newOtherPlayerToken = assets.unknownOtherPlayerToken;
      }
      else {
        newOtherPlayerToken = assets.waitOtherPlayerToken;
      }
    }
    else {
      newOtherPlayerToken = assets.waitOtherPlayerToken;
    }

    newOtherPlayerToken.initForDisplay(
      relX: gsconst.TopRightXPos,
      relY: gsconst.TopRightYPos,
      anchor: gsconst.TopRightAnchor,
      scale: gsconst.ScaleSmall,
      opacity: 0
    );

    return await newOtherPlayerToken.animateOpacityTo(1).runOn(animCtx, gsconst.DefaultOpacityAnimDuration);
  }
}