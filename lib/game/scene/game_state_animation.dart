import 'package:flutter/animation.dart';
import 'package:gallery_gambling/game/scene/game_assets.dart';
import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

class GameStateAnimation {
  final GameScene scene;
  GameAssets get assets => scene.assets;
  AnimationContext get animCtx => scene.assets.animCtx;
  SSSLocalizations loc;

  GameStateAnimation(this.scene) {
    loc = SSSLocalizations.of(util.getMainContext());
  }

  AnimationDescription animateMessage(String message) {
    final messageDrawable = assets.label_CenterMessage;
    messageDrawable.initForDisplay(relX: 0.5, relY: 0.5, anchor: gsconst.CenterAnchor, scale: 0);
    messageDrawable.text = message;
    final anim = CompositeAnimationDescription(id: "MessageAnimation");
    anim.add(messageDrawable.animateScaleTo(1, curve: Curves.elasticOut)
      ..beginAt = 0
      ..finishAt = 0.4
    );
    anim.add(messageDrawable.animateScale(1, 0, curve: Curves.easeInCubic)
      ..beginAt = 0.6
      ..finishAt = 1.0
    );
    return anim;
  }

  void showScores(GameStatus g) {
    final scores = gsutil.getScores(g);

    final yourScore = assets.label_Score_PlayerYou;
    final otherScore = assets.label_Score_PlayerOther;

    yourScore.initForDisplay(
      relX: gsconst.BottomRightXPos,
      relY: gsconst.BottomRightYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    yourScore.text = scores.yourScore.toString();

    otherScore.initForDisplay(
      relX: gsconst.TopLeftXPos,
      relY: gsconst.TopLeftYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    otherScore.text = scores.otherPlayerScore.toString();
  }

  void showPlayerNames(GameStatus g) {
    final yourLabel = assets.label_Name_PlayerYou;
    final otherLabel = assets.label_Name_PlayerOther;

    yourLabel.initForDisplay(
      relX: gsconst.BottomRightXPos,
      relY: gsconst.BottomRightPlayerLabelYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    yourLabel.text = scene.playerName;

    otherLabel.initForDisplay(
      relX: gsconst.TopLeftXPos,
      relY: gsconst.TopLeftPlayerLabelYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    otherLabel.text = g.otherPlayer.username;
  }

  void showOpponentName_GameNotStarted(GameStatus g) {
    final descriptionPlayerOtherLabel = assets.label_Description_PlayerOther;
    descriptionPlayerOtherLabel.initForDisplay(
      relX: gsconst.TopRightXPos,
      relY: 0,
      anchor: gsconst.TopRightAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    descriptionPlayerOtherLabel.text = loc.game_opponentlabel_gamenotstarted;

    final otherLabel = assets.label_Name_PlayerOther;
    otherLabel.initForDisplay(
      relX: gsconst.TopRightXPos,
      relY: gsconst.TopRightPlayerDescriptionLabelYPos,
      anchor: gsconst.TopRightAnchor,
      scale: 1,
      opacity: gsconst.ScoreOpacity
    );
    otherLabel.text = g.otherPlayer.username;
  }
}