import 'dart:math';

import 'package:flutter/animation.dart';
import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_state_animation.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/util_functions.dart';

abstract class GameStateTransition extends GameStateAnimation {
  GameStateTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus newGameStatus);

  Future<bool> animatePreviousMove(GameStatus previousGameStatus, GameStatus g) async {
    showScores(previousGameStatus);

    final yourToken = assets.getMoveAssetForPreviousRound(g, Player.You);
    final otherToken = assets.getMoveAssetForPreviousRound(g, Player.Other);

    yourToken.initForDisplay(
      relX: gsconst.BottomLeftXPos,
      relY: gsconst.BottomLeftYPos,
      anchor: gsconst.BottomLeftAnchor,
      scale: gsconst.ScaleSmall
    );

    final unknownOtherPlayerToken = assets.unknownOtherPlayerToken;
    unknownOtherPlayerToken.initForDisplay(
      relX: gsconst.TopRightXPos,
      relY: gsconst.TopRightYPos,
      anchor: gsconst.TopRightAnchor,
      scale: gsconst.ScaleSmall
    );

    if (previousGameStatus.state == GameState.YourMoveRequired) {
      // We had to set a move in the previous frame, animate showing our token.
      yourToken.setAnchor(gsconst.BottomRightAnchor);
      yourToken.scale = gsconst.ScaleSmall;
      yourToken.setTranslateRelative(0, gsconst.BottomLeftYPos);
      yourToken.setAnchor(gsconst.BottomLeftAnchor);
      if (!await yourToken.animateTranslationToRelative(gsconst.BottomLeftXPos, gsconst.BottomLeftYPos, curve: Curves.elasticInOut)
        .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration)) {
        return false;
      }
    }

    // Reveal the other player's token.
    otherToken.isVisible = true;
    otherToken.scale = gsconst.ScaleSmall;
    otherToken.setAnchor(gsconst.TopLeftAnchor);
    otherToken.setTranslateRelative(1, gsconst.TopRightYPos);
    otherToken.setAnchor(gsconst.TopRightAnchor);
    unknownOtherPlayerToken.animateOpacityTo(0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration ~/ 2);
    if (!await otherToken.animateTranslationToRelative(gsconst.TopRightXPos, gsconst.TopRightYPos, curve: Curves.elasticInOut)
      .runOn(animCtx, gsconst.DefaultOpacityAnimDuration)) {
      return false;
    }

    final lastRoundWinner = g.roundWinners.last;
    if (lastRoundWinner == null || lastRoundWinner == "nobody") {
      // Draw!
      if (!await waitBool([
        yourToken.animateOpacityTo(0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
        otherToken.animateOpacityTo(0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
        animateMessage(loc.game_draw).runOn(animCtx, gsconst.DefaultOpacityAnimDuration * 2),
      ])) {
        return false;
      }
    }
    else {
      // One of the players won.
      final hasWon = lastRoundWinner != g.otherPlayer.username;
      final winnerToken = hasWon ? yourToken : otherToken;
      final loserToken = hasWon ? otherToken : yourToken;

      winnerToken.setAnchor(gsconst.CenterAnchor);
      if (!await waitBool([
        loserToken.animateScaleTo(0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
        winnerToken.animateScaleTo(gsconst.ScaleBig, curve: Curves.easeOutExpo).runOn(animCtx, gsconst.DefaultTranslateInAnimDuration),
        winnerToken.animateTranslationToRelative(gsconst.CenterXPos, gsconst.CenterYPos, curve: Curves.easeOutExpo)
          .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration),
      ])) {
        return false;
      }

      await Future.delayed(Duration(milliseconds: 400));

      Point<double> scoreTargetAnchor;
      double scoreTargetX, scoreTargetY;
      if (hasWon) {
        scoreTargetAnchor = gsconst.TopLeftAnchor;
        scoreTargetX = 1;
        scoreTargetY = 1;
      }
      else {
        scoreTargetAnchor = gsconst.BottomRightAnchor;
        scoreTargetX = 0;
        scoreTargetY = 0;
      }

      winnerToken.setAnchor(scoreTargetAnchor);
      if (!await waitBool([
        winnerToken.animateTranslationToRelative(scoreTargetX, scoreTargetY, curve: Curves.easeInCubic).runOn(animCtx, gsconst.DefaultTranslateOutAnimDuration ~/ 2),
        winnerToken.animateScaleTo(0, curve: Curves.easeInCubic).runOn(animCtx, gsconst.DefaultTranslateOutAnimDuration ~/ 2),
      ])) {
        return false;
      }

      showScores(g);
    }

    if (!gsutil.isGameFinished(g)) {
      if (!await animateMessage(loc.game_newround).runOn(animCtx, gsconst.DefaultScaleAnimDuration ~/ 2)) {
        return false;
      }
    }

    return true;
  }

}