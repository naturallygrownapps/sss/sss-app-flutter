import 'package:flutter/animation.dart';
import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/game/scene/game_scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:gallery_gambling/game/scene/game_state_transition.dart';
import 'package:gallery_gambling/shared/game_status.dart';

class WaitingForOtherPlayerTransition extends GameStateTransition {
  WaitingForOtherPlayerTransition(GameScene scene) : super(scene);

  Future<bool> apply(GameStatus previousGameStatus, GameStatus g) {
    final waitForOtherPlayer = assets.waitOtherPlayerToken;

    waitForOtherPlayer.isVisible = true;
    waitForOtherPlayer.setAnchor(gsconst.TopRightAnchor);
    waitForOtherPlayer.setTranslateRelative(gsconst.TopRightXPos, gsconst.TopRightYPos);
    waitForOtherPlayer.scale = gsconst.ScaleSmall;

    showScores(g);
    showPlayerNames(g);

    final yourToken = assets.getMoveAsset(g, Player.You, gsutil.getActiveRound(g));
    if (yourToken != null) {
      yourToken.isVisible = true;
      yourToken.setAnchor(gsconst.BottomRightAnchor);
      yourToken.setTranslateRelative(0, gsconst.BottomRightYPos);
      yourToken.scale = gsconst.ScaleSmall;

      bool animate = false;
      if (previousGameStatus != null) {
        final previousStatusRound = gsutil.getActiveRound(previousGameStatus);
        if (previousStatusRound == null) {
          animate = true;
        }
        else {
          animate = previousStatusRound.yourMove == null;
        }
      }

      yourToken.setAnchor(gsconst.BottomLeftAnchor);
      if (animate) {
        return yourToken.animateTranslationToRelative(gsconst.BottomLeftXPos, gsconst.BottomLeftYPos, curve: Curves.elasticInOut)
          .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration);
      }
      else {
        yourToken.setTranslateRelative(gsconst.BottomLeftXPos, gsconst.BottomLeftYPos);
        return Future.value(true);
      }
    }

    return Future.value(true);
  }
}