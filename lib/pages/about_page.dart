import 'package:flutter/material.dart';
import 'package:gallery_gambling/util_functions.dart' as util;
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';
import 'package:gallery_gambling/routes.dart' as routes;

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var loc = SSSLocalizations.of(context);
    return BasePage(
        title: loc.about_title,
        child: ListView(
          padding: EdgeInsets.only(left: 16, right: 16),
          children: <Widget>[
            Text(loc.common_appname),
            VersionLabel(),
            Text(loc.about_author),
            HyperLink(
                linkText: "support@naturallygrownapps.de",
                url: "mailto:support@naturallygrownapps.de"),
            SSSButton(
              onPressed: () {
                util.openDocument(context, "legal/privacy-notice.html",
                    localize: true,
                    title: "");
              },
              child: Text(loc.about_button_dataprivacy),
            ),
            SSSButton(
              onPressed: () {
                util.openDocument(context, "legal/termsofuse.html",
                    localize: true,
                    title: "");
              },
              child: Text(loc.about_button_termsofuse),
            ),
            SSSButton(
              onPressed: () {
                Navigator.of(context).pushNamed(routes.licenses);
              },
              child: Text(loc.about_button_licenses),
            ),
          ],
        ));
  }
}
