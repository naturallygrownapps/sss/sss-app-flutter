import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/pages/start/intro/intro_context.dart';
import 'package:gallery_gambling/pages/start/intro/intro_scene_painter.dart';
import 'package:gallery_gambling/scene/scene_widget.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  IntroContext _introCtx;

  @override
  void initState() {
    super.initState();
    _introCtx = IntroContext();
  }

  @override
  void dispose() {
    _introCtx.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return BasePage(
      title: loc.start_menu_item_tutorial,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          children: <Widget>[
            Expanded(
                child: createScene()
            ),
            createMenu()
          ]
        ),
      )
    );
  }

  SceneWidget createScene() {
    final widget = SceneWidget((ctx) async {
      final painter = IntroScenePainter(ctx, _introCtx);
      await painter.scene.loadAssets();
      return painter;
    });
    return widget;
  }

  Widget createMenu() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: AnimatedBuilder(
        animation: _introCtx,
        builder: (ctx, _) => Row(
          mainAxisAlignment: _introCtx.isInFirstState ? MainAxisAlignment.end : MainAxisAlignment.spaceBetween,
          children: <Widget>[
            if (!_introCtx.isInFirstState) IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: _gotoPreviousStep,
            ),
            IconButton(
              icon: Icon(_introCtx.isInLastState ? Icons.done : Icons.arrow_forward),
              onPressed: () {
                if (_introCtx.isInLastState) {
                  Navigator.of(ctx).pop();
                }
                else {
                  _gotoNextStep();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  void _gotoNextStep() {
    final curStateIdx = _introCtx.state.index;
    final nextStateIdx = curStateIdx < IntroState.values.length - 1 ? curStateIdx + 1 : curStateIdx;
    _introCtx.state = IntroState.values.elementAt(nextStateIdx);
  }

  void _gotoPreviousStep() {
    final curStateIdx = _introCtx.state.index;
    final prevStateIdx = curStateIdx > 0 ? curStateIdx - 1 : curStateIdx;
    _introCtx.state = IntroState.values.elementAt(prevStateIdx);
  }


}
