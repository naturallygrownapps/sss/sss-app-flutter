import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/pages/start/intro/intro_assets.dart';
import 'package:gallery_gambling/pages/start/intro/intro_context.dart';
import 'package:gallery_gambling/pages/start/intro/intro_scene.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/scene_widget.dart';

class IntroScenePainter extends ScenePainter<IntroScene> {
  final IntroContext _introCtx;
  IntroState _lastSeenIntroState;

  IntroScenePainter(BuildContext ctx, this._introCtx) {
    final animCtx = AnimationContext.of(ctx);
    animCtx.animationTick = invalidate;
    final assets = IntroAssets(animCtx, this);
    scene = IntroScene(assets);

    _introCtx.addListener(_onIntroContextChanged);
  }

  void _onIntroContextChanged() {
    final newIntroState = _introCtx.state;
    if (_lastSeenIntroState != newIntroState) {
      _lastSeenIntroState = newIntroState;
      scene.update(newIntroState).whenComplete(invalidate);
    }
  }

  @override
  void onGotValidSize() {
    scene.update(_introCtx.state).whenComplete(invalidate);
    super.onGotValidSize();
  }

  @override
  void dispose() {
    _introCtx.removeListener(_onIntroContextChanged);
    super.dispose();
  }
}