import 'package:flutter/animation.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/start/intro/intro_assets.dart';
import 'package:gallery_gambling/pages/start/intro/intro_context.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/drawables.dart';
import 'package:gallery_gambling/scene/scene.dart';
import 'package:gallery_gambling/game/scene/game_scene_constants.dart' as gsconst;
import 'package:gallery_gambling/pages/start/intro/intro_scene_constants.dart' as inconst;
import 'package:gallery_gambling/util_functions.dart' as util;

enum _Dimension {
  Width, Height
}

class IntroScene extends SceneWithAssets<IntroAssets> {
  SSSLocalizations _loc;
  AnimationContext get animCtx => assets.animCtx;
  IntroState _currentState;
  
  IntroScene(IntroAssets assets) : super(assets) {
    _loc = SSSLocalizations.of(util.getMainContext());
  }

  Future update(IntroState introState) async {
    if (_currentState == introState) {
      return;
    }
    _currentState = introState;

    // Reset asset state.
    for (final a in assets) {
      a.isVisible = false;
      a.opacity = 1;
    }

    animCtx.abortAllAnimations();

    if (introState == IntroState.Step1_1) {
      await _animateStep1_1();
    }
    else if (introState == IntroState.Step1_2) {
      await _animateStep1_2();
    }
    else if (introState == IntroState.Step1_3) {
      await _animateStep1_3();
    }
    else if (introState == IntroState.Step2) {
      await _animateStep2();
    }
    else if (introState == IntroState.Step3_1) {
      await _animateStep3_1();
    }
    else if (introState == IntroState.Step3_2) {
      await _animateStep3_2();
    }
  }

  Future _animateStep1_1() {
    assets.sssLogo.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos, 
      scale: 0.2
    );
    assets.textblock1.text = _loc.intro_step1_1;
    assets.textblock1.initForDisplay(
      relX: -0.5, 
      relY: inconst.TextStartYPos, 
      anchor: gsconst.TopCenterAnchor
    );

    return Future.wait([
      assets.sssLogo.animateScaleTo(1.0, curve: Curves.elasticOut)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.textblock1.animateTranslationToRelative(gsconst.BottomCenterXPos, inconst.TextStartYPos)
        .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration)
    ]);
  }
  
  Future _animateStep1_2() async {
    assets.centerArena.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos, 
      scale: 1.0, 
      opacity: 0.2
    );
    assets.textblock1.text = _loc.intro_step1_2;
    assets.textblock1.initForDisplay(
      relX: -0.5, 
      relY: inconst.TextStartYPos, 
      anchor: gsconst.TopCenterAnchor
    );

    assets.phonePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: 1.0, 
      opacity: 0
    );
    assets.phonePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: 1.0, 
      opacity: 0
    );

    assets.betPlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos + assets.phonePlayer1.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: 0, 
      opacity: 1
    );
    assets.betPlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos - assets.phonePlayer2.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: 0, 
      opacity: 1
    );

    if (!await util.waitBool([
      assets.centerArena.animateOpacityTo(1.0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
      assets.phonePlayer1.animateOpacityTo(1.0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
      assets.phonePlayer2.animateOpacityTo(1.0).runOn(animCtx, gsconst.DefaultOpacityAnimDuration),
      assets.textblock1.animateTranslationToRelative(gsconst.BottomCenterXPos, inconst.TextStartYPos)
        .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration)
    ])) {
      return;
    }

    final betScale = _getScaleToFit(assets.phonePlayer1, assets.betPlayer1, _Dimension.Height);

    await Future.wait([
      assets.betPlayer1.animateScaleTo(betScale, curve: Curves.elasticOut)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.betPlayer2.animateScaleTo(betScale, curve: Curves.elasticOut)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration)
    ]);
  }

  Future _animateStep1_3() {
    assets.centerArena.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos, 
      scale: 1.0, 
      opacity: 1
    );
    assets.textblock1.text = _loc.intro_step1_3;
    assets.textblock1.initForDisplay(
      relX: -0.5, 
      relY: inconst.TextStartYPos, 
      anchor: gsconst.TopCenterAnchor
    );

    assets.phonePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: 1.0, 
      opacity: 1
    );
    assets.phonePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: 1.0, 
      opacity: 1
    );

    final betInitialScale = _getScaleToFit(assets.phonePlayer1, assets.betPlayer1, _Dimension.Height);

    assets.betPlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos + assets.phonePlayer1.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: betInitialScale, 
      opacity: 1
    );
    assets.betPlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos - assets.phonePlayer2.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: betInitialScale, 
      opacity: 1
    );

    assets.betPlayer1.setAnchor(gsconst.BottomCenterAnchor);
    assets.betPlayer2.setAnchor(gsconst.TopCenterAnchor);

    return Future.wait([
      assets.textblock1.animateTranslationToRelative(gsconst.BottomCenterXPos, inconst.TextStartYPos)
        .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration),
      
      assets.betPlayer1.animateScaleTo(inconst.BetCenterScale, curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.betPlayer2.animateScaleTo(inconst.BetCenterScale, curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),

      assets.betPlayer1.animateOpacityTo(0.5, curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.betPlayer2.animateOpacityTo(0.5, curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),

      assets.betPlayer1.animateTranslationToRelative(
          gsconst.TopCenterXPos, 
          inconst.IntroContentCenterYPos * 0.99,
          curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.betPlayer2.animateTranslationToRelative(
          gsconst.TopCenterXPos, 
          inconst.IntroContentCenterYPos * 1.01,
          curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration)
    ]);
  }
  
  Future _animateStep2() async {
    assets.centerArena.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos, 
      scale: 1.0,
      opacity: 1
    );

    assets.textblock1.text = _loc.intro_step2;
    assets.textblock1.initForDisplay(
      relX: -0.5, 
      relY: inconst.TextStartYPos, 
      anchor: gsconst.TopCenterAnchor
    );

    assets.phonePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: 1.0, 
      opacity: 1
    );
    assets.phonePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: 1.0, 
      opacity: 1
    );

    assets.betPlayer1.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos * 0.99, 
      anchor: gsconst.BottomCenterAnchor, 
      scale: inconst.BetCenterScale, 
      opacity: 0.5
    );
    assets.betPlayer2.initForDisplay(
      relX: gsconst.TopCenterXPos, 
      relY: inconst.IntroContentCenterYPos * 1.01, 
      anchor: gsconst.TopCenterAnchor, 
      scale: inconst.BetCenterScale, 
      opacity: 0.5
    );

    final tokenScale = _getScaleToFit(assets.phonePlayer1, assets.tokenPlayer1, _Dimension.Height);

    assets.textblock1.animateTranslationToRelative(gsconst.BottomCenterXPos, inconst.TextStartYPos)
      .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration);

    assets.tokenPlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos + assets.phonePlayer1.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentTopYPos, 
      anchor: gsconst.TopLeftAnchor, 
      scale: tokenScale,
      opacity: 1
    );
    assets.tokenPlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos - assets.phonePlayer2.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05, 
      relY: inconst.IntroContentBottomYPos, 
      anchor: gsconst.BottomRightAnchor, 
      scale: tokenScale, 
      opacity: 1
    );

    await Future.delayed(Duration(milliseconds: 500));

    assets.tokenPlayer1.setAnchor(gsconst.CenterAnchor);
    assets.tokenPlayer2.setAnchor(gsconst.CenterAnchor);

    await Future.wait([
      assets.tokenPlayer1.animateScaleTo(tokenScale * 1.5, curve: Curves.elasticOut)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),
      assets.tokenPlayer2.animateScaleTo(0, curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration),

      assets.tokenPlayer1.animateTranslationToRelative(
          gsconst.TopCenterXPos, 
          inconst.IntroContentCenterYPos,
          curve: Curves.easeOutCubic)
        .runOn(animCtx, gsconst.DefaultScaleAnimDuration)
    ]);
  }
  
  Future _animateStep3_1() async {
    assets.centerArena.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos,
      scale: 1.0,
      opacity: 1
    );

    assets.textblock1.text = _loc.intro_step3_1;
    assets.textblock1.initForDisplay(
      relX: -0.5,
      relY: inconst.TextStartYPos,
      anchor: gsconst.TopCenterAnchor
    );

    assets.phonePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos,
      relY: inconst.IntroContentTopYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 1.0,
      opacity: 1
    );
    assets.phonePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos,
      relY: inconst.IntroContentBottomYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 1.0,
      opacity: 1
    );

    assets.betPlayer1.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos * 0.99,
      anchor: gsconst.BottomCenterAnchor,
      scale: inconst.BetCenterScale,
      opacity: 0.5
    );
    assets.betPlayer2.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos * 1.01,
      anchor: gsconst.TopCenterAnchor,
      scale: inconst.BetCenterScale,
      opacity: 0.5
    );
    // for later animation:
    assets.betPlayer2.setAnchor(gsconst.CenterAnchor);

    assets.emotePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos + assets.phonePlayer1.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05,
      relY: inconst.IntroContentTopYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 0,
      opacity: 1
    );
    assets.emotePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos - assets.phonePlayer2.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05,
      relY: inconst.IntroContentBottomYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 0,
      opacity: 1
    );

    assets.loserBetImage.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: 1,
      opacity: 0
    );

    // Match the size of the revealed image to the icon image.
    final revealedBetScale = _getScaleToFit(assets.centerArena, assets.betPlayer1, _Dimension.Width);

    const int AnimLength = 1000;

    if (!await util.waitBool([
      assets.textblock1.animateTranslationToRelative(
          gsconst.BottomCenterXPos,
          inconst.TextStartYPos)
        .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration),

      // show the loser's bet
      assets.betPlayer2.animateTranslationToRelative(
          gsconst.TopCenterXPos,
          inconst.IntroContentCenterYPos,
          curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength),
      assets.betPlayer2.animateScaleTo(revealedBetScale, curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength),
      assets.betPlayer2.animateOpacityTo(1, curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength),

      // hide the winner's bet
      assets.betPlayer1.animateOpacityTo(0, curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength)
    ])) {
      return;
    }

    final loserBetImageTargetScale = _getScaleToFit(assets.betPlayer2, assets.loserBetImage, _Dimension.Width);
    assets.loserBetImage.scale = loserBetImageTargetScale;

    await Future.wait([
      // show the emojis
      assets.emotePlayer1.animateScaleTo(0.9, curve: Curves.elasticOut)
        .runOn(animCtx, AnimLength),
      assets.emotePlayer2.animateScaleTo(0.9, curve: Curves.elasticOut)
        .runOn(animCtx, AnimLength),

      // show the actual bet image
      assets.betPlayer2.animateOpacityTo(0, curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength),
      assets.loserBetImage.animateOpacityTo(1, curve: Curves.easeOutCubic)
        .runOn(animCtx, AnimLength)
    ]);
  }

  Future _animateStep3_2() {
    assets.centerArena.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos,
      scale: 1.0,
      opacity: 1
    );

    assets.textblock1.text = _loc.intro_step3_2;
    assets.textblock1.initForDisplay(
      relX: -0.5,
      relY: inconst.TextStartYPos,
      anchor: gsconst.TopCenterAnchor
    );

    assets.phonePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos,
      relY: inconst.IntroContentTopYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 1.0,
      opacity: 1
    );
    assets.phonePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos,
      relY: inconst.IntroContentBottomYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 1.0,
      opacity: 1
    );

    assets.emotePlayer1.initForDisplay(
      relX: gsconst.TopLeftXPos + assets.phonePlayer1.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05,
      relY: inconst.IntroContentTopYPos,
      anchor: gsconst.TopLeftAnchor,
      scale: 0.9,
      opacity: 1
    );
    assets.emotePlayer2.initForDisplay(
      relX: gsconst.BottomRightXPos - assets.phonePlayer2.actualWidth.getRelativeValueHorizontal(assets.canvasCtx) * 1.05,
      relY: inconst.IntroContentBottomYPos,
      anchor: gsconst.BottomRightAnchor,
      scale: 0.9,
      opacity: 1
    );

    final loserBetImageTargetScale =_getScaleToFit(assets.centerArena, assets.loserBetImage, _Dimension.Width);
    assets.loserBetImage.initForDisplay(
      relX: gsconst.TopCenterXPos,
      relY: inconst.IntroContentCenterYPos,
      anchor: gsconst.CenterAnchor,
      scale: loserBetImageTargetScale,
      opacity: 1
    );

    return assets.textblock1.animateTranslationToRelative(gsconst.BottomCenterXPos, inconst.TextStartYPos)
      .runOn(animCtx, gsconst.DefaultTranslateInAnimDuration);
  }

  double _getScaleToFit(ImageDrawable targetScaleDrawable, ImageDrawable drawableToScale, _Dimension dim)
  {
    final o = dim == _Dimension.Width ? 
      targetScaleDrawable.actualWidth.getAbsoluteValueHorizontal(assets.canvasCtx) : 
      targetScaleDrawable.actualHeight.getAbsoluteValueVertical(assets.canvasCtx);
    final canvasRect = assets.canvasCtx.canvasRect;
    final screen = dim == _Dimension.Width ? canvasRect.width : canvasRect.height;
    final t = dim == _Dimension.Width ? 
      drawableToScale.originalWidth.getRelativeValueHorizontal(assets.canvasCtx) : 
      drawableToScale.originalHeight.getRelativeValueVertical(assets.canvasCtx);

    return o / (screen * t);
  }

}