import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/assets.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/drawables.dart';

class IntroAssets extends Assets {
  IntroAssets(AnimationContext animCtx, CanvasContext canvasCtx) : super(animCtx, canvasCtx);

  ImageDrawable _sssLogo;
  ImageDrawable get sssLogo => _sssLogo;
  ImageDrawable _phonePlayer1;
  ImageDrawable get phonePlayer1 => _phonePlayer1;
  ImageDrawable _phonePlayer2;
  ImageDrawable get phonePlayer2 => _phonePlayer2;
  ImageDrawable _betPlayer1;
  ImageDrawable get betPlayer1 => _betPlayer1;
  ImageDrawable _betPlayer2;
  ImageDrawable get betPlayer2 => _betPlayer2;
  ImageDrawable _centerArena;
  ImageDrawable get centerArena => _centerArena;
  ImageDrawable _tokenPlayer1;
  ImageDrawable get tokenPlayer1 => _tokenPlayer1;
  ImageDrawable _tokenPlayer2;
  ImageDrawable get tokenPlayer2 => _tokenPlayer2;
  ImageDrawable _emotePlayer1;
  ImageDrawable get emotePlayer1 => _emotePlayer1;
  ImageDrawable _emotePlayer2;
  ImageDrawable get emotePlayer2 => _emotePlayer2;
  
  ImageDrawable _loserBetImage;
  ImageDrawable get loserBetImage => _loserBetImage;
  
  TextDrawable _textblock1;
  TextDrawable get textblock1 => _textblock1;
  TextDrawable _textblock2;
  TextDrawable get textblock2 => _textblock2;

  @override
  Stream<DrawableEntity> loadAssets() async* {
    yield _sssLogo = await loadImageFromAsset("game/logo.png");
    yield _phonePlayer1 = await loadImageFromAsset("intro/icon-phone.png");
    yield _phonePlayer2 = await loadImageFromAsset("intro/icon-phone.png");
    yield _centerArena = await loadImageFromAsset("intro/bg-circle.png");
    yield _betPlayer1 = await loadImageFromAsset("intro/picture.png");
    yield _betPlayer2 = await loadImageFromAsset("intro/picture.png");
    yield _tokenPlayer1 = await loadImageFromAsset("game/paper.png");
    yield _tokenPlayer2 = await loadImageFromAsset("game/rock.png");
    yield _loserBetImage = await loadImageFromAsset("intro/lost-bet.png");
    yield _emotePlayer1 = await loadImageFromAsset("intro/player1-winner.png");
    yield _emotePlayer2 = await loadImageFromAsset("intro/player2-loser.png");

    yield _textblock1 = createTextAsset("intro/text1")
      ..textSize = 32
      ..lineHeightFactor = 1.5
      ..horizontalMargin = 4;
    yield _textblock2 = createTextAsset("intro/text2")
      ..textSize = 32
      ..lineHeightFactor = 1.5
      ..horizontalMargin = 4;
  }












}