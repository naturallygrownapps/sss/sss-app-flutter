import 'package:flutter/widgets.dart';

enum IntroState {
  Step1_1,
  Step1_2,
  Step1_3,

  Step2,

  Step3_1,
  Step3_2
}

class IntroContext with ChangeNotifier {
  IntroState _state;
  IntroState get state => _state;
  set state(IntroState newState) {
    _state = newState;
    notifyListeners();
  }

  IntroContext() {
    _state = IntroState.Step1_1;
  }

  bool get isInFirstState => state == IntroState.values.first;
  bool get isInLastState => state == IntroState.values.last;
}