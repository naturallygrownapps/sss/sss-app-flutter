import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gallery_gambling/exceptions/authentication_exception.dart';
import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/start/register_page.dart';
import 'package:gallery_gambling/routes.dart' as routes;
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/dynamic_links_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/validators.dart' as validation;
import 'package:gallery_gambling/widgets/drawer_button.dart';
import 'package:gallery_gambling/widgets/spinner_container.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';

bool hasShownIntroFirst = false;

class LoginPage extends StatefulWidget {
  final bool startWithIntro;

  LoginPage({Key key, this.title, this.startWithIntro}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  FocusNode _passwordFocus = FocusNode();
  FocusNode _usernameFocus = FocusNode();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _autovalidate = false;
  final DynamicLinksService _dynamicLinksService = getService<DynamicLinksService>();
  StreamSubscription _dynamicLinksSubscription;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_onInitialLayoutDone);
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _usernameFocus.dispose();
    _passwordFocus.dispose();
    _dynamicLinksSubscription?.cancel();
    _dynamicLinksSubscription = null;
    super.dispose();
  }

  void _onInitialLayoutDone(Duration d) async {
    if (widget.startWithIntro && !hasShownIntroFirst) {
      hasShownIntroFirst = true;
      Navigator.of(context).pushNamed(routes.intro);
    }

    _dynamicLinksSubscription = _dynamicLinksService.links.listen(_onDynamicLink);
  }

  void _onDynamicLink(DynamicLink dynamicLink) {
    if (dynamicLink.tryProcessBy("LoginPage")) {
      final inviteLinkInfo = InviteLink.tryParse(dynamicLink.link);
      if (inviteLinkInfo != null) {
        register();
      }
    }
  }

  _login() async {
    try {
      await getService<LoadingTracker>().track(getService<AuthService>()
          .login(_usernameController.text?.trim(), _passwordController.text));
    } on AuthenticationException catch (e) {
      final loc = SSSLocalizations.of(context);
      throw new UserFacingException(loc.login_invalidlogin, e);
    }
  }

  register() async {
    final registerPageResult = await Navigator.push<RegisterPageResult>(
        context, new MaterialPageRoute<RegisterPageResult>(builder: (ctx) => new RegisterPage()));
    if (registerPageResult != null) {
      _usernameController.text = registerPageResult.registeredUsername;
      _passwordController.text = registerPageResult.registeredPassword;
      _login();
    }
  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);

    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      drawer: new _LoginPageDrawer(),
      body: SpinnerContainer(
        child: DecoratedBox(
          decoration: BoxDecoration(
              image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/images/background.png"),
          )),
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  height: screenSize.height,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 32, left: 16, right: 16, bottom: 16),
                    child: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(32),
                        child: Image.asset("assets/images/logo-typography.png"),
                      ),
                      Text(loc.login_newhere),
                      SSSButton(child: Text(loc.login_register), onPressed: register),
                      Text(loc.login_alreadyregistered),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Form(
                          autovalidate: _autovalidate,
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                textInputAction: TextInputAction.next,
                                focusNode: _usernameFocus,
                                controller: _usernameController,
                                validator: validation.notEmpty(loc.login_missing_username),
                                decoration: InputDecoration(labelText: loc.common_username),
                                onFieldSubmitted: (input) {
                                  _usernameFocus.unfocus();
                                  FocusScope.of(context).requestFocus(_passwordFocus);
                                },
                              ),
                              TextFormField(
                                textInputAction: TextInputAction.done,
                                obscureText: true,
                                focusNode: _passwordFocus,
                                controller: _passwordController,
                                validator: validation.notEmpty(loc.login_missing_password),
                                decoration: InputDecoration(labelText: loc.common_password),
                                onFieldSubmitted: (_) {
                                  _login();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: SSSButton(
                            child: Text(loc.login_submit),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _login();
                              } else {
                                setState(() {
                                  _autovalidate = true;
                                });
                              }
                            }),
                      )
                    ]),
                  ),
                ),
              ),
              Positioned(
                child: DrawerButton(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _LoginPageDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return SSSDrawer(
      child: Column(
        children: <Widget>[
          DrawerListTile(
            text: loc.start_menu_item_tutorial,
            onPressed: () {
              Navigator.popAndPushNamed(context, routes.intro);
            },
          ),
          DrawerListTile(
            text: loc.common_menu_item_about,
            onPressed: () {
              Navigator.popAndPushNamed(context, routes.about);
            },
          ),
          //ListTile(title: Text(loc.main_menu_item_feedback))
        ],
      ),
    );
  }
}

//class _AnimatedLogo extends StatefulWidget {
//  @override
//  _AnimatedLogoState createState() => _AnimatedLogoState();
//}
//
//class _AnimatedLogoState extends State<_AnimatedLogo>
//    with SingleTickerProviderStateMixin {
//  double screenWidth;
//  AnimationController _controller;
//  Animation<Offset> _imagePosition;
//
//  @override
//  void initState() {
//    super.initState();
//
//    _controller = AnimationController(
//        duration: const Duration(milliseconds: 15000), vsync: this);
//
//    _imagePosition = Tween<Offset>(begin: Offset(0, 0), end: Offset(-1, 0))
//        .chain(CurveTween(curve: Curves.easeInOut))
//        .animate(_controller)
//          ..addStatusListener((status) {
//            if (status == AnimationStatus.completed) {
//              _controller.forward(from: 0);
//            }
//          });
//
//    _controller.forward();
//  }
//
//  @override
//  void didChangeDependencies() {
//    super.didChangeDependencies();
//    screenWidth = MediaQuery.of(context).size.width;
//  }
//
//  @override
//  dispose() {
//    _controller?.dispose();
//    super.dispose();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final img = Image.asset("assets/images/collage.jpg");
//    return SlideTransition(
//      position: _imagePosition,
//      child: Stack(overflow: Overflow.visible, children: [
//        Positioned(left: screenWidth, right: -screenWidth, child: img),
//        img
//      ]),
//    );
//  }
//}
