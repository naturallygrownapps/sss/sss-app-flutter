import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/routes.dart' as routes;
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/theme.dart';
import 'package:gallery_gambling/util_functions.dart' as util;
import 'package:gallery_gambling/validators.dart' as validation;
import 'package:gallery_gambling/widgets/sss_widgets.dart';

class RegisterPageResult {
  final String registeredUsername;
  final String registeredPassword;

  RegisterPageResult(this.registeredUsername, this.registeredPassword);
}

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  FocusNode _passwordFocus = FocusNode();
  FocusNode _usernameFocus = FocusNode();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _autovalidate = false;
  bool _hasConfirmedRegistration = false;

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  register() async {
    if (!await _showConfirmDialog(context)) {
      return;
    }

    try {
      await getService<LoadingTracker>().track(
          getService<AuthService>().register(_usernameController.text, _passwordController.text));
      Navigator.pop(
          context, new RegisterPageResult(_usernameController.text, _passwordController.text));
    } on ServiceException catch (e) {
      if (e.statusCode == 409) {
        var loc = SSSLocalizations.of(context);
        throw new UserFacingException(loc.register_usernametaken, e);
      }
      rethrow;
    }

    _hasConfirmedRegistration = false;
  }

  openIntro() {
    Navigator.pushNamed(context, routes.intro);
  }

  showTermsOfUse() {
    util.openDocument(context, "legal/termsofuse.html", localize: true, title: "");
  }

  showDataPrivacy() {
    util.openDocument(context, "legal/privacy-notice.html", localize: true, title: "");
  }

  @override
  Widget build(BuildContext context) {
    var loc = SSSLocalizations.of(context);

    return BasePage(
      title: loc.register_title,
      child: Stack(overflow: Overflow.visible, children: <Widget>[
        SingleChildScrollView(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Form(
                autovalidate: _autovalidate,
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    TextFormField(
                      textInputAction: TextInputAction.next,
                      focusNode: _usernameFocus,
                      controller: _usernameController,
                      validator: validation.concat([
                        validation.minChars(3, loc.register_insufficientusername),
                        validation.usernameChars(loc.register_insufficientusername),
                      ]),
                      decoration: InputDecoration(
                        labelText: loc.common_username,
                        errorMaxLines: 10,
                        contentPadding: EdgeInsets.only(bottom: 2.0),
                      ),
                      onFieldSubmitted: (input) {
                        _usernameFocus.unfocus();
                        FocusScope.of(context).requestFocus(_passwordFocus);
                      },
                    ),
                    if (!_autovalidate)
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text(loc.register_insufficientusername,
                            style: Theme.of(context).primaryTextTheme.caption),
                      ),
                    Container(
                      margin: EdgeInsets.only(top: 4.0),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        focusNode: _passwordFocus,
                        controller: _passwordController,
                        obscureText: true,
                        autovalidate: _autovalidate,
                        validator: validation.minChars(5, loc.register_insufficientpassword),
                        decoration: InputDecoration(
                          labelText: loc.common_password,
                          errorMaxLines: 10,
                          contentPadding: EdgeInsets.only(top: 4.0, bottom: 2.0),
                        ),
                      ),
                    ),
                    if (!_autovalidate)
                      Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Text(loc.register_insufficientpassword,
                            style: Theme.of(context).primaryTextTheme.caption),
                      ),
                    Container(
                      margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Column(
                        children: <Widget>[
                          RichText(text: _renderTermsOfUseNotice(loc, Theme.of(context).textTheme)),
                        ],
                      ),
                    ),
                    SSSButton(
                        child: Text(loc.register_submit),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            register();
                          } else {
                            setState(() {
                              _autovalidate = true;
                            });
                          }
                        }),
                  ],
                ))),
      ]),
    );
  }

  TextSpan _renderTermsOfUseNotice(SSSLocalizations loc, TextTheme textTheme) {
    final parts = loc.register_terms_text.split("%");
    return TextSpan(style: textTheme.bodyText2, children: <TextSpan>[
      TextSpan(text: parts[0]),
      TextSpan(
          text: loc.register_termsofuse,
          style: textTheme.bodyText2
              .apply(decoration: TextDecoration.underline, color: SSSColors.cyan),
          recognizer: TapGestureRecognizer()..onTap = showTermsOfUse),
      TextSpan(text: parts[1]),
      TextSpan(
          text: loc.register_dataprivacy,
          style: textTheme.bodyText2
              .apply(decoration: TextDecoration.underline, color: SSSColors.cyan),
          recognizer: TapGestureRecognizer()..onTap = showDataPrivacy),
      TextSpan(text: parts[2])
    ]);
  }

  Future<bool> _showConfirmDialog(BuildContext context) async {
    if (_hasConfirmedRegistration) {
      return true;
    }
    final dialogResult = await showDialog<bool>(
        context: context,
        builder: (builder) {
          final loc = SSSLocalizations.of(builder);
          return AlertDialog(
            title: Text(loc.register_confirm_title),
            content: Text(loc.register_confirm_text,
                style: Theme.of(builder).primaryTextTheme.subtitle2),
            actions: [
              RaisedButton(
                child: Text(loc.common_yes),
                onPressed: () {
                  Navigator.of(builder).pop(true);
                },
              ),
              RaisedButton(
                child: Text(loc.common_no),
                onPressed: () {
                  Navigator.of(builder).pop(false);
                },
              ),
            ],
          );
        });

    return _hasConfirmedRegistration = dialogResult ?? false;
  }
}
