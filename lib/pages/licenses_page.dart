import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';

const _mediaLicenseFiles = {
  "Rock, Paper, Scissors icons by John Redman": "icons_rock_paper_scissors.txt",
  "Helsinki font by Vic Fieger": "font_helsinki.txt",
  "Cabin font": "font_cabin_sil.txt",
  "Material Design icons by Google": "icons_material.txt",
  '"Hourglass" and "Block" icons from Entypo by Daniel Bruce': "icons_entypo.txt",
  '"Gift" icon from Typicons by Stephen Hutchings': "icons_typicons.txt",
  "Noto Emojis by Google": "icons_noto-emojis.txt",
  "Smartphone icon by Skoll": "icons_smartphone.txt",
  '"Surprised cat" image by tevenet from Pixabay': "image_surprisedcat.txt"
};

class SingleLicenseTextPage extends StatelessWidget {
  final String _title;
  final List<LicenseParagraph> _licenseParagraphs;

  SingleLicenseTextPage(this._title, this._licenseParagraphs);

  @override
  Widget build(BuildContext context) {
    var loc = SSSLocalizations.of(context);
    return BasePage(
        title: _title + " " + loc.license_single_title,
        child: Padding(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: ListView(
              children: _licenseParagraphs.map((p) {
            if (p.indent == LicenseParagraph.centeredIndent) {
              return Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Text(
                  p.text,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              );
            } else {
              return Padding(
                padding: EdgeInsetsDirectional.only(top: 8.0, start: 16.0 * p.indent),
                child: p.text != null && p.text.startsWith("http")
                    ? HyperLink(
                        linkText: p.text,
                        url: p.text,
                      )
                    : Text(p.text),
              );
            }
          }).toList()),
        ));
  }
}

class LicensesPage extends StatefulWidget {
  @override
  _LicensesPageState createState() => _LicensesPageState();
}

class _LicensesPageState extends State<LicensesPage> {
  List<Widget> _mediaLicenseEntries = List();
  List<Widget> _softwareLicenseEntries = List();

  @override
  void initState() {
    // Getting the licenses from the registry is a bit slow so better show a spinner meanwhile.
    getService<LoadingTracker>().track(
        Future.wait([_getMediaLicenses(), _getSoftwareLicenses()])
            .then((loadedLicenses) => [
                  _renderLicenseList(context, loadedLicenses[0], false),
                  _renderLicenseList(context, loadedLicenses[1], true)
                ])
            .then((licenseEntryWidgets) {
          setState(() {
            _mediaLicenseEntries = licenseEntryWidgets[0];
            _softwareLicenseEntries = licenseEntryWidgets[1];
          });
        }),
        message: "Loading licenses");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    final textHeadStyle = Theme.of(context).textTheme.subtitle1;

    return BasePage(
        title: loc.licenses_title,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("Made with", style: textHeadStyle),
                      Padding(
                        padding: const EdgeInsets.only(top: 8, bottom: 4),
                        child: FlutterLogo(style: FlutterLogoStyle.stacked, size: 40),
                      ),
                      Text("and the following:", style: textHeadStyle),
                    ],
                  ),
                ),
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text("Media", style: textHeadStyle),
              ),
              Column(
                children: _mediaLicenseEntries,
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text("Software", style: textHeadStyle),
              ),
              Column(
                children: _softwareLicenseEntries,
              ),
            ],
          ),
        ));
  }

  Future<Map<String, List<LicenseParagraph>>> _getMediaLicenses() async {
    final result = LinkedHashMap<String, List<LicenseParagraph>>();
    for (final packageName in _mediaLicenseFiles.keys) {
      final licenseFilename = _mediaLicenseFiles[packageName];
      final licenseText = await rootBundle.loadString("assets/docs/licenses/$licenseFilename");
      result.putIfAbsent(packageName,
          () => LicenseEntryWithLineBreaks([packageName], licenseText).paragraphs.toList());
    }
    return result;
  }

  Future<Map<String, List<LicenseParagraph>>> _getSoftwareLicenses() async {
    final pkgsWithLicenses = LinkedHashMap<String, List<LicenseParagraph>>();
    await for (final license in LicenseRegistry.licenses) {
      for (final pkg in license.packages) {
        final licensesForPkg =
            pkgsWithLicenses.putIfAbsent(pkg, () => new List<LicenseParagraph>());
        licensesForPkg.addAll(license.paragraphs);
      }
    }
    return pkgsWithLicenses;
  }

  List<Widget> _renderLicenseList(
      BuildContext context, Map<String, List<LicenseParagraph>> pkgsWithLicenses, bool sortByName) {
    final licenseWidgets = new List<Widget>();

    final pkgs = pkgsWithLicenses.keys.where((p) => isNotNullOrEmpty(p)).toList();
    if (sortByName) {
      pkgs.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
    }

    for (final pkg in pkgs) {
      licenseWidgets.add(ListTile(
        title: Text(
          pkg,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        subtitle: Text(
          _getFirstParagraphText(pkgsWithLicenses[pkg]),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.caption,
        ),
        trailing: Icon(Icons.arrow_forward),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (ctx) => SingleLicenseTextPage(pkg, pkgsWithLicenses[pkg])));
        },
      ));
    }

    return licenseWidgets;
  }

  static String _getFirstParagraphText(List<LicenseParagraph> pars) {
    if (pars == null || pars.length == 0) {
      return "";
    } else {
      return pars.first.text ?? "";
    }
  }
}
