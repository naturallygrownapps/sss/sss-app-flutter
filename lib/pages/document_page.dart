import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;

class DocumentPage extends StatelessWidget {
  final String docContent;
  final String title;
  DocumentPage({@required this.docContent, this.title});

  @override
  Widget build(BuildContext context) {
    return BasePage(
        title: title ?? SSSLocalizations.of(context).documents_title_general,
        child: SingleChildScrollView(
          child: Html(
            data: this.docContent,
            padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
            onLinkTap: (url) async {
              if (await canLaunch(url)) {
                await launch(url);
              }
            },
            customTextStyle: (dom.Node node, TextStyle baseStyle) {
              if (node is dom.Element) {
                switch (node.localName) {
                  case "strong":
                    return baseStyle.merge(TextStyle(fontWeight: FontWeight.w900));
                }
              }
              return baseStyle;
            },
          ),
        )
    );
  }
}
