import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/loggedin/game_page.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/local_game_service.dart';
import 'package:gallery_gambling/services/local_players_service.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/shared/dto/user_entity_dto.dart';

mixin GameStarter<T extends StatefulWidget> on State<T> {
  final LocalGameService _localGameService = getService<LocalGameService>();
  final LocalPlayersService _localPlayersService = getService<LocalPlayersService>();
  final LoadingTracker _loadingTracker = getService<LoadingTracker>();

  void _popToRoot() {
    Navigator.popUntil(context, ModalRoute.withName("/"));
  }

  Future startGame(UserEntityDto otherPlayer, {bool initiatedByInvite = false}) async {
    if (await _showStartGameConfirmDialog(otherPlayer, initiatedByInvite: initiatedByInvite)) {
      final startedGame =
          await _loadingTracker.track(_localGameService.startGame(otherPlayer.username));
      if (!startedGame.isExistingGame) {
        await _localPlayersService.addRecentOpponent(otherPlayer);
        _popToRoot();
        Navigator.push(
            context, new MaterialPageRoute(builder: (ctx) => GamePage(startedGame.gameStatus)));
      } else {
        if (await _showOpenExistingGameConfirmDialog(otherPlayer)) {
          _popToRoot();
          Navigator.push(
              context, new MaterialPageRoute(builder: (ctx) => GamePage(startedGame.gameStatus)));
        }
      }
    }
  }

  Future<bool> _showStartGameConfirmDialog(UserEntityDto otherPlayer,
      {bool initiatedByInvite = false}) async {
    final dialogResult = await showDialog<bool>(
        context: context,
        builder: (builder) {
          final loc = SSSLocalizations.of(builder);
          return AlertDialog(
            title: Text(loc.opponentselection_selectopponent_confirm_title),
            content: Text(
                initiatedByInvite
                    ? loc.opponentselection_selectopponent_confirm_message_invited
                        .replaceFirst("{0}", otherPlayer.username)
                    : loc.opponentselection_selectopponent_confirm_message
                        .replaceFirst("{0}", otherPlayer.username),
                style: Theme.of(builder).primaryTextTheme.subtitle2),
            actions: [
              RaisedButton(
                child: Text(loc.common_yes),
                onPressed: () {
                  Navigator.of(builder).pop(true);
                },
              ),
              RaisedButton(
                child: Text(loc.common_no),
                onPressed: () {
                  Navigator.of(builder).pop(false);
                },
              ),
            ],
          );
        });

    return dialogResult ?? false;
  }

  Future<bool> _showOpenExistingGameConfirmDialog(UserEntityDto otherPlayer) async {
    final dialogResult = await showDialog<bool>(
        context: context,
        builder: (builder) {
          final loc = SSSLocalizations.of(builder);
          return AlertDialog(
            title: Text(loc.opponentselection_gameexists_title),
            content: Text(
                loc.opponentselection_gameexists_message.replaceFirst("{0}", otherPlayer.username),
                style: Theme.of(builder).primaryTextTheme.subtitle2),
            actions: [
              RaisedButton(
                child: Text(loc.opponentselection_gameexists_yes),
                onPressed: () {
                  Navigator.of(builder).pop(true);
                },
              ),
              RaisedButton(
                child: Text(loc.opponentselection_gameexists_no),
                onPressed: () {
                  Navigator.of(builder).pop(false);
                },
              ),
            ],
          );
        });

    return dialogResult ?? false;
  }
}
