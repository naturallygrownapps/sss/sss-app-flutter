import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_gambling/services/crypt_helper.dart';

// flutter_sodium doesn't work in the test environment, maybe because it doesn't bring the required native bindings.
// To run the tests on device, I couldn't get flutter_driver to work.
// So this is a manual implementation to run some crypto tests.

class SSSTestException implements Exception {
  final message;

  SSSTestException({ this.message });

  String toString() {
    if (message == null) return "Test failed";
    return "Test failed: $message";
  }
}

void equals(Uint8List expected, Uint8List actual) {
  if (base64.encode(expected) != base64.encode(actual)) {
    throw new SSSTestException();
  }
}

void throwsAsync<T>(AsyncCallback f, { bool Function(T e) exceptionMatcher }) async {
  try {
    await f();
    throw new SSSTestException(message: "Test function should have thrown a ${T.toString()} but threw nothing.");
  }
  catch (e) {
    if (!(e is T)) {
      throw new SSSTestException(message: "Test function should have thrown a ${T.toString()} but threw ${e.runtimeType.toString()}.");
    }

    if (exceptionMatcher != null && !exceptionMatcher(e))  {
      throw new SSSTestException(message: "Test function threw a ${e.runtimeType.toString()}, but it didn't match the conditions.");
    }
  }

}

enum TestState {
  NotRun, Failed, Success
}

class Test {
  TestFunction runCore;
  String name;
  TestState state = TestState.NotRun;

  Test(this.name, this.runCore);

  Future run() async {
    try {
      await this.runCore();
      print("\"${this.name}\" successful.");
      state = TestState.Success;
    }
    catch (ex) {
      print(ex);
      state = TestState.Failed;
    }
  }
}

typedef TestFunction = Future<Null> Function();
List<Test> tests = [
  new Test("Exports and imports player keys.", () async {
    var cryptHelper = new CryptHelper();
    var pw = "password";
    var playerKey = await cryptHelper.createPlayerKey();

    var exported = await cryptHelper.exportPlayerKey(pw, playerKey);
    var imported = await cryptHelper.parsePlayerKey(pw, exported);

    equals(playerKey.derivationSalt, imported.derivationSalt);
    equals(playerKey.iv, imported.iv);
    equals(playerKey.privateKey, imported.privateKey);
  }),

  new Test("Fails on import playerKey with incorrect password.", () async {
    var cryptHelper = new CryptHelper();
    var playerKey = await cryptHelper.createPlayerKey();
    var exported = await cryptHelper.exportPlayerKey("password", playerKey);
    await throwsAsync<PlatformException>(
      () => cryptHelper.parsePlayerKey("wrong", exported),
      exceptionMatcher: (e) => e.code == "CryptoError"
    );
  }),

  new Test("Encrypts and decrypts bets for a winner.", () async {
    var cryptHelper = new CryptHelper();
    var data = await CryptHelper.getRandomBytes(1000);
    var yourKey = await cryptHelper.createPlayerKey();
    var otherPlayerKey = await cryptHelper.createPlayerKey();
    // Encrypt a bet that only the other player can decrypt as a winner.
    var encryptedBet = await cryptHelper.encryptBet(data,
      yourKey.publicKey,
      otherPlayerKey.publicKey
    );

    var betDecryptedByWinner = await cryptHelper.decryptBetAsWinner(encryptedBet, otherPlayerKey);

    equals(data, betDecryptedByWinner);

    await throwsAsync<PlatformException>(
      () => cryptHelper.decryptBetAsWinner(encryptedBet, yourKey),
      exceptionMatcher: (e) => e.code == "CryptoError"
    );
  }),

  new Test("Encrypts and decrypts bets for a loser.", () async {
    var cryptHelper = new CryptHelper();
    var data = await CryptHelper.getRandomBytes(1000);
    var yourKey = await cryptHelper.createPlayerKey();
    var otherPlayerKey = await cryptHelper.createPlayerKey();
    // Encrypt a bet that only the other player can decrypt as a winner.
    var encryptedBet = await cryptHelper.encryptBet(data,
      yourKey.publicKey,
      otherPlayerKey.publicKey
    );

    var betDecryptedByLoser = await cryptHelper.decryptBetAsLoser(encryptedBet, yourKey);

    equals(data, betDecryptedByLoser);
    await throwsAsync<PlatformException>(
        () => cryptHelper.decryptBetAsLoser(encryptedBet, otherPlayerKey),
      exceptionMatcher: (e) => e.code == "CryptoError"
    );
  }),
];

class TestPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TestPageState();
  }

}

class TestPageState extends State<TestPage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: RaisedButton(
            child: Text("Run all"),
            onPressed: () async {
              setState(() {
                for(var t in tests) {
                  t.state = TestState.NotRun;
                }
              });
              for(var t in tests) {
                await t.run();
              }
              setState(() {});
            },
          ),
        ),
        ...tests.map((t) {
          return RaisedButton(
            child: Text(t.name),
            color: t.state == TestState.NotRun ? Colors.grey : t.state == TestState.Failed ? Colors.red : Colors.green,
            onPressed: () async {
              await t.run();
              setState(() {});
            },
          );
        }).toList()
      ],
    );
  }
}
