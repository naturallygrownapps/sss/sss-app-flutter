import 'package:flutter/material.dart';
import 'package:gallery_gambling/widgets/spinner_container.dart';

class BasePage extends StatelessWidget {

  final Widget child;
  final Widget drawer;
  final String title;
  final Widget floatingActionButton;

  BasePage({
    this.child,
    this.drawer,
    this.title,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return SpinnerContainer(
      child:
        DecoratedBox(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/background.png'),
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Text(this.title),
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            drawer: this.drawer,
            body: this.child,
            floatingActionButton: this.floatingActionButton,
        ),
      ),
    );
  }
}