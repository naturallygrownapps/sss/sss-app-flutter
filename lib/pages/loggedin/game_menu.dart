
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/game/game_context.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';
import 'package:gallery_gambling/theme.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';

class GameMenuCommands {
  final AsyncCallback acceptGame;
  final AsyncCallback rejectGame;
  final Future Function(Move move) makeMove;
  final AsyncCallback claimPrize;
  final AsyncCallback endGame;
  final AsyncCallback showLoss;

  GameMenuCommands({
    @required this.acceptGame,
    @required this.rejectGame,
    @required this.makeMove,
    @required this.claimPrize,
    @required this.endGame,
    @required this.showLoss });
}

class GameMenu extends StatefulWidget {
  final GameMenuCommands menuCommands;
  final GameContext gameCtx;

  const GameMenu({Key key, @required this.menuCommands, @required this.gameCtx}) : super(key: key);

  @override
  _GameMenuState createState() => _GameMenuState();
}

class _GameMenuState extends State<GameMenu> with SingleTickerProviderStateMixin {
  // This flag is not used for rendering but for indicating if we have to
  // run the "hide" animation first before switching to a new state.
  bool _isVisible = false;
  // The state that we were updated with last.
  GameState _lastGameMenuState;
  // The state that we're currently animating towards. This is only updated when the
  // menu hiding animation finished so that content is switched when we're not visible.
  GameState _targetGameMenuState;
  AnimationController _animationController;
  bool _isGameFinished = false;

  _GameMenuState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
    );
  }

  @override
  void initState() {
    super.initState();
    widget.gameCtx.addListener(_onGameContextUpdated);
  }

  void _onGameContextUpdated() {
    final newGameMenuState = widget.gameCtx.menuState;
    if (newGameMenuState != _lastGameMenuState) {
      _update(newGameMenuState);
    }

    final isGameFinishedNow = widget.gameCtx.isGameFinished();
    if (isGameFinishedNow != _isGameFinished) {
      setState(() {
        _isGameFinished = isGameFinishedNow;
      });
    }
  }

  @override
  void dispose() {
    widget.gameCtx.removeListener(_onGameContextUpdated);
    _animationController.dispose();
    super.dispose();
  }

  void _update(GameState gameMenuState) {
    void slideMenuIn() {
      // This must be wrapped in setState() to get the ui updated.
      setState(() {
        _targetGameMenuState = gameMenuState;
      });
      _isVisible = true;
      if (_animationController.isAnimating) {
        _animationController.stop();
      }
      // Reversing the animation shows the menu.
      _animationController.reverse();
    }

    _lastGameMenuState = gameMenuState;

    // Hide the menu if it is currently visible or if it is requested to be hidden
    // (state is null).
    if (_isVisible || gameMenuState == null) {
      // This update is taking over, cancelling previous running updates.
      if (_animationController.isAnimating) {
        _animationController.stop();
      }
      // Forwarding the animation shows the menu.
      _animationController.forward().then((_) {
        // Only actually update when the animation finished
        // so that the ui is updated while invisible and
        // to give subsequent updates the possibility to cancel
        // the current update by cancelling the animation.
        _isVisible = false;
        if (gameMenuState != null) {
          slideMenuIn();
        }
      });
    }
    else if (gameMenuState != null) {
      // Menu not visible and we have a valid state, just slide it in.
      slideMenuIn();
    }
  }

  @override
  Widget build(BuildContext context) {
    final factory = getFactoryForState(_targetGameMenuState);
    final text = getTextForState(_targetGameMenuState, SSSLocalizations.of(context));

    return SizeTransition(
      axisAlignment: -1.0,
      sizeFactor: Tween<double>(
        begin: 1.0,
        end: 0.0,
      ).animate(CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeOutQuint,
        reverseCurve: Curves.easeInQuint)),
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Divider(
              color: SSSColors.brown[500],
              height: 1,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Text(text ?? ""),
            ),
            if (factory != null) factory(context)
          ],
        ),
      )
    );
  }

  Widget Function(BuildContext ctx) getFactoryForState(GameState state) {
    if (state == GameState.AcceptanceRequired) {
      return _createAcceptanceRequiredPanel;
    }
    else if (state == GameState.YourMoveRequired) {
      return _createYourMovePanel;
    }
    else if (state == GameState.ChallengerBetRequired) {
      return _createYourMovePanel;
    }
    else if (state == GameState.Rejected) {
      return _createRejectedPanel;
    }
    else if (state == GameState.YouLost) {
      return _createYouLostPanel;
    }
    else if (state == GameState.YouWon) {
      return _createYouWonPanel;
    }

    return null;
  }

  String getTextForState(GameState state, SSSLocalizations loc) {
    if (state == null) {
      return null;
    }
    else if (state == GameState.AcceptanceRequired)
    {
      return loc.game_ingame_status_acceptancerequired;
    }
    else if (state == GameState.InitiatedByYou)
    {
      return loc.game_ingame_status_initiatedbyyou;
    }
    else if (state == GameState.Rejected)
    {
      return loc.game_ingame_status_rejected;
    }
    else if (state == GameState.ChallengerBetRequired || state == GameState.YourMoveRequired)
    {
      return loc.game_ingame_status_yourmoverequired;
    }
    else if (state == GameState.WaitingForOtherPlayer)
    {
      return loc.game_ingame_status_waitingforotherplayer;
    }
    else if (state == GameState.YouLost)
    {
      return loc.game_ingame_status_youlost;
    }
    else if (state == GameState.YouWon)
    {
      return loc.game_ingame_status_youwon;
    }

    return null;
  }

  Widget _createAcceptanceRequiredPanel(BuildContext ctx) {
    final loc = SSSLocalizations.of(ctx);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8, right: 4),
            child: SSSButton(
              child: Text(loc.common_yes),
              onPressed: widget.menuCommands.acceptGame,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 4, right: 8),
            child: SSSButton(
              child: Text(loc.common_no),
              onPressed: widget.menuCommands.rejectGame,
            ),
          ),
        )
      ],
    );
  }

  Widget _createRejectedPanel(BuildContext ctx) {
    final loc = SSSLocalizations.of(ctx);
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 8, right: 8),
      child: SizedBox(
        width: double.infinity,
        child: SSSButton(
          child: Text(loc.game_ingame_button_endgame),
          onPressed: widget.menuCommands.endGame,
        ),
      ),
    );
  }

  Widget _createYourMovePanel(BuildContext ctx) {
    return Container(
      height: scaleToDevice(150),
      padding: EdgeInsets.only(bottom: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: FlatButton(
              onPressed: () => widget.menuCommands.makeMove(Move.Rock),
              child: Image(image: AssetImage("assets/images/game/rock.png")),
            ),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: FlatButton(
              onPressed: () => widget.menuCommands.makeMove(Move.Scissors),
              child: Image(image: AssetImage("assets/images/game/scissors.png")),
            ),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: FlatButton(
              onPressed: () => widget.menuCommands.makeMove(Move.Paper),
              child: Image(image: AssetImage("assets/images/game/paper.png")),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createYouWonPanel(BuildContext ctx) {
    final loc = SSSLocalizations.of(ctx);
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 8, right: 8),
      child: SizedBox(
        width: double.infinity,
        child: _isGameFinished ?
          SSSButton(
            child: Text(loc.game_ingame_button_endgame),
            onPressed: widget.menuCommands.endGame,
          ) :
          SSSButton(
            child: Text(loc.game_ingame_button_claimprize),
            onPressed: widget.menuCommands.claimPrize
          ),
      ),
    );
  }

  Widget _createYouLostPanel(BuildContext ctx) {
    final loc = SSSLocalizations.of(ctx);
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 8, right: 8),
      child: SizedBox(
        width: double.infinity,
        child: _isGameFinished ?
          SSSButton(
            child: Text(loc.game_ingame_button_endgame),
            onPressed: widget.menuCommands.endGame,
          ) :
          SSSButton(
            child: Text(loc.game_ingame_button_showloss),
            onPressed: widget.menuCommands.showLoss
          ),
      ),
    );
  }

}
