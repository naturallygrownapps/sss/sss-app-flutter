import 'dart:convert';
import 'dart:math';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gallery_gambling/game/game_context.dart';
import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/shared/bet_capabilities.dart';
import 'package:gallery_gambling/shared/dto/game_participant_dto.dart';
import 'package:gallery_gambling/shared/dto/round_dto.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';
import 'package:gallery_gambling/shared/player_role.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gsutil;
import 'package:provider/provider.dart';

enum DebuggerGameAction
{
  Challenge, Accept, Reject, PlaceBet,
  Rock, Paper, Scissors
}

const String OTHERPLAYER_NAME = "OtherPlayer";
const String YOUR_NAME = "You";
const String NOBODY_NAME = "nobody";

class GamePageDebuggerWidget extends StatefulWidget {
  GamePageDebuggerWidget();

  @override
  _GamePageDebuggerWidgetState createState() => _GamePageDebuggerWidgetState();
}

class _GamePageDebuggerWidgetState extends State<GamePageDebuggerWidget> {
  bool _isVisible = false;

  @override
  Widget build(BuildContext context) {
    final layout = Column(
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            icon: Icon(Icons.developer_mode),
            onPressed: () {
              setState(() {
                _isVisible = !_isVisible;
              });
            },
          ),
        ),
      ],
    );

    if (!_isVisible) {
      return layout;
    }

    final debuggerModel = Provider.of<GamePageDebuggerModel>(context);
    layout.children.add(Container(
      child: Row(
        children: <Widget>[
          Flexible(child: Text(EnumToString.parse(debuggerModel.state), style: TextStyle(fontSize: 10),)),
          DropdownButton(
            value: debuggerModel.selectedPlayer,
            onChanged: (Player newValue) {
              setState(() {
                debuggerModel.selectedPlayer = newValue;
              });
            },
            items: Player.values.map((p) => DropdownMenuItem<Player>(
              value: p,
              child: Text(EnumToString.parse(p)),
            )).toList(),
          ),
          DropdownButton(
            value: debuggerModel.selectedAction,
            onChanged: (DebuggerGameAction newValue) {
              setState(() {
                debuggerModel.selectedAction = newValue;
              });
            },
            items: DebuggerGameAction.values.map((a) => DropdownMenuItem<DebuggerGameAction>(
              value: a,
              child: Text(EnumToString.parse(a)),
            )).toList(),
          ),
          Flexible(
            child: SSSButton(
              child: Text("DO IT"),
              onPressed: debuggerModel._performCurrentAction,
            ),
          )
        ],
      ),
    ));
    return layout;
  }
}

class GamePageDebuggerModel with ChangeNotifier {
  final GameContext gameCtx;

  GameStatus _game;
  GameStatus get game => _game;
  set game(GameStatus value) {
    _game = value;
    notifyListeners();
  }

  Player _selectedPlayer;
  Player get selectedPlayer => _selectedPlayer;
  set selectedPlayer(Player value) {
    _selectedPlayer = value;
    notifyListeners();
  }

  DebuggerGameAction _selectedAction;
  DebuggerGameAction get selectedAction => _selectedAction;
  set selectedAction(DebuggerGameAction value) {
    _selectedAction = value;
    notifyListeners();
  }

  GameState get state => _game?.state;
  set state(GameState value) {
    // Create a copy to get an updated reference.
    _updateGameStatus(GameStatus.fromJson(_game.toJson())
      ..state = value
    );
  }

  GamePageDebuggerModel(this.gameCtx) {
    gameCtx.addListener(_onGameContextChanged);

    selectedPlayer = Player.values.first;
    selectedAction = DebuggerGameAction.values.first;
  }

  @override
  void dispose() {
    gameCtx.removeListener(_onGameContextChanged);
    super.dispose();
  }

  void init() {
    final initialGame = GameStatus()
      ..gameId = base64Encode(List<int>.generate(16, (i) => Random.secure().nextInt(256)))
      ..timestamp = DateTime.now().microsecondsSinceEpoch
      ..rounds = <RoundDto>[]
      ..roundWinners = <String>[]
      ..requiredWinningRounds = 3
      ..revision = 0;

//    var g = _modifyGameStatusWithAction(null, Player.Other, DebuggerGameAction.Challenge);
//    g = _modifyGameStatusWithAction(g, Player.You, DebuggerGameAction.Accept);
//
//    g = _modifyGameStatusWithAction(g, Player.You, DebuggerGameAction.Scissors);
//    g = _modifyGameStatusWithAction(g, Player.Other, DebuggerGameAction.Rock);
//
//    g = _modifyGameStatusWithAction(g, Player.You, DebuggerGameAction.Scissors);
//    g = _modifyGameStatusWithAction(g, Player.Other, DebuggerGameAction.Rock);
//
////    g = _modifyGameStatusWithAction(g, Player.You, DebuggerGameAction.Scissors);
//    g = _modifyGameStatusWithAction(g, Player.Other, DebuggerGameAction.Rock);
//    _updateGameStatus(g);

    _updateGameStatus(initialGame);
    performAction(Player.Other, DebuggerGameAction.Challenge);
    performAction(Player.You, DebuggerGameAction.Accept);

    performAction(Player.You, DebuggerGameAction.Rock);
    performAction(Player.Other, DebuggerGameAction.Scissors);

    performAction(Player.You, DebuggerGameAction.Rock);
    performAction(Player.Other, DebuggerGameAction.Scissors);

    performAction(Player.You, DebuggerGameAction.Rock);
    performAction(Player.Other, DebuggerGameAction.Scissors);

    selectedPlayer = Player.Other;
    selectedAction = DebuggerGameAction.Rock;
  }

  void _onGameContextChanged() {
    this.game = gameCtx.game;
  }

  void _updateGameStatus(GameStatus newGameStatus) {
    newGameStatus.timestamp = DateTime.now().microsecondsSinceEpoch;
    newGameStatus.revision++;
    gameCtx.game = newGameStatus;
  }

  void _performCurrentAction() {
    performAction(selectedPlayer, selectedAction);
  }

  static GameStatus _modifyGameStatusWithAction(GameStatus originalStatus, Player player, DebuggerGameAction action) {
    // Create a clone so we get the reference updated for change notifications.
    var status = originalStatus != null ? GameStatus.fromJson(originalStatus.toJson()) : GameStatus();
    final round = gsutil.getActiveRound(status);

    if (action == DebuggerGameAction.Challenge) {
      status = new GameStatus()
        ..timestamp = DateTime.now().microsecondsSinceEpoch
        ..revision = 0
        ..state = player == Player.You ? GameState.InitiatedByYou : GameState.AcceptanceRequired
        ..rounds = <RoundDto>[]
        ..agreedBetType = BetCapabilities.Photo
        ..otherPlayer = (GameParticipantDto()
          ..username = OTHERPLAYER_NAME)
        ..playerRole = player == Player.You ? PlayerRole.Challenger : PlayerRole.Opponent
        ..requiredWinningRounds = 3
        ..roundWinners = <String>[];
    }
    else if (action == DebuggerGameAction.Accept) {
      status.state = player == Player.Other ? GameState.ChallengerBetRequired : GameState.YourMoveRequired;
      if (player == Player.You) {
        status.rounds = <RoundDto>[ RoundDto() ];
      }
    }
    else if (action == DebuggerGameAction.Reject) {
      status.state = GameState.Rejected;
    }
    else if (action == DebuggerGameAction.PlaceBet) {
      status.state = GameState.YourMoveRequired;
      status.rounds = <RoundDto> [ RoundDto() ];
    }
    else if (action == DebuggerGameAction.Paper || action == DebuggerGameAction.Rock || action == DebuggerGameAction.Scissors) {
      final move = EnumToString.fromString(Move.values, EnumToString.parse(action));
      if (player == Player.Other) {
        round.otherPlayerMove = move;
      }
      else {
        round.yourMove = move;
      }

      if (gsutil.isRoundFinished(round)) {
        String winnerName = null;
        var roundWinner = _getRoundWinner(round);
        if (roundWinner == Player.You) {
          winnerName = YOUR_NAME;
        }
        else if (roundWinner == Player.Other) {
          winnerName = OTHERPLAYER_NAME;
        }
        else {
          winnerName = NOBODY_NAME;
        }

        status.roundWinners = status.roundWinners.followedBy([
          winnerName
        ]).toList();

        final winner = _getWinner(status);
        if (winner != Player.Nobody) {
          status.state = winner == Player.You ? GameState.YouWon : GameState.YouLost;
        }
        else {
          // next round
          status.rounds = status.rounds.followedBy([
            RoundDto()
          ]).toList();
          status.state = GameState.YourMoveRequired;
        }
      }
      else {
        status.state = _determineStateAfterMove(status);
      }
    }

    return status;
  }

  void performAction(Player player, DebuggerGameAction action) {
    final newStatus = _modifyGameStatusWithAction(_game, player, action);
    _updateGameStatus(newStatus);
  }

  static Player _getRoundWinner(RoundDto round) {
    if (!gsutil.isRoundFinished(round))
      throw new Exception("Cannot determine a winner or loser when the round is not finished yet.");

    Move p1Move = round.yourMove;
    Move p2Move = round.otherPlayerMove;
    if (p1Move == p2Move) {
      return Player.Nobody;
    }
    else if (p1Move == Move.Paper) {
      if (p2Move == Move.Rock) {
        return Player.You;
      }
      else if (p2Move == Move.Scissors) {
        return Player.Other;
      }
    }
    else if (p1Move == Move.Rock) {
      if (p2Move == Move.Paper) {
        return Player.Other;
      }
      else if (p2Move == Move.Scissors) {
        return Player.You;
      }
    }
    else if (p1Move == Move.Scissors) {
      if (p2Move == Move.Paper) {
        return Player.You;
      }
      else if (p2Move == Move.Rock) {
        return Player.Other;
      }
    }

    throw new Exception("Unknown move.");
  }

  static Player _getWinner(GameStatus gameStatus) {
    if (gameStatus.rounds.length < gameStatus.requiredWinningRounds) {
      return Player.Nobody;
    }

    int yourWins = 0;
    int otherWins = 0;
    for (final winner in gameStatus.roundWinners) {
      if (winner == OTHERPLAYER_NAME) {
        otherWins++;
        if (otherWins >= gameStatus.requiredWinningRounds) {
          return Player.Other;
        }
      }
      else if (winner == YOUR_NAME) {
        yourWins++;
        if (yourWins >= gameStatus.requiredWinningRounds) {
          return Player.You;
        }
      }
    }

    return Player.Nobody;
  }

  static GameState _determineStateAfterMove(GameStatus status) {
    if (_isPlayersTurn(Player.You, status)) {
      return GameState.YourMoveRequired;
    }
    else {
      return GameState.WaitingForOtherPlayer;
    }
  }

  static bool _isPlayersTurn(Player player, GameStatus status) {
    final round = gsutil.getActiveRound(status);
    if (round == null) {
      return false;
    }
    else {
      Move move;
      if (player == Player.Other) {
        move = round.otherPlayerMove;
      }
      else {
        move = round.yourMove;
      }

      return move == null;
    }
  }
}
