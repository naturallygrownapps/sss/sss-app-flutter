import 'dart:async';
import 'dart:io';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/game_status_utils.dart' as gameStatusUtils;
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/pages/game_starter.dart';
import 'package:gallery_gambling/pages/loggedin/game_page.dart';
import 'package:gallery_gambling/pages/loggedin/loggedin_drawer.dart';
import 'package:gallery_gambling/route_aware_widget.dart';
import 'package:gallery_gambling/routes.dart' as routes;
import 'package:gallery_gambling/services/android_services.dart';
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/dynamic_links_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/local_game_service.dart';
import 'package:gallery_gambling/services/local_players_service.dart';
import 'package:gallery_gambling/services/permissions.dart';
import 'package:gallery_gambling/services/push_notifications.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/shared/dto/user_entity_dto.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/notifications.dart' as n;
import 'package:gallery_gambling/theme.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:share/share.dart';

// Only check permissions once per app run.
bool hasCheckedPermissions = false;
// Indicates if we were just started or were already running.
bool hasJustBeenStarted = true;

enum _GameMenuOptions { Delete }

/**
 * The root page when logged in.
 * Shows the list of games, handles navigating to games and handles lifecycle changes
 * like initial start or resume.
 */
class GamesOverviewPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GamesOverviewPageState();
  }
}

class _GamesOverviewPageState extends State<GamesOverviewPage>
    with WidgetsBindingObserver, GameStarter {
  final LocalGameService _localGameService = getService<LocalGameService>();
  final LoadingTracker _loadingTracker = getService<LoadingTracker>();
  final PushNotifications _pushNotifications = getService<PushNotifications>();
  final DynamicLinksService _dynamicLinksService = getService<DynamicLinksService>();
  StreamSubscription<n.Notification> _pushNotificationSubscription;
  StreamSubscription _dynamicLinksSubscription;

  List<GameStatus> _games;

  @override
  void initState() {
    _games = _localGameService.getLocalGames();
    _pushNotificationSubscription = _pushNotifications.notifications.listen(_onPushNotification);
    WidgetsBinding.instance.addPostFrameCallback(_onInitialLayoutDone);
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  void _onInitialLayoutDone(Duration d) async {
    _checkAuthState().then((_) => _refreshGamesList());

    final isInitialStart = hasJustBeenStarted;
    hasJustBeenStarted = false;
    final permissionsGranted = await _enforcePermissions();
    if (permissionsGranted && isInitialStart) {
      // Check if we were passed launch args on the first start.
      await _checkLaunchArgs();
    }

    _dynamicLinksSubscription = _dynamicLinksService.links.listen(_onDynamicLink);
  }

  @override
  void dispose() {
    _pushNotificationSubscription?.cancel();
    _pushNotificationSubscription = null;
    _dynamicLinksSubscription?.cancel();
    _dynamicLinksSubscription = null;
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // Check if we were resumed with any args (e.g. resumed through a notification).
      _checkLaunchArgs().then((hasResumedWithGame) {
        // Reload the games list when we're resumed, but not if we went directly
        // to the game screen.
        if (!hasResumedWithGame) {
          _refreshGamesList();
        }
      });
    }
    super.didChangeAppLifecycleState(state);
  }

  void _onDynamicLink(DynamicLink dynamicLink) {
    if (dynamicLink.tryProcessBy("GamesOverview")) {
      final inviteLinkInfo = InviteLink.tryParse(dynamicLink.link);
      if (inviteLinkInfo != null) {
        startGame(UserEntityDto()..username = inviteLinkInfo.invitingUsername,
            initiatedByInvite: true);
      }
    }
  }

  Future<bool> _checkLaunchArgs() async {
    if (Platform.isAndroid) {
      final notificationFromIntent =
          await getService<AndroidServices>().getNotificationFromIntent();
      if (notificationFromIntent != null) {
        final gameFromNotification = await _processNotification(notificationFromIntent);
        _showGame(gameFromNotification);
        return true;
      }
    } else {
      // No other platforms supported yet.
    }

    return false;
  }

  Future _checkAuthState() async {
    final session = getService<Session>();
    if (!session.hasCheckedAuthState) {
      session.hasCheckedAuthState = true;
      final loc = SSSLocalizations.of(context);
      await _loadingTracker.track(getService<LocalPlayersService>().getSelf(),
          message: loc.overview_signingin);
    }
  }

  Future<bool> _enforcePermissions() async {
    if (!hasCheckedPermissions) {
      hasCheckedPermissions = true;
      var exitDueToMissingPermissions = false;
      final permissions = getService<Permissions>();
      while (!await permissions.checkPermissions()) {
        final dialogResult = await showDialog<bool>(
            context: context,
            builder: (builder) {
              final loc = SSSLocalizations.of(builder);
              return AlertDialog(
                title: Text(loc.permissions_confirm_title),
                content: Text(loc.permissions_confirm_text,
                    style: Theme.of(builder).primaryTextTheme.subtitle2),
                actions: [
                  RaisedButton(
                    child: Text(loc.common_ok),
                    onPressed: () {
                      Navigator.of(builder).pop(true);
                    },
                  ),
                  RaisedButton(
                    child: Text(loc.common_quit),
                    onPressed: () {
                      Navigator.of(builder).pop(false);
                    },
                  ),
                ],
              );
            });

        if (dialogResult == null || !dialogResult) {
          exitDueToMissingPermissions = true;
          break;
        }
      }

      if (exitDueToMissingPermissions) {
        if (Platform.isAndroid) {
          SystemNavigator.pop();
        } else {
          // Exit is not possible on iOS as Apple does not allow apps to exit themselves.
          getService<AuthService>().logout();
        }
        return false;
      }
    }

    return true;
  }

  void _onPushNotification(n.Notification notification) {
    _processNotification(notification);
  }

  Future<GameStatus> _processNotification(n.Notification notification) async {
    String gameId = null;
    GameStatus gameFromNotification = null;

    if (notification is n.NewGameStartedNotification) {
      gameId = notification.gameId;
    } else if (notification is n.GameUpdatedNotification) {
      gameId = notification.gameId;
    } else if (notification is n.GameRejectedNotification) {
      gameId = notification.gameId;
    }

    var hasChanges = false;
    if (gameId != null) {
      gameFromNotification = await _localGameService.queryGameStatusById(gameId);
      if (gameFromNotification != null) {
        if (!_games.any((g) => g.gameId == gameFromNotification.gameId)) {
          // new game
          _games.add(gameFromNotification);
          hasChanges = true;
        } else {
          // updated game
          for (int i = 0; i < _games.length; i++) {
            if (_games[i].gameId == gameFromNotification.gameId) {
              _games[i] = gameFromNotification;
              hasChanges = true;
              break;
            }
          }
        }
      }
    }

    if (hasChanges && mounted) {
      setState(() {});
    }

    return gameFromNotification;
  }

  void _refreshLocalGames() {
    if (mounted) {
      setState(() {
        _games = _localGameService.getLocalGames();
      });
    }
  }

  Future _refreshGamesList() async {
    final games = await _loadingTracker.track(_localGameService.getGames(true));
    if (mounted) {
      setState(() {
        _games = games;
      });
    }
  }

  void _deleteGame(GameStatus gameStatus) {
    _localGameService.endGame(gameStatus);
    setState(() {
      _games.remove(gameStatus);
    });
  }

  void _showGame(GameStatus gameStatus) {
    // Remove all other (possibly game) routes that are active before we open the next game.
    // This is especially important if we were called from launch arguments to prevent
    // a stack of overview->game->game which is confusing to the user.
    Navigator.of(context).pushAndRemoveUntil(
        new MaterialPageRoute(builder: (ctx) => GamePage(gameStatus)), ModalRoute.withName("/"));
  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return RouteAwareWidget(
      didPopNext: _refreshLocalGames,
      child: BasePage(
        title: loc.overview_title,
        drawer: LoggedInDrawer(),
        child: RefreshIndicator(
          color: SSSColors.brown[500],
          onRefresh: () => _refreshGamesList(),
          child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: DataTable(
                    columnSpacing: 8,
                    horizontalMargin: 0,
                    columns: <DataColumn>[
                      DataColumn(label: Text(loc.overview_header_opponentname)),
                      DataColumn(label: Text(loc.overview_header_gamestatus)),
                      DataColumn(label: Text(loc.overview_header_score), numeric: true),
                      DataColumn(label: Container()),
                    ],
                    rows: _games.map((g) => _createRowForGame(g, loc)).toList(),
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: SSSColors.beige[500],
              ),
              child: ButtonBar(
                buttonPadding: EdgeInsets.all(2.0),
                alignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FlatButton(
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.share),
                          Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Text(loc.overview_inviteplayer),
                          )
                        ],
                      ),
                      onPressed: () {
                        _loadingTracker.track(_invitePlayer(loc));
                      }),
                  FlatButton(
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.add),
                          Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Text(loc.overview_startnewgame),
                          )
                        ],
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, routes.newGame);
                      })
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  DataRow _createRowForGame(GameStatus gameStatus, SSSLocalizations loc) {
    final textTheme = Theme.of(context).textTheme;
    return DataRow(cells: <DataCell>[
      DataCell(
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(gameStatus.otherPlayer.username, style: textTheme.bodyText2),
          ),
          onTap: () => _showGame(gameStatus)),
      DataCell(
          Text(
            _translateGameState(gameStatus.state, loc),
            style: textTheme.bodyText1,
          ),
          onTap: () => _showGame(gameStatus)),
      DataCell(Text(_roundWinnersToScoreString(gameStatus), style: textTheme.bodyText2),
          onTap: () => _showGame(gameStatus)),
      DataCell(_createMenuForGameRow(gameStatus, loc)),
    ]);
  }

  Widget _createMenuForGameRow(GameStatus gameStatus, SSSLocalizations loc) {
    if (!_canDeleteGame(gameStatus)) {
      return Container();
    }

    return PopupMenuButton(
        onSelected: (_GameMenuOptions selection) {
          if (selection == _GameMenuOptions.Delete) {
            _deleteGame(gameStatus);
          }
        },
        itemBuilder: (context) => <PopupMenuItem<_GameMenuOptions>>[
              PopupMenuItem<_GameMenuOptions>(
                value: _GameMenuOptions.Delete,
                child: Text(loc.common_delete),
              ),
            ]);
  }

  String _translateGameState(GameState state, SSSLocalizations loc) {
    final stateName = EnumToString.parse(state).toLowerCase();
    return loc.getLocalizationFor("game_status_$stateName");
  }

  String _roundWinnersToScoreString(GameStatus gameStatus) {
    final scores = gameStatusUtils.getScores(gameStatus);
    return "${scores.yourScore} : ${scores.otherPlayerScore}";
  }

  bool _canDeleteGame(GameStatus gameStatus) {
    return gameStatus != null &&
        (gameStatus.state == GameState.Rejected ||
            gameStatus.state == GameState.YouWon ||
            gameStatus.state == GameState.YouLost);
  }

  Future _invitePlayer(SSSLocalizations loc) async {
    final currentPlayername = getService<Session>().currentPlayer?.username;
    if (!isNotNullOrEmpty(currentPlayername)) {
      return;
    }

    final dynamicLink =
        await _dynamicLinksService.createDynamicLink(InviteLink.create(currentPlayername));

    final message = loc.overview_inviteplayer_linkmessage
        .replaceFirst("{0}", currentPlayername)
        .replaceFirst("{1}", dynamicLink.toString());
    Share.share(message);
  }
}
