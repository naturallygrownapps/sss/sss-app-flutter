import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/widgets/sss_button.dart';

class AccountSettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var loc = SSSLocalizations.of(context);
    return BasePage(
      title: loc.accountsettings_title,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(loc.accountsettings_delete_account_description),
            SSSButton(
              child: Text(loc.accountsettings_delete_account_button),
              onPressed: () async {
                if (await confirmDeleteDialog(context)) {
                  getService<LoadingTracker>().track(getService<AuthService>().deleteAccount());
                }
              },
            )
          ],
        ),
      ),
    );
  }

  Future<bool> confirmDeleteDialog(BuildContext context) async {
    final dialogResult = await showDialog<bool>(
        context: context,
        builder: (builder) {
          final loc = SSSLocalizations.of(builder);
          return AlertDialog(
            title: Text(loc.accountsettings_delete_account_button),
            content: Text(loc.settings_delete_account_confirm_text,
                style: Theme.of(builder).primaryTextTheme.subtitle2),
            actions: [
              RaisedButton(
                child: Text(loc.common_yes),
                onPressed: () {
                  Navigator.of(builder).pop(true);
                },
              ),
              RaisedButton(
                child: Text(loc.common_no),
                onPressed: () {
                  Navigator.of(builder).pop(false);
                },
              ),
            ],
          );
        });

    return dialogResult ?? false;
  }
}
