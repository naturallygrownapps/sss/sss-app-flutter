import 'dart:async';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/game/game_context.dart';
import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/game/scene/game_scene_painter.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/pages/loggedin/game_menu.dart';
import 'package:gallery_gambling/pages/loggedin/game_page_debugger.dart';
import 'package:gallery_gambling/scene/scene_widget.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/local_game_service.dart';
import 'package:gallery_gambling/services/local_players_service.dart';
import 'package:gallery_gambling/services/push_notifications.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';
import 'package:gallery_gambling/shared/notifications.dart' as n;
import 'package:gallery_gambling/util_functions.dart';
import 'package:provider/provider.dart';

const enableDebugger = false;

class GamePage extends StatefulWidget {
  final GameStatus initialGameStatus;

  GamePage(this.initialGameStatus);

  @override
  State<StatefulWidget> createState() {
    return _GamePageState();
  }
}

class _GamePageState extends State<GamePage> with WidgetsBindingObserver {
  LocalGameService _localGameService = getService<LocalGameService>();
  LocalPlayersService _localPlayersService = getService<LocalPlayersService>();
  LoadingTracker _loadingTracker = getService<LoadingTracker>();
  PushNotifications _pushNotifications = getService<PushNotifications>();

  GameContext _gameCtx;
  GamePageDebuggerModel _debuggerModel;
  // Indicates if an async action, triggered by input, is currently executing.
  bool _isPerformingAction = false;
  StreamSubscription<n.Notification> _pushNotificationSubscription;

  @override
  void initState() {
    super.initState();
    _gameCtx = GameContext(widget.initialGameStatus);
    if (_useDebugger()) {
      _debuggerModel = GamePageDebuggerModel(_gameCtx);
    } else {
      _loadingTracker.track(_refreshGame());
    }
    _pushNotificationSubscription = _pushNotifications.notifications.listen(_onPushNotification);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    // We are leaving the game view, store the game as previously displayed.
    if (_gameCtx?.game != null) {
      _localGameService.rememberPreviouslyDisplayedGameStatus(_gameCtx.game);
    }
    _gameCtx?.dispose();
    _pushNotificationSubscription?.cancel();
    _pushNotificationSubscription = null;
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive || state == AppLifecycleState.paused) {
      if (_gameCtx?.game != null) {
        _localGameService.rememberPreviouslyDisplayedGameStatus(_gameCtx.game);
      }
    } else if (state == AppLifecycleState.resumed) {
      final game = _gameCtx.game;
      // We only have to refresh the game when our opponent can change something on the game.
      final refreshGame = game != null && game.state == GameState.InitiatedByYou ||
          game.state == GameState.WaitingForOtherPlayer ||
          game.state == GameState.YourMoveRequired;
      if (refreshGame) {
        _loadingTracker.track(_refreshGame());
      }
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return BasePage(
      title: loc.ingame_title,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: ChangeNotifierProvider.value(
              value: _gameCtx,
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: GestureDetector(
                          onTap: () {
                            // Make the big gift icon clickable.
                            if (_gameCtx.game.state == GameState.YouWon &&
                                _gameCtx.winnerPrize == null) {
                              _skipFurtherCallsUntilFinished(_claimPrize)();
                            }
                          },
                          child: _initSceneForGame(widget.initialGameStatus))),
                  GameMenu(
                    menuCommands: GameMenuCommands(
                      acceptGame: _skipFurtherCallsUntilFinished(_acceptGame),
                      rejectGame: _skipFurtherCallsUntilFinished(_rejectGame),
                      endGame: _skipFurtherCallsUntilFinished(_endGame),
                      claimPrize: _skipFurtherCallsUntilFinished(_claimPrize),
                      showLoss: _skipFurtherCallsUntilFinished(_showLoss),
                      makeMove: (m) => _skipFurtherCallsUntilFinished(() => _makeMove(m))(),
                    ),
                    gameCtx: _gameCtx,
                  ),
                ],
              ),
            ),
          ),
          if (_useDebugger())
            Align(
              child: _createDebuggerWidget(),
              alignment: Alignment.topLeft,
            ),
        ],
      ),
    );
  }

  SceneWidget _initSceneForGame(GameStatus game) {
    final widget = SceneWidget((ctx) async {
      final painter = GameScenePainter(ctx, _gameCtx);
      await painter.scene.loadAssets();

      final previousGameStatus = _localGameService.getPreviouslyDisplayedGameStatus(game);
      painter.scene.initForGame(previousGameStatus);

      if (_debuggerModel != null) {
        _debuggerModel.init();
      }

      return painter;
    });
    return widget;
  }

  void _onPushNotification(n.Notification notification) {
    if (notification is n.GameNotification && _gameCtx.game?.gameId == notification.gameId) {
      _loadingTracker.track(_refreshGame());
    }
  }

  Future _refreshGame() async {
    final updatedGame = await _localGameService.queryGameStatus(_gameCtx.game);
    if (updatedGame == null) {
      if (_gameCtx.game != null) {
        // Game does not exist, might have been deleted.
        _localGameService.markGameDeleted(_gameCtx.game);
      }
      Navigator.of(context).pop();
    } else {
      _gameCtx.game = updatedGame;
    }
  }

  Widget _createDebuggerWidget() {
    return ChangeNotifierProvider.value(value: _debuggerModel, child: GamePageDebuggerWidget());
  }

  Future _acceptGame() async {
    if (!_canExecuteInStates(GameState.AcceptanceRequired)) {
      return;
    }

    if (_useDebugger()) {
      _debuggerModel.performAction(Player.You, DebuggerGameAction.Accept);
    } else {
      final loc = SSSLocalizations.of(context);
      final newGameStatus = await _loadingTracker.track(_localGameService.acceptGame(_gameCtx.game),
          message: loc.game_ingame_placingbet);
      _gameCtx.game = newGameStatus;
      await _loadingTracker
          .track(_localPlayersService.addRecentOpponentByName(newGameStatus.otherPlayer.username));
    }
  }

  Future _rejectGame() async {
    if (!_canExecuteInStates(GameState.AcceptanceRequired)) {
      return;
    }

    if (_useDebugger()) {
      _debuggerModel.performAction(Player.You, DebuggerGameAction.Reject);
    } else {
      final newGameStatus =
          await _loadingTracker.track(_localGameService.rejectGame(_gameCtx.game));
      _gameCtx.game = newGameStatus;
    }
  }

  Future _makeMove(Move move) async {
    if (!_canExecuteInStates(GameState.YourMoveRequired, GameState.ChallengerBetRequired)) {
      return;
    }

    if (_useDebugger()) {
      _debuggerModel.performAction(
          Player.You, EnumToString.fromString(DebuggerGameAction.values, EnumToString.parse(move)));
    } else {
      final game = _gameCtx.game;
      if (game.state == GameState.ChallengerBetRequired) {
        final loc = SSSLocalizations.of(context);
        final progress = _loadingTracker.beginProgress();
        try {
          await _loadingTracker.track(_localGameService.placeChallengerBet(game, progress),
              message: loc.game_ingame_placingbet);
        } finally {
          progress.dispose();
        }
      }
      _gameCtx.game = await _loadingTracker.track(_localGameService.makeMove(game, move));
    }
  }

  Future _claimPrize() async {
    if (!_canExecuteInStates(GameState.YouWon) && _gameCtx.winnerPrize != null) {
      return;
    }

    if (_useDebugger()) {
      final imgData = await rootBundle.load("assets/images/intro/picture.png");
      _gameCtx.winnerPrize = imgData.buffer.asUint8List();
    } else {
      final prize = await _loadingTracker.track(_localGameService.getWinnerPrize(_gameCtx.game));
      _gameCtx.winnerPrize = prize;
    }
  }

  Future _endGame() async {
    if (!_gameCtx.isGameFinished()) {
      return;
    }

    await _localGameService.endGame(_gameCtx.game);
    Navigator.of(context).pop();
  }

  Future _showLoss() async {
    if (!_canExecuteInStates(GameState.YouLost) || _gameCtx.loserImage != null) {
      return;
    }

    if (_useDebugger()) {
      final imgData = await rootBundle.load("assets/images/intro/picture.png");
      _gameCtx.loserImage = imgData.buffer.asUint8List();
    } else {
      final loss = await _loadingTracker.track(_localGameService.getLostBet(_gameCtx.game));
      _gameCtx.loserImage = loss;
    }
  }

  bool _useDebugger() => enableDebugger && isDebugMode();

  bool _canExecuteInStates([GameState state1, GameState state2, GameState state3]) {
    final currentState = _gameCtx.game.state;
    return currentState != null && currentState == state1 ||
        currentState == state2 ||
        currentState == state3;
  }

  AsyncCallback _skipFurtherCallsUntilFinished(AsyncCallback fn) {
    return () {
      if (_isPerformingAction) {
        return Future.value();
      }

      _isPerformingAction = true;
      return fn().whenComplete(() {
        _isPerformingAction = false;
      });
    };
  }
}
