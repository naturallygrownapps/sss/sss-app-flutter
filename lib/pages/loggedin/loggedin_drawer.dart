import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/services/auth_service.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/widgets/sss_drawer.dart';
import 'package:gallery_gambling/widgets/sss_widgets.dart';
import 'package:gallery_gambling/routes.dart' as routes;

class LoggedInDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final loc = SSSLocalizations.of(context);
    final session = getService<Session>();
    return SSSDrawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              DrawerListTile(
                text: loc.main_menu_item_newgame,
                onPressed: () {
                  Navigator.popAndPushNamed(context, routes.newGame);
                },
              ),
            ],
          ),
          Column(
            children: <Widget>[
              DrawerListTile(
                text: loc.main_menu_item_accountsettings,
                onPressed: () {
                  Navigator.popAndPushNamed(context, routes.accountSettings);
                },
              ),
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(
                      child: DrawerListTile(
                        text: loc.common_menu_item_about,
                        onPressed: () {
                          Navigator.popAndPushNamed(context, routes.about);
                        },
                      ),
                    ),
                    Expanded(
                      child: DrawerListTile(
                        text: loc.main_menu_item_logout,
                        onPressed: () {
                          var authSvc = getService<AuthService>();
                          var loadTracker = getService<LoadingTracker>();
                          loadTracker.track(authSvc.logout());
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.fromLTRB(8, 0, 50, 8),
                child: Text(
                  loc.main_menu_loggedinas.replaceFirst("{0}", session.currentPlayer?.username),
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}