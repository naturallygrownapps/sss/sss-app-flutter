import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/base_page.dart';
import 'package:gallery_gambling/pages/game_starter.dart';
import 'package:gallery_gambling/services/local_players_service.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/shared/dto/user_entity_dto.dart';
import 'package:gallery_gambling/theme.dart';
import 'package:rxdart/rxdart.dart';

class NewGamePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NewGamePageState();
  }
}

class _PlayerListState {
  bool isLoading;
  bool hasSearched;
  List<UserEntityDto> players;

  _PlayerListState({this.isLoading, this.hasSearched, this.players}) {
    this.players ??= new List();
    this.isLoading ??= false;
    this.hasSearched ??= false;
  }
}

class _PlayerSearcher {
  final LocalPlayersService _playersService;
  PublishSubject<Stream<_PlayerListState>> _sentRequests;
  Observable<_PlayerListState> states;
  String _lastSearchTerm;

  _PlayerSearcher(this._playersService) {
    _sentRequests = new PublishSubject();
    states = Observable.switchLatest(_sentRequests);
  }

  void search(String searchTerm) {
    if (_lastSearchTerm == searchTerm) {
      return;
    }

    _lastSearchTerm = searchTerm;
    final requestFuture = _playersService.findPlayers(searchTerm);
    _sentRequests.add(Observable.fromFuture(requestFuture)
        .map((foundPlayers) =>
            _PlayerListState(players: foundPlayers, isLoading: false, hasSearched: true))
        .startWith(_PlayerListState(
          isLoading: true,
        )));
  }

  void reset() {
    _lastSearchTerm = null;
    _sentRequests.add(Observable.just(_PlayerListState(
        players: _playersService.getRecentOpponents(), isLoading: false, hasSearched: false)));
  }
}

class _NewGamePageState extends State<NewGamePage> with GameStarter {
  final LocalPlayersService _localPlayersService = getService<LocalPlayersService>();
  TextEditingController _searchInputCtrl;
  PublishSubject<String> _searchInput = PublishSubject();
  FocusNode _searchInputFocusNode = FocusNode();
  _PlayerSearcher _playerSearcher;

  _NewGamePageState() {
    // Convert to observable.
    _searchInputCtrl = TextEditingController()
      ..addListener(() => _searchInput.add(_searchInputCtrl.text));

    _playerSearcher = _PlayerSearcher(_localPlayersService);
  }

  @override
  void initState() {
    _searchInput
        .where((s) => s != null && s.length > 2)
        .debounceTime(Duration(milliseconds: 750))
        .listen((s) => _performUserSearch(s));
    super.initState();
  }

  @override
  void dispose() {
    _searchInputCtrl.dispose();
    _searchInputFocusNode.dispose();
    super.dispose();
  }

  void _performUserSearch(String searchTerm) async {
    if (searchTerm == null || searchTerm.length == 0) {
      return;
    }
    _playerSearcher.search(searchTerm);
  }

  void _cancelSearch() {
    if (_searchInputCtrl.text.isNotEmpty) {
      _searchInputCtrl.text = "";
    }
    _playerSearcher.reset();
  }

//  Future _startGameWithRandom() async {
//    final randomPlayer = await _loadingTracker.track(_localPlayersService.findRandomPlayer());
//    await startGame(randomPlayer);
//  }

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return BasePage(
        title: loc.opponentselection_title,
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: <Widget>[
              // This feature is currently disabled:
//              SSSButton(
//                  child: Text(loc.opponentselection_randomplayer),
//                  onPressed: () {
//                    _startGameWithRandom();
//                  }),
//              Text(loc.opponentselection_or),
              StreamBuilder<String>(
                  stream: _searchInput,
                  builder: (context, snapshot) {
                    return TextField(
                      autocorrect: false,
                      autofocus: true,
                      controller: _searchInputCtrl,
                      focusNode: _searchInputFocusNode,
                      decoration: InputDecoration(
                        hintText: loc.opponentselection_searchplayerplaceholder,
                        icon: InkWell(
                          child: Icon(snapshot.hasData && snapshot.data.length > 0
                              ? Icons.close
                              : Icons.search),
                          onTap: () {
                            _cancelSearch();
                          },
                        ),
                      ),
                      textInputAction: TextInputAction.search,
                      onSubmitted: (text) {
                        _performUserSearch(text);
                        _searchInputFocusNode.unfocus();
                      },
                    );
                  }),
              Expanded(
                child: StreamBuilder<_PlayerListState>(
                  stream: _playerSearcher.states,
                  initialData: _PlayerListState(players: _localPlayersService.getRecentOpponents()),
                  builder: (ctx, snapshot) {
                    if (!snapshot.hasData) {
                      return Container();
                    } else {
                      return _createPlayerList(loc, snapshot.data);
                    }
                  },
                ),
              ),
            ],
          ),
        ));
  }

  Widget _createPlayerList(SSSLocalizations loc, _PlayerListState state) {
    return Column(
      children: <Widget>[
        if (state.isLoading)
          Container(
            padding: const EdgeInsets.fromLTRB(40, 0, 0, 0),
            height: 4,
            child: LinearProgressIndicator(
              // Be indeterminate.
              value: null,
              backgroundColor: Colors.transparent,
              valueColor: AlwaysStoppedAnimation<Color>(SSSColors.brown[400]),
            ),
          ),
        if (state.players.isEmpty && state.hasSearched)
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(top: 8),
            child: Text(loc.opponentselection_nosearchresults),
          ),
        if (state.players.isNotEmpty)
          Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 8),
                  child: Text(loc.opponentselection_pickplayerhint),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: ListView(
                      children: state.players
                          .map((u) => InkWell(
                                child: Container(
                                  padding: EdgeInsets.only(top: 12, bottom: 12),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom:
                                              BorderSide(width: 0.5, color: SSSColors.brown[400]))),
                                  child: Text(u.username),
                                ),
                                onTap: () {
                                  startGame(u);
                                },
                              ))
                          .toList(growable: false),
                    ),
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }
}
