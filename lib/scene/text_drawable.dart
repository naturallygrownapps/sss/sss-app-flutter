import 'dart:ui';

import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/positioned_drawable.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

class TextDrawable extends PositionedSizedDrawable {
  Color _color = Color.fromARGB(255, 0, 0, 0);
  Color get color => _color;
  set color(Color newValue) {
    if (color == newValue) {
      return;
    }
    _color = newValue;
    _invalidateParagraph();
    requestRedraw();
  }

  String _text = "";
  String get text => _text;
  set text(String value) {
    if (_text == value) {
      return;
    }
    _text = value;
    _invalidateParagraph();
    requestRedraw();
  }

  double _textSize = 20;
  double get textSize => _textSize;
  set textSize(double value) {
    if (_textSize == value) {
      return;
    }
    _textSize = value;
    _invalidateParagraph();
    requestRedraw();
  }

  double _horizontalMargin = 0;
  double get horizontalMargin => _horizontalMargin;
  set horizontalMargin(double value) {
    if (_horizontalMargin == value) {
      return;
    }
    _horizontalMargin = value;
    _invalidateParagraph();
    requestRedraw();
  }

  String _fontFamily = "";
  String get fontFamily => _fontFamily;
  set fontFamily(String value) {
    if (_fontFamily == value) {
      return;
    }
    _fontFamily = value;
    _invalidateParagraph();
    requestRedraw();
  }

  double _lineHeightFactor = 1.0;
  double get lineHeightFactor => _lineHeightFactor;
  set lineHeightFactor(double value) {
    if (_lineHeightFactor == value) {
      return;
    }
    _lineHeightFactor = value;
    _invalidateParagraph();
    requestRedraw();
  }

  Paragraph _paragraph;

  TextDrawable(AnimationContext animationContext, CanvasContext canvasContext, [ String id = null ]) :
      super(animationContext, canvasContext, id) {

  }

  @override
  void drawCore(Canvas canvas) {
    if (_paragraph == null) {
      final fontSize = util.scaleToDevice(this.textSize);
      final builder = ParagraphBuilder(ParagraphStyle(
        fontSize: fontSize,
        fontFamily: fontFamily,
        textAlign: TextAlign.left,
      ));
      builder.pushStyle(TextStyle(
        color: color,
        height: lineHeightFactor
      ));
      builder.addText(this.text);
      _paragraph = builder.build();

      _paragraph.layout(ParagraphConstraints(
        width: canvasContext.canvasRect.width - horizontalMargin * 2,
      ));
      updateDimensions(_paragraph.longestLine, _paragraph.height, scaleToScreen: false);
    }

    final targetRect = getTargetRect();
    canvas.translate(targetRect.left, targetRect.top);
    canvas.scale(scale, scale);
    canvas.drawParagraph(_paragraph, Offset.zero);
  }

  void _invalidateParagraph() {
    if (_paragraph != null) {
      _paragraph = null;
    }
  }
}
