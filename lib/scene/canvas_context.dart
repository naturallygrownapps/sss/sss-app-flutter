import 'dart:ui';

abstract class CanvasContext {
  Rect get canvasRect;
}
