import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/scene/assets.dart';

abstract class Scene {
  void draw(Canvas canvas);
  Future loadAssets();
}

abstract class SceneWithAssets<TAssets extends Assets> implements Scene {
  final TAssets assets;

  SceneWithAssets(this.assets);

  void draw(Canvas canvas) {
    for(final a in assets) {
      a.draw(canvas);
    }
  }

  Future loadAssets() {
    if (assets.isLoaded) {
      return Future.value();
    }

    return assets.load().then((_) {
      for(final a in assets) {
        a.hide(false);
      }
    });
  }

  void dispose() {
    assets.dispose();
  }
}