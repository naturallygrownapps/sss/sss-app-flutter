import 'dart:collection';
import 'dart:typed_data';

import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/drawables.dart';
import 'package:gallery_gambling/theme.dart';

class Assets with IterableMixin<DrawableEntity> {
  final AnimationContext animCtx;
  final CanvasContext canvasCtx;

  final Color _primaryTextColor = SSSColors.brown[500];
  final String PrimaryFont = "Helsinki";

  List<DrawableEntity> _loadedAssets = List();
  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Assets(this.animCtx, this.canvasCtx);

  Future load() async {
    if (_isLoaded) {
      return;
    }
    _isLoaded = true;

    await for(final a in loadAssets()) {
      _loadedAssets.add(a);
    }
  }

  Stream<DrawableEntity> loadAssets() async* {}

  ImageDrawable loadImageFromImage(ImageDrawable copyFrom) {
    return copyFrom.copyWithSameImageResource();
  }

  Future<ImageDrawable> loadImageFromAsset(String imgAsset) async {
    final imgData = await rootBundle.load("assets/images/$imgAsset");
    final imgDataAsUint8 = Uint8List.view(imgData.buffer);
    final img = await decodeImageFromList(imgDataAsUint8);
    return new ImageDrawable(img, animCtx, canvasCtx);
  }

  TextDrawable createTextAsset(String id, [Color color = null]) {
    return TextDrawable(animCtx, canvasCtx, id)
      ..color = color ?? _primaryTextColor
      ..fontFamily = PrimaryFont;
  }

  void dispose() {
    for(final a in _loadedAssets) {
      a.dispose();
    }
    _loadedAssets.clear();
  }

  @override
  Iterator<DrawableEntity> get iterator => _loadedAssets.iterator;
}