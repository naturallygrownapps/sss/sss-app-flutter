import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:provider/provider.dart';

class AnimationContextProvider extends StatefulWidget {
  final Widget _child;

  AnimationContextProvider({child: Widget, key: Key})
    : _child = child;

  @override
  State<StatefulWidget> createState() {
    return _AnimationContextProviderState();
  }

}

class _AnimationContextProviderState extends State<AnimationContextProvider> with TickerProviderStateMixin {
  AnimationContext _animationContext;

  _AnimationContextProviderState() {
    _animationContext = AnimationContext(this);
  }

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: _animationContext,
      child: widget._child,
    );
  }

  @override
  void dispose() {
    _animationContext.dispose();
    super.dispose();
  }

}

class AnimationContext {
  final _AnimationContextProviderState _tickerProvider;
  List<DrawableAnimationController> _activeAnimations = List();
  VoidCallback animationTick;

  AnimationContext(this._tickerProvider);

  void dispose() {
    abortAllAnimations();
  }

  static AnimationContext of(BuildContext context) {
    return Provider.of<AnimationContext>(context);
  }

  DrawableAnimationController _createController({ Duration duration, String id }) {
    return DrawableAnimationController(_tickerProvider, id: id)
      ..duration = duration;
  }

  Future<bool> run(AnimationDescription animDesc, Duration duration) {
    final ctrl = _createController(duration: duration, id: animDesc.id);
    return _runWith(ctrl, animDesc);
  }

  Future<bool> _runWith(DrawableAnimationController ctrl, AnimationDescription animDesc) {
    if (ctrl.id != null) {
      disposeAnimationById(ctrl.id);
    }

    ctrl.add(animDesc);
    _activeAnimations.add(ctrl);

    ctrl.addListener(() {
      final handler = animationTick;
      if (handler != null) {
        handler();
      }
    });
    ctrl.addStatusListener((status) {
      if (status == AnimationStatus.completed ||
        status == AnimationStatus.dismissed) {
        _activeAnimations.remove(ctrl);
      }
    });

    return ctrl
      .forward()
      .orCancel
      .whenComplete(() {
        disposeAnimation(ctrl);
      })
      .then((_) => true)
      .catchError((e) {
        return false;
      }, test: (e) => e is TickerCanceled);
  }

  void disposeAnimation(DrawableAnimationController animation) {
    if (animation.isAnimating) {
      animation.stop(canceled: true);
    }
    if (animationTick != null) {
      animation.removeListener(animationTick);
    }
    animation.dispose();
  }

  bool disposeAnimationById(String id) {
    final anim = findInList(_activeAnimations, (a) => a.id == id);
    if (anim != null) {
      disposeAnimation(anim);
      return true;
    }

    return false;
  }

  void abortAllAnimations() {
    for (var a in _activeAnimations.toList()) {
      disposeAnimation(a);
    }
    _activeAnimations = List();
  }

  void requestRedraw() {
    final handler = animationTick;
    if (handler != null) {
      handler();
    }
  }
}

class DrawableAnimationController extends AnimationController {
  bool _isDisposed = false;
  String id;
  List<Animation> _dependentAnimations = List();

  DrawableAnimationController(TickerProvider tickerProvider, { String id = null })
    : super(vsync: tickerProvider) {
      this.id = id;
  }

  void add(AnimationDescription animDesc) {
    if (animDesc is CompositeAnimationDescription) {
      for(final c in animDesc.children) {
        add(c);
      }
    }
    else if (animDesc is SingleAnimationDescription) {
      _addSingleAnimation(animDesc);
    }
  }

  void _addSingleAnimation(SingleAnimationDescription animDesc) {
    final anim = animDesc.asAnimatable().animate(this);

    final valueHandler = animDesc.onValue;
    if (valueHandler != null) {
      anim.addListener(() {
        final t = this.value;
        final emitValue =
          (animDesc.beginAt == null || t >= animDesc.beginAt) &&
          (animDesc.finishAt == null || t <= animDesc.finishAt);
        if (emitValue) {
          valueHandler(anim.value);
        }
      });
    }

    final finishedHandler = animDesc.onFinished;
    if (finishedHandler != null) {
      anim.addStatusListener((status) {
        if (status == AnimationStatus.completed ||
          status == AnimationStatus.dismissed) {
          finishedHandler();
        }
      });
    }

    _dependentAnimations.add(anim);
  }

  @override
  void dispose() {
    if (_isDisposed) {
      return;
    }
    _isDisposed = true;
    super.dispose();
  }
}

typedef void AnimationValueCallback(double);

abstract class AnimationDescription {
  String id;

  AnimationDescription({this.id});

  Future<bool> runOn(AnimationContext animCtx, int durationInMs) {
    return animCtx.run(this, Duration(milliseconds: durationInMs));
  }
}

class CompositeAnimationDescription extends AnimationDescription {
  List<AnimationDescription> children = List();

  CompositeAnimationDescription({ String id }) : super(id: id);

  void add(AnimationDescription child) {
    children.add(child);
  }
}

class SingleAnimationDescription extends AnimationDescription {
  double from;
  double to;
  Curve curve;
  double beginAt;
  double finishAt;
  AnimationValueCallback onValue;
  VoidCallback onFinished;

  SingleAnimationDescription({
    String id,
    this.from = 0,
    this.to = 1,
    this.beginAt = 0,
    this.finishAt = 1,
    this.curve = null,
    this.onValue = null,
    this.onFinished = null,
  }) : super(id: id) {}

  Animatable<double> asAnimatable() {
    Animatable<double> animatable = Tween<double>(
      begin: from,
      end: to
    );

    if (curve != null) {
      animatable = animatable.chain(CurveTween(
        curve: curve
      ));
    }

    if (beginAt != null || finishAt != null) {
      animatable = animatable.chain(CurveTween(
        curve: Interval(beginAt ?? 0, finishAt ?? 1)
      ));
    }

    return animatable;
  }

}