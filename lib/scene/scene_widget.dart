import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/scene.dart';

typedef Future<ScenePainter> ScenePainterFactory(BuildContext ctx);

class SceneWidget extends StatelessWidget {
  final ScenePainterFactory _painterFactory;
  SceneWidget(this._painterFactory);

  @override
  Widget build(BuildContext context) {
    return AnimationContextProvider(
      child: _SceneWidgetPaint(_painterFactory)
    );
  }
}

class _SceneWidgetPaint extends StatefulWidget {
  final ScenePainterFactory _painterFactory;
  _SceneWidgetPaint(this._painterFactory);

  @override
  _SceneWidgetPaintState createState() => _SceneWidgetPaintState();
}

class _SceneWidgetPaintState extends State<_SceneWidgetPaint> {
  Future<ScenePainter> _scenePainterSource;

  @override
  void didChangeDependencies() {
    _scenePainterSource = widget._painterFactory(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(builder: (ctx, snapshot) {
      if (snapshot.hasData) {
        return ClipRect(
          child: CustomPaint(
            painter: snapshot.data,
            // Use a container to auto size to the available space.
            child: Container(),
            willChange: true,
          ),
        );
      }
      else {
        return Container();
      }
    }, future: _scenePainterSource);
  }
}

abstract class ScenePainter<TScene extends SceneWithAssets> extends CustomPainter with ChangeNotifier implements CanvasContext{
  TScene scene;
  bool _requiresRepaint = true;
  Size lastSize = Size.zero;
  Rect get canvasRect {
    assert(lastSize.width != 0, "Canvas does not have a valid width.");
    assert(lastSize.height != 0, "Canvas does not have a valid height.");
    return Rect.fromLTRB(0, 0, lastSize.width, lastSize.height);
  }

  ScenePainter();

  @override
  void paint(Canvas canvas, Size size) {
    final hadValidSizeBefore = isValidSize(lastSize);
    lastSize = size;
    if (!hadValidSizeBefore && isValidSize(size)) {
      onGotValidSize();
    }
    if (scene == null)
        return;

    scene.draw(canvas);
    _requiresRepaint = false;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return _requiresRepaint;
  }

  void invalidate() {
    if (!_requiresRepaint) {
      _requiresRepaint = true;
      this.notifyListeners();
    }
  }

  @override
  void dispose() {
    super.dispose();
    scene.dispose();
  }

  void onGotValidSize() {}

  static bool isValidSize(Size size) => size != null && !size.isEmpty;
}