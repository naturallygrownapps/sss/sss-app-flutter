import 'dart:typed_data';
import 'dart:ui' as dartUi;

import 'package:flutter/rendering.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/positioned_drawable.dart';

class ImageDrawable extends PositionedSizedDrawable {
  dartUi.Image _img;
  int _copies = 0;

  ImageDrawable(dartUi.Image img, AnimationContext animCtx, CanvasContext canvasCtx, [ String id = null ]) :
      super(animCtx, canvasCtx, id) {
    updateImage(img);
  }

  ImageDrawable.empty(AnimationContext animCtx, CanvasContext canvasCtx, [ String id = null ])
    : super(animCtx, canvasCtx, id) {
  }

  @override
  void drawCore(Canvas canvas) {
    if (_img == null)
      return;

    final targetRect = getTargetRect();

    final paint = Paint();
    paint.colorFilter = ColorFilter.mode(Color.fromRGBO(255, 255, 255, opacity), BlendMode.modulate);
    canvas.drawImageRect(_img,
      Rect.fromLTWH(0, 0, _img.width.toDouble(), _img.height.toDouble()),
      targetRect,
      paint
    );
  }

  void updateImage(dartUi.Image img, { bool scaleToScreen = true }) {
    _img = img;
    updateDimensions(img.width.toDouble(), img.height.toDouble());
  }

  ImageDrawable copyWithSameImageResource() {
    var copy = ImageDrawable(_img, animationContext, canvasContext, id + _copies.toString());
    _copies++;
    return copy;
  }

  @override
  void dispose() {
    super.dispose();
    if (_img != null) {
      _img.dispose();
      _img = null;
    }
  }
}

class ReassignableImageDrawable extends ImageDrawable {
  ReassignableImageDrawable(AnimationContext animCtx, CanvasContext canvasCtx, [ String id = null ])
    : super.empty(animCtx, canvasCtx, id) {
  }

  Future loadImage(Uint8List imgData) async {
    final img = await decodeImageFromList(imgData);
    updateImage(img);
    sizeToFitScreen();
  }
}