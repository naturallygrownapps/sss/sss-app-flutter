import 'dart:ui';

import 'package:gallery_gambling/scene/animation_context.dart';

abstract class Drawable {
  final AnimationContext animationContext;
  bool _isVisible = true;
  bool get isVisible => _isVisible;
  set isVisible(bool value) {
    _isVisible = value;

  }

  Drawable(this.animationContext);

  void draw(Canvas canvas) {
    if (isVisible) {
      canvas.save();
      drawCore(canvas);
      canvas.restore();
    }
  }

  void drawCore(Canvas canvas);

  void requestRedraw() {
    animationContext.requestRedraw();
  }

  void dispose() {
  }
}