import 'dart:math';
import 'dart:ui';

import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/drawable_entity.dart';
import 'package:gallery_gambling/scene/transform_value.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

abstract class PositionedSizedDrawable extends DrawableEntity {
  Point<double> _anchor = Point(0.5, 0.5);
  Size size = Size.zero;

  TransformValue get actualWidth => TransformValue(size.width * scale);
  TransformValue get actualHeight => TransformValue(size.height * scale);
  TransformValue get originalWidth => TransformValue(size.width);
  TransformValue get originalHeight => TransformValue(size.height);

  PositionedSizedDrawable(AnimationContext animCtx, CanvasContext canvasCtx, [ String id = null ]) :
      super(animCtx, canvasCtx, id) {
  }

  Rect getTargetRect() {
    final w = size.width * scale;
    final h = size.height * scale;

    final x = this.x.getAbsoluteValueHorizontal(canvasContext) - w * _anchor.x;
    final y = this.y.getAbsoluteValueVertical(canvasContext) - h * _anchor.y;
    return Rect.fromLTWH(x, y, w, h);
  }

  void updateDimensions(double newWidth, double newHeight, { bool scaleToScreen = true }) {
    size = Size(
      scaleToScreen ? util.scaleToDevice(newWidth) : newWidth,
      scaleToScreen ? util.scaleToDevice(newHeight) : newHeight
    );
  }

  void sizeToFitScreen() {
    if (size.width <= 0 || size.height <= 0) {
      return;
    }

    final canvasRect = canvasContext.canvasRect;
    final maxWidth = canvasRect.width;
    final maxHeight = canvasRect.height;

    final imgWidth = size.width;
    final imgHeight = size.height;

    var scale = maxWidth / imgWidth;
    if (scale * imgHeight > maxHeight) {
      scale = maxHeight / imgHeight;
    }

    size = Size(imgWidth * scale, imgHeight * scale);
  }

  void setAnchor(Point<double> value) {
    // Keep the current position when changing the anchor.
    final w = size.width * scale;
    final h = size.height * scale;

    final curAnchorOffsetX = w * _anchor.x;
    final curAnchorOffsetY = h * _anchor.y;

    final newAnchorOffsetX = w * value.x;
    final newAnchorOffsetY = h * value.y;

    this.x = new TransformValue(this.x.getAbsoluteValueHorizontal(canvasContext) - (curAnchorOffsetX - newAnchorOffsetX));
    this.y = new TransformValue(this.y.getAbsoluteValueVertical(canvasContext) - (curAnchorOffsetY - newAnchorOffsetY));

    _anchor = value;
    requestRedraw();
  }

  void initForDisplay({ double relX = null, double relY = null, Point<double> anchor = null, double scale = null, double opacity = null }) {
    this.isVisible = true;
    this.scale = scale ?? 1.0;
    this.opacity = opacity ?? 1.0;
    if (anchor != null) {
      setAnchor(anchor);
    }
    if (relX != null) {
      setXRelative(relX);
    }
    if (relY != null) {
      setYRelative(relY);
    }
  }
}