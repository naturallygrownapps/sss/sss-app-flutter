import 'package:gallery_gambling/scene/canvas_context.dart';

class TransformValue {
  final double value;
  final bool isRelative;

  const TransformValue(this.value, [ this.isRelative = false]);

  double getRelativeValueHorizontal(CanvasContext ctx) {
    return _getRelativeValue(ctx, false);
  }

  double getRelativeValueVertical(CanvasContext ctx) {
    return _getRelativeValue(ctx, true);
  }

  double _getRelativeValue(CanvasContext ctx, bool isVertical) {
    if (isRelative) {
      return value;
    }
    else {
      var r = ctx.canvasRect;
      if (isVertical)
        return (value - r.top) / r.height;
      else
        return (value - r.left) / r.width;
    }
  }

  double getAbsoluteValueHorizontal(CanvasContext ctx) {
    return _getAbsoluteValue(ctx, false);
  }

  double getAbsoluteValueVertical(CanvasContext ctx) {
    return _getAbsoluteValue(ctx, true);
  }

  double _getAbsoluteValue(CanvasContext ctx, bool isVertical) {
    if (!isRelative) {
      return value;
    }
    else {
      var r = ctx.canvasRect;
      if (isVertical)
        return r.top + value * r.height;
      else
        return r.left + value * r.width;
    }
  }
}
