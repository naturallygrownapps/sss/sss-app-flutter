import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/scene/animation_context.dart';
import 'package:gallery_gambling/scene/canvas_context.dart';
import 'package:gallery_gambling/scene/drawable.dart';
import 'package:gallery_gambling/scene/transform_value.dart';

abstract class DrawableEntity extends Drawable {

  TransformValue _x = TransformValue(0);
  TransformValue get x => _x;
  set x(TransformValue newValue) {
    _x = newValue;
    requestRedraw();
  }

  TransformValue _y = TransformValue(0);
  TransformValue get y => _y;
  set y(TransformValue newValue) {
    _y = newValue;
    requestRedraw();
  }

  double _scale = 1.0;
  double get scale => _scale;
  set scale(double newValue) {
    _scale = newValue;
    requestRedraw();
  }

  double _opacity = 1.0;
  double get opacity => _opacity;
  set opacity(double newValue) {
    _opacity = newValue;
    requestRedraw();
  }

  final CanvasContext canvasContext;

  final Curve _defaultCurve = Curves.easeInOutCubic;
  final Duration _showHideAnimDuration = Duration(milliseconds: 500);
  static int _idCounter = 0;

  String id;
  String _translateAnimId;
  String _scaleAnimId;
  String _opacityAnimId;

  DrawableEntity(AnimationContext animationContext, this.canvasContext, [String id = null])
    : super(animationContext) {
    this.id = id ?? (_idCounter++).toString();

    _translateAnimId = "DrawableEntity_${this.id}_Translate";
    _scaleAnimId = "DrawableEntity_${this.id}_Scale";
    _opacityAnimId = "DrawableEntity_${this.id}_Opacity";
  }

  void setTranslate(double x, double y) {
    this.x = new TransformValue(x);
    this.y = new TransformValue(y);
  }

  void setXRelative(double relX) {
    this.x = new TransformValue(relX, true);
  }

  void setYRelative(double relY) {
    this.y = new TransformValue(relY, true);
  }

  void setTranslateRelative(double relX, double relY) {
    setXRelative(relX);
    setYRelative(relY);
  }

  void show(bool animated) {
    isVisible = true;
    if (animated) {
      final opacityAnim = animateOpacityTo(1.0);
      animationContext.run(opacityAnim, _showHideAnimDuration);
    }
  }

  void hide(bool animated) {
    if (!animated) {
      isVisible = false;
    }
    else {
      final opacityAnim = animateOpacityTo(0, onFinished: () {
        isVisible = false;
      });
      animationContext.run(opacityAnim, _showHideAnimDuration);
    }
  }

  SingleAnimationDescription animateScale(double fromScale, double toScale,
    {
      Curve curve = null,
      VoidCallback onFinished = null
    }) {
    final anim = SingleAnimationDescription(
      id: _scaleAnimId,
      onValue: (v) => scale = v,
      from: fromScale,
      to: toScale,
      curve: curve ?? _defaultCurve,
      onFinished: onFinished
    );
    return anim;
  }

  SingleAnimationDescription animateScaleTo(double newScale,
    {
      Curve curve = null,
      VoidCallback finished = null
    }) {
    return animateScale(scale, newScale, curve: curve, onFinished: finished);
  }

  CompositeAnimationDescription animateTranslationToRelative(double relX, double relY, {Curve curve = null, VoidCallback onFinished = null}) {
    return animateTranslationTo(relX, relY, curve: curve, onFinished: onFinished, relativeValues: true);
  }

  CompositeAnimationDescription animateTranslationTo(double x, double y, { Curve curve = null, VoidCallback onFinished = null, bool relativeValues = false }) {
    final fromX = relativeValues ? this.x.getRelativeValueHorizontal(canvasContext) : this.x.getAbsoluteValueHorizontal(canvasContext);
    final fromY = relativeValues ? this.y.getRelativeValueVertical(canvasContext) : this.y.getAbsoluteValueVertical(canvasContext);

    final xAnim = SingleAnimationDescription(
      onValue: (v) => this.x = TransformValue(v, relativeValues),
      from: fromX,
      to: x,
      curve: curve ?? _defaultCurve,
    );
    final yAnim = SingleAnimationDescription(
      onValue: (v) => this.y = TransformValue(v, relativeValues),
      from: fromY,
      to: y,
      curve: curve ?? _defaultCurve,
      onFinished: onFinished
    );

    final composite = CompositeAnimationDescription(id: _translateAnimId);
    composite.add(xAnim);
    composite.add(yAnim);
    return composite;
  }

  SingleAnimationDescription animateOpacityTo(double opacity, { VoidCallback onFinished = null, Curve curve = Curves.linear }) {
    final anim = SingleAnimationDescription(
      id: _opacityAnimId,
      onValue: (v) => this.opacity = v,
      from: this.opacity,
      to: opacity,
      curve: curve,
      onFinished: onFinished
    );
    return anim;
  }

  void initForDisplay({ double relX = null, double relY = null, double scale = null, double opacity = null }) {
    this.isVisible = true;
    this.scale = scale ?? 1.0;
    this.opacity = opacity ?? 1.0;
    if (relX != null) {
      this.setXRelative(relX);
    }
    if (relY != null) {
      this.setYRelative(relY);
    }
  }
}