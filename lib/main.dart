import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gallery_gambling/auth_handling.dart' as auth_handling;
import 'package:gallery_gambling/error_handling.dart' as error_handling;
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/about_page.dart';
import 'package:gallery_gambling/pages/licenses_page.dart';
import 'package:gallery_gambling/pages/loggedin/account_settings_page.dart';
import 'package:gallery_gambling/pages/loggedin/games_overview_page.dart';
import 'package:gallery_gambling/pages/loggedin/newgame_page.dart';
import 'package:gallery_gambling/pages/start/intro/intro_page.dart';
import 'package:gallery_gambling/pages/start/login_page.dart';
import 'package:gallery_gambling/pages/start/register_page.dart';
import 'package:gallery_gambling/pages/test_page.dart';
import 'package:gallery_gambling/route_aware_widget.dart' as routeAwareUtils;
import 'package:gallery_gambling/routes.dart' as routes;
import 'package:gallery_gambling/services/local_notifications.dart';
import 'package:gallery_gambling/services/services.dart';
import 'package:gallery_gambling/services/session.dart';
import 'package:gallery_gambling/settings.dart';
import 'package:gallery_gambling/theme.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

final bool _runTests = false;
bool _isFirstRun = false;

void main() async {
  await error_handling.runInErrorHandlingZone(() async {
    await error_handling.setupErrorHandling();

    await SystemChrome.setEnabledSystemUIOverlays([]);
    KeyboardVisibilityNotification().addNewListener(onHide: () {
      SystemChrome.restoreSystemUIOverlays();
    });
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    await SSSServices.setup();
    await auth_handling.setupAuthHandling();

    final settings = getService<Settings>();
    if (!settings.appHasRunBefore) {
      settings.appHasRunBefore = true;
      _isFirstRun = true;
    }
    runApp(SSSApp());
  });
}

class SSSApp extends StatefulWidget {
  @override
  _SSSAppState createState() => _SSSAppState();
}

class _SSSAppState extends State<SSSApp> with WidgetsBindingObserver {
  @override
  void initState() {
    // Always disable reminders when we're starting.
    getService<LocalNotificationsService>().disableGameReminderNotification();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      // If we're logged in, enable the reminders when pausing.
      if (getService<Session>().isLoggedIn) {
        getService<LocalNotificationsService>().enableGameReminderNotification();
      }
    } else if (state == AppLifecycleState.resumed) {
      // Always disable reminders when we're active.
      getService<LocalNotificationsService>().disableGameReminderNotification();
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    var session = getService<Session>();
    return MaterialApp(
      navigatorKey: util.navigatorKey,
      // Cannot use the localizations here as they were not initialized yet and
      // they are only initialized for the context inside the MaterialApp, not for
      // the context we get passed here.
      title: 'Gallery Gambling',
      theme: getTheme(),
      color: getTheme().accentColor,
      initialRoute: _runTests ? routes.dev_tests : "/",
      routes: {
        "/": (builder) =>
            session.isLoggedIn ? GamesOverviewPage() : LoginPage(startWithIntro: _isFirstRun),
        routes.dev_tests: (builder) => TestPage(),
        routes.about: (builder) => AboutPage(),
        routes.register: (builder) => RegisterPage(),
        routes.intro: (builder) => IntroPage(),
        routes.newGame: (builder) => NewGamePage(),
        routes.accountSettings: (builder) => AccountSettingsPage(),
        routes.licenses: (builder) => LicensesPage(),
      },
      localizationsDelegates: [
        const SSSLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('de', 'DE'),
      ],
      navigatorObservers: [routeAwareUtils.routeObserver],
    );
  }
}
