import 'dart:convert';
import 'dart:io';

import 'package:gallery_gambling/serialization.dart';
import 'package:gallery_gambling/util_functions.dart';
import 'package:path_provider/path_provider.dart';

class PersistedCollection<T> {
  final String _collectionName;
  final JsonMapSerializer<T> _serializer;
  final JsonDeserializer<T> _deserializer;
  File _storageFile;
  List<T> _items;

  PersistedCollection(this._collectionName, [ this._serializer, this._deserializer ]) {
    _items = new List<T>();
  }

  Future init() async {
    final appDataDir = await getApplicationDocumentsDirectory();
    final userDataDir = new Directory("${appDataDir.path}/userdata");
    await userDataDir.create(recursive: true);
    _storageFile = new File('${userDataDir.path}/$_collectionName.json');
    await load();
  }

  Future load() async {
    if (await _storageFile.exists()) {
      final storedData = await _storageFile.readAsString();
      final asJsonList = jsonDecode(storedData);
      _items = new List<T>();
      for(var i in asJsonList) {
        _items.add(_deserializer != null ? _deserializer(i) : i);
      }
    }
    else {
      _items = new List<T>();
    }
  }

  Future save() async {
    var asJsonList = jsonEncode(_items, toEncodable: (o) {
      return _serializer != null ? _serializer(o) : o;
    });
    return _storageFile.writeAsString(asJsonList, flush: true);
  }

  Future add(T item) {
    _items.add(item);
    return save();
  }

  T get(bool Function(T item) test) {
    return findInList(_items, test);
  }

  List<T> getAll() {
    return _items.toList();
  }

  Future replaceWithItem(T oldItem, T newItem) {
    _items.remove(oldItem);
    _items.add(newItem);
    return save();
  }

  Future replace(bool Function(T item) itemSelector, T newItem) {
    _items.removeWhere(itemSelector);
    _items.add(newItem);
    return save();
  }

  Future replaceAll(Iterable<T> newItems) {
    _items = newItems.toList();
    return save();
  }

  Future remove(bool Function(T item) itemSelector) {
    _items.removeWhere(itemSelector);
    return save();
  }

  Future clear() {
    _items = new List<T>();
    return save();
  }

}