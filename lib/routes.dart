const dev_tests = "/_tests";
const about = "/about";
const register = "/register";
const intro = "/intro";
const newGame = "/newgame";
const accountSettings = "/accountsettings";
const licenses = "/licenses";
