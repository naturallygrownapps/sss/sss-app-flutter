import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/pages/document_page.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

_navigateWithDocumentContent(BuildContext context, String docContent, {String title}) {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => DocumentPage(docContent: docContent, title: title)));
}

openDocument(BuildContext context, String documentPath,
    {bool localize = false, String title}) {
  var documentToLoad = documentPath;
  var fallbackDocument = documentPath;

  if (localize) {
    var currentLocale = Localizations.localeOf(context);
    var currentLanguage = currentLocale.languageCode;

    var dotIdx = documentPath.lastIndexOf(".");
    var extension = "";
    var v = documentPath;
    if (dotIdx != -1) {
      extension = documentPath.substring(dotIdx);
      v = documentPath.substring(0, dotIdx);
    }

    documentToLoad = v + "_" + currentLanguage + extension;
    fallbackDocument = v + "_de" + extension;
    if (!SSSLocalizations.isSupportedLanguage(currentLocale)) {
      // Use german language as a fallback. We might not have all documents available and want to show something anyway.
      documentToLoad = fallbackDocument;
    }
  }

  return rootBundle
      .loadString("assets/docs/${documentToLoad}")
      .then((docContent) => _navigateWithDocumentContent(context, docContent, title: title))
      .catchError((e) {
    if (documentToLoad != fallbackDocument) {
      return rootBundle.loadString("assets/docs/${fallbackDocument}").then(
          (docContent) => _navigateWithDocumentContent(context, docContent, title: title));
    } else {
      throw e;
    }
  });
}

bool _isDebugMode = null;
bool isDebugMode() {
  if (_isDebugMode == null) {
    assert(_isDebugMode = true);
  }
  return _isDebugMode ?? false;
}

isNotNullOrEmpty(String s) {
  return s != null && s.isNotEmpty;
}

T findInList<T>(List<T> list, bool Function(T item) test) =>
  list.firstWhere(test, orElse: () => null);

final navigatorKey = new GlobalKey<NavigatorState>();
BuildContext getMainContext() => navigatorKey.currentState?.overlay?.context;

const defaultScreenWidth = 720.0;

double scaleToDevice(double size) {
  final mainContext = getMainContext();
  final screenSize = MediaQuery.of(mainContext).size;
  return size * (screenSize.width / defaultScreenWidth);
}

Future deleteUserRelatedFiles() async {
  final appDataDir = await getApplicationDocumentsDirectory();
  final userDataDir = new Directory("${appDataDir.path}/userdata");
  if (await userDataDir.exists()) {
    await userDataDir.delete(recursive: true);
    // We must restore an empty userdata dir as it is only created on appstart
    // and would be missing for the rest of the app runtime otherwise.
    await userDataDir.create(recursive: true);
  }
}

Future<bool> waitBool(Iterable<Future<bool>> futures) {
  return Future.wait(futures).then((results) => results.every((b) => b));
}

SSSLocalizations getLocalizations() {
  final ctx = getMainContext();
  if (ctx == null) {
    return null;
  }

  return SSSLocalizations.of(ctx);
}