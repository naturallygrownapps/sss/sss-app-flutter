import 'dart:async';

import 'package:flutter/cupertino.dart';

abstract class Progress<T> {
  void report(T value);
  void dispose();
}

class DoubleValueReportingProgress implements Progress<double> {
  StreamController<double> _valueController = StreamController.broadcast();
  Stream<double> get value => _valueController.stream;
  VoidCallback onDisposed;

  DoubleValueReportingProgress(this.onDisposed);

  @override
  void report(double value) {
    if (_valueController.isClosed) {
      return;
    }

    _valueController.add(value);
    if (value >= 1) {
      _valueController.close();
    }
  }

  void dispose() {
    _valueController.close();
    if (onDisposed != null) {
      onDisposed();
      onDisposed = null;
    }
  }
}