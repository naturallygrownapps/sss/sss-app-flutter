import 'package:gallery_gambling/game/player.dart';
import 'package:gallery_gambling/shared/dto/game_participant_dto.dart';
import 'package:gallery_gambling/shared/dto/round_dto.dart';
import 'package:gallery_gambling/shared/game_status.dart';
import 'package:gallery_gambling/shared/move.dart';

class GameScores {
  final int yourScore;
  final int otherPlayerScore;

  GameScores(this.yourScore, this.otherPlayerScore);
}

RoundDto getActiveRound(GameStatus gameStatus)  {
  if (gameStatus.rounds != null && gameStatus.rounds.isNotEmpty) {
    return gameStatus.rounds.last;
  }
  else {
    return null;
  }
}

RoundDto getPreviousRound(GameStatus gameStatus) {
  if (gameStatus.rounds != null && gameStatus.rounds.isNotEmpty) {
    // When the game is finished, we return the last round.
    if (isGameFinished(gameStatus)) {
      return gameStatus.rounds.last;
    }
    // Otherwise, we return the finished round before the current active round.
    else if (gameStatus.rounds.length > 1) {
      return gameStatus.rounds[gameStatus.rounds.length - 2];
    }
  }

  return null;
}

bool hasStarted(GameStatus gameStatus) {
  return gameStatus.rounds.isNotEmpty;
}

bool isGameFinished(GameStatus gameStatus) {
  return gameStatus.state == GameState.YouWon ||
         gameStatus.state == GameState.YouLost;
}

bool hasChanges(GameStatus gameStatus, GameStatus other)  {
  if (other == null) {
    return true;
  }

  return roundsDiffer(gameStatus.rounds, other.rounds) ||
    roundWinnersDiffer(gameStatus.roundWinners, other.roundWinners) ||
    gameStatus.state != other.state;
}

bool roundWinnersDiffer(List<String> roundWinners1, List<String> roundWinners2) {
  if (roundWinners1.length != roundWinners2.length) {
    return true;
  }

  for (int i = 0; i < roundWinners1.length; i++)
  {
    if (roundWinners1[i] != roundWinners2[i]) {
      return true;
    }
  }

  return false;
}

bool roundsDiffer(List<RoundDto> rounds1, List<RoundDto> rounds2)  {
  if (rounds1.length != rounds2.length) {
    return true;
  }

  for (int i = 0; i < rounds1.length; i++)  {
    final r1 = rounds1[i];
    final r2 = rounds2[i];
    if (r1.yourMove != r2.yourMove ||
        r1.otherPlayerMove != r2.otherPlayerMove) {
      return true;
    }
  }

  return false;
}

Move getMove(RoundDto round, Player player)  {
  return player == Player.You ? round.yourMove : round.otherPlayerMove;
}

bool isRoundFinished(RoundDto round)  {
  return round.yourMove != null && round.otherPlayerMove != null;
}

GameScores getScores(GameStatus gameStatus) {
  var wins = 0;
  var losses = 0;
  if (gameStatus.roundWinners != null)  {
    for (final winner in gameStatus.roundWinners)  {
      if (winner == GameParticipantDto.Nobody.username) {
        // Draw, no points.
      }
      else if (winner != gameStatus.otherPlayer.username) {
        wins++;
      }
      else {
        losses++;
      }
    }
  }

  return GameScores(wins, losses);
}