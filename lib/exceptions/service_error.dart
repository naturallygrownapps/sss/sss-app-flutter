import 'package:json_annotation/json_annotation.dart';

part 'service_error.g.dart';

@JsonSerializable()
class ServiceError {
  String errorMessage;
  String fullException;
  String errorCode;

  ServiceError() {}

  bool get isEmpty => 
    (errorMessage == null || errorMessage.length == 0) &&
    (fullException == null || fullException.length == 0) &&
    (errorCode == null || errorCode.length == 0);
    
  static ServiceError fromJson(Map<String, dynamic> json) => _$ServiceErrorFromJson(json);
  Map<String, dynamic> toJson() => _$ServiceErrorToJson(this);
}