class UserFacingException implements Exception {
  String message;
  Exception inner;

  UserFacingException(this.message, [this.inner]);

  @override
  String toString() => message;
}
