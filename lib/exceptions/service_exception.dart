import 'package:gallery_gambling/exceptions/service_error.dart';
import 'package:gallery_gambling/localization.dart';

class ServiceException implements Exception {
  ServiceError error;
  int statusCode;

  ServiceException(this.error, this.statusCode) {}

  @override
  String toString() {
    var result = "HTTP-Status ${statusCode}";
    if (error != null) {
      if (error.errorCode != null) {
        result += "\n${error.errorCode}";
      }
      if (error.errorMessage != null) {
        if (error.errorCode != null) {
          result += ": ${error.errorMessage}";
        }
        else {
          result += "\n${error.errorMessage}";
        }
      }
      if (error.fullException != null) {
        result += "\n${error.fullException}";
      }
    }
    return result;
  }

  String GetUserFriendlyMessage(SSSLocalizations localizations) {
    if (error != null) {
      // Try to translate ErrorCode.
      if (error.errorCode != null && error.errorCode.length > 0) {
        var translationResource = "service_error_" + error.errorCode;
        var translation = localizations.getLocalizationFor(translationResource);
        if (translation != null) {
          return translation;
        }
      }

      // Or use ErrorMessage (which is in english).
      if (error.errorMessage != null && error.errorMessage.length > 0) {
        return error.errorMessage;
      }
    }

    // fallback
    return this.toString();
  }
}
