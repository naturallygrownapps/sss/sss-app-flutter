import 'package:gallery_gambling/exceptions/exceptions.dart';

class GameException implements Exception {
  String message;
  Exception inner;

  GameException(this.message, [this.inner]);

  @override
  String toString() => message;
}

class NoBetAvailableException extends UserFacingException {

  NoBetAvailableException(String message, [Exception inner]) : super(message, inner);

}