enum AuthenticationFailReason {
  Failed,
  AutomaticReloginFailed
}

class AuthenticationException implements Exception {
  String message;
  AuthenticationFailReason reason;

  AuthenticationException({this.message, this.reason = AuthenticationFailReason.Failed}) {

  }
}