// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServiceError _$ServiceErrorFromJson(Map<String, dynamic> json) {
  return ServiceError()
    ..errorMessage = json['errorMessage'] as String
    ..fullException = json['fullException'] as String
    ..errorCode = json['errorCode'] as String;
}

Map<String, dynamic> _$ServiceErrorToJson(ServiceError instance) =>
    <String, dynamic>{
      'errorMessage': instance.errorMessage,
      'fullException': instance.fullException,
      'errorCode': instance.errorCode
    };
