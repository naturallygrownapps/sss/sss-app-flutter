import 'package:flutter/material.dart';

class SSSColors {
  static const Map<int, Color> beige = const <int, Color>{
    50: const Color(0xfffefefb),
    100: const Color(0xfffefbf5),
    200: const Color(0xfffdf9ee),
    300: const Color(0xfffcf7e7),
    400: const Color(0xfffbf5e2),
    500: const Color(0xfffaf3dd),
    600: const Color(0xfff9f1d9),
    700: const Color(0xfff9efd4),
    800: const Color(0xfff8edcf),
    900: const Color(0xfff6eac7)
  };

  static const Map<int, Color> brown = const <int, Color>{
    50: const Color(0xffedecea),
    100: const Color(0xffd3cfcc),
    200: const Color(0xffb5b0aa),
    300: const Color(0xff979087),
    400: const Color(0xff81786e),
    500: const Color(0xff6b6054),
    600: const Color(0xff63584d),
    700: const Color(0xff584e43),
    800: const Color(0xff4e443a),
    900: const Color(0xff3c3329)
  };

  static final Color cyan = Color(0xFF43D0D0);
  static final Color green2 = Color(0xFF88B882);
}

ThemeData theme;

getTheme() {
  if (true) {
    theme = new ThemeData(
        primarySwatch: MaterialColor(SSSColors.brown[500].value, SSSColors.brown),
        accentColor: SSSColors.beige[500],
        fontFamily: "Cabin");

    var newTextTheme =
        theme.textTheme.apply(bodyColor: SSSColors.brown[500], displayColor: SSSColors.brown[500]);
    newTextTheme = newTextTheme.copyWith(
      headline6: newTextTheme.headline6.apply(fontFamily: "Helsinki"),
      caption: newTextTheme.caption.copyWith(
        fontSize: 12,
        color: SSSColors.green2,
      ),
      bodyText1: newTextTheme.bodyText1.copyWith(fontSize: 12),
    );

    var iconTheme = IconThemeData(color: SSSColors.brown[500]);

    var buttonTheme = theme.buttonTheme
        .copyWith(buttonColor: theme.accentColor, textTheme: ButtonTextTheme.primary);

    theme = theme.copyWith(
      textTheme: newTextTheme,
      primaryTextTheme: newTextTheme,
      iconTheme: iconTheme,
      primaryIconTheme: iconTheme,
      buttonTheme: buttonTheme,
    );
  }
  return theme;
}
