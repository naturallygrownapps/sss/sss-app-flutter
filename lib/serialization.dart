import 'dart:convert';
import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';

typedef T JsonDeserializer<T>(Map<String, dynamic> json);
typedef Map<String, dynamic> JsonMapSerializer<T>(T obj);
typedef String JsonSerializer<T>(T obj);

String uint8ListToJsonString(Uint8List list) {
  return base64.encode(list);
}

Uint8List uint8listFromJsonString(String json) {
  return base64.decode(json);
}

class Base64JsonConverter implements JsonConverter<Uint8List, String> {
  const Base64JsonConverter();

  @override
  Uint8List fromJson(String json) => json == null ? null : base64.decode(json);

  @override
  String toJson(Uint8List list) => list == null ? "" : base64.encode(list);
}
