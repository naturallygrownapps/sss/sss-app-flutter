import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/theme.dart';

class DrawerListTile extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  DrawerListTile({ this.text, this.onPressed });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: this.onPressed,
        child: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(8.0),
          height: 40,
          decoration: BoxDecoration(
            color: SSSColors.beige[800],
          ),
          child: Text(
            this.text,
          ),
        ),
      ),
    );
  }
}