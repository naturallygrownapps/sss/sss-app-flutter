import 'package:flutter/material.dart';
import 'package:gallery_gambling/progress.dart';
import 'package:gallery_gambling/services/loading_tracker.dart';
import 'package:gallery_gambling/services/services.dart';

class SpinnerContainer extends StatefulWidget {
  final Widget child;

  SpinnerContainer({this.child});

  @override
  SpinnerContainerState createState() {
    return new SpinnerContainerState();
  }
}

class SpinnerContainerState extends State<SpinnerContainer> {
  LoadingTracker loadingTracker;

  @override
  void initState() {
    loadingTracker = getService<LoadingTracker>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      widget.child,
      StreamBuilder(
        stream: loadingTracker.loadingState,
        builder: (ctx, AsyncSnapshot<LoadingState> snapshot) {
          if (snapshot.hasData && snapshot.data != LoadingState.NotLoading) {
            return Stack(children: <Widget>[
              Container(
                  color:
                      Color.fromARGB(snapshot.data == LoadingState.BlockUiOnly ? 0 : 25, 0, 0, 0)),
              if (snapshot.data == LoadingState.Spinner)
                Center(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor.withOpacity(0.8),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        StreamBuilder(
                          stream: loadingTracker.progress,
                          initialData: null,
                          builder: (ctx, AsyncSnapshot<DoubleValueReportingProgress> snapshot) {
                            if (snapshot.hasData &&
                                snapshot.data != null &&
                                snapshot.connectionState == ConnectionState.active) {
                              return StreamBuilder(
                                  stream: snapshot.data.value,
                                  initialData: 0.0,
                                  builder: (ctx, AsyncSnapshot<double> snapshot) {
                                    return CircularProgressIndicator(
                                      value: snapshot.data ?? 0,
                                      valueColor: new AlwaysStoppedAnimation<Color>(
                                          Theme.of(context).primaryColor),
                                    );
                                  });
                            } else {
                              return CircularProgressIndicator(
                                value: null,
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Theme.of(context).primaryColor),
                              );
                            }
                          },
                        ),
                        StreamBuilder(
                            stream: loadingTracker.busyMessage,
                            builder: (ctx, AsyncSnapshot<String> snapshot) {
                              if (snapshot.hasData) {
                                return Container(
                                    padding: EdgeInsets.only(top: 8),
                                    child: Text(
                                      snapshot.hasData ? snapshot.data : "",
                                      style: Theme.of(ctx).textTheme.bodyText2,
                                    ));
                              } else {
                                return Container(width: 0, height: 0);
                              }
                            })
                      ],
                    ),
                  ),
                ),
            ]);
          } else {
            return Container(width: 0, height: 0);
          }
        },
      )
    ]);
  }
}
