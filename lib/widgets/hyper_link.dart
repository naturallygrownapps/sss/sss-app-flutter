import 'package:flutter/material.dart';
import 'package:gallery_gambling/theme.dart';
import 'package:url_launcher/url_launcher.dart';

class HyperLink extends StatelessWidget {
  final String linkText;
  final String url;
  final VoidCallback onTap;
  final double fontSize;
  final bool underline;

  HyperLink({
    this.linkText,
    this.url,
    this.onTap,
    this.fontSize,
    this.underline = true,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: new Text(this.linkText,
        style: TextStyle(
          decoration: underline ? TextDecoration.underline : TextDecoration.none,
          color: SSSColors.cyan,
          fontSize: this.fontSize,
        ),
      ),
      onTap: this.onTap ??
          () async {
          var url = this.url ?? this.linkText;
          if (await canLaunch(url)) {
            await launch(url);
          }
        });
  }
}

