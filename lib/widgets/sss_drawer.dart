import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_gambling/localization.dart';
import 'package:gallery_gambling/widgets/version_label.dart';

class SSSDrawer extends StatelessWidget {
  final Widget child;

  const SSSDrawer({
    this.child,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loc = SSSLocalizations.of(context);
    return SizedBox(
      width: 250,
      child: Drawer(
        child: Container(
          color: Theme.of(context).accentColor,
          child: Stack(children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  height: 80,
                  child: DrawerHeader(
                    padding: EdgeInsets.all(8),
                    child: Stack(fit: StackFit.passthrough, children: [
                      Container(
                          child: Text(loc.start_menu_title,
                              style: Theme.of(context).primaryTextTheme.headline6),
                          alignment: Alignment.centerLeft),
                    ]),
                  ),
                ),
                Expanded(child: this.child),
              ],
            ),
            Container(
              child: VersionLabel(),
              alignment: Alignment.bottomRight,
              padding: EdgeInsets.only(right: 8, bottom: 8),
            )
          ]),
        ),
      ),
    );
  }
}
