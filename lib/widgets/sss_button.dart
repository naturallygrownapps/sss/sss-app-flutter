
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SSSButton extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  SSSButton({this.child, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      child: child,
      textColor: Theme.of(context).primaryColor,
    );
  }
}