import 'package:package_info/package_info.dart';

import 'package:flutter/material.dart';

class VersionLabel extends StatefulWidget {
  VersionLabel()
  {
  }

  @override
  _VersionLabelState createState() => _VersionLabelState();
}

class _VersionLabelState extends State<VersionLabel> {

  String versionString = "";

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((pkg) =>
      setState(()
      {
        this.versionString = "v${pkg.version} (${pkg.buildNumber})";
      })
    );
  }

  @override
  Widget build(BuildContext context) {
    return Text(this.versionString);
  }


}