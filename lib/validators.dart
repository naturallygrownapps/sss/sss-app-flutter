import 'package:flutter/widgets.dart';

notEmpty(String errorMessage) {
  return (value) {
    if (value == null || value.isEmpty) {
      return errorMessage;
    }
    return null;
  };
}

minChars(int minCharCount, String errorMessage) {
  return (String value) {
    if (value == null || value.length < minCharCount) {
      return errorMessage;
    }
    return null;
  };
}

var _usernameRegex = new RegExp(r"^[a-zA-Zäöüß0-9]{3,25}$");
usernameChars(String errorMessage) {
  return (String value) {
    if (!_usernameRegex.hasMatch(value)) {
      return errorMessage;
    }
    return null;
  };
}

concat(List<FormFieldValidator<String>> validators) {
  return (String value) {
    for (var v in validators) {
      var validated = v(value);
      if (validated != null) {
        return validated;
      }
    }
    return null;
  };
}
