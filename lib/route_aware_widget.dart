import 'package:flutter/widgets.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

typedef void RouteCallback();

class RouteAwareWidget extends StatefulWidget {
  final Widget child;
  final RouteCallback didPopNext;

  const RouteAwareWidget({Key key, this.child, this.didPopNext}) : super(key: key);

  State<RouteAwareWidget> createState() => RouteAwareWidgetState();
}

class RouteAwareWidgetState extends State<RouteAwareWidget> with RouteAware {

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
    if (widget.didPopNext != null) {
      widget.didPopNext();
    }
  }

  @override
  Widget build(BuildContext context) => widget.child;

}