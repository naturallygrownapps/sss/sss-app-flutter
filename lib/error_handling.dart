import 'dart:async';
import 'dart:developer' as dev;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:gallery_gambling/exceptions/authentication_exception.dart';
import 'package:gallery_gambling/exceptions/exceptions.dart';
import 'package:gallery_gambling/exceptions/intended_cancellation_exception.dart';
import 'package:gallery_gambling/util_functions.dart' as util;

typedef ZoneStartDelegate = Future Function();

Object _lastError;

Future setupErrorHandling() async {
  // HACK: Re-enable our Flutter.onError handler as it gets removed by a debug tool in the current
  // Flutter version (see e.g. https://github.com/flutter/flutter/issues/44407).
  if (util.isDebugMode()) {
    Timer.periodic(new Duration(seconds: 1), (t) {
      if (FlutterError.onError != _flutterErrorHandler) {
        FlutterError.onError = _flutterErrorHandler;
      }
    });
  }
  FlutterError.onError = _flutterErrorHandler;

  await FlutterCrashlytics().initialize();
}

void _flutterErrorHandler(FlutterErrorDetails details) {
  if (util.isDebugMode()) {
    // In development mode, simply print to console.
    FlutterError.dumpErrorToConsole(details);
  } else {
    // In production mode, handle error.
    _handleError(details.exception, details.stack);
  }
}

Future runInErrorHandlingZone(ZoneStartDelegate startDelegate) {
  return runZoned(() async {
    WidgetsFlutterBinding.ensureInitialized();
    try {
      await startDelegate();
    } catch (e, stackTrace) {
      if (util.isDebugMode()) {
        FlutterError.dumpErrorToConsole(FlutterErrorDetails(exception: e, stack: stackTrace));
      } else {
        await FlutterCrashlytics().reportCrash(e, stackTrace, forceCrash: true);
      }
    }
  }, onError: (Object error, StackTrace stackTrace) {
    _handleError(error, stackTrace);
  });
}

void _handleError(dynamic error, StackTrace stackTrace) async {
  if (error is IntendedCancellationException) {
    return;
  }

  if (error is AuthenticationException &&
      error.reason == AuthenticationFailReason.AutomaticReloginFailed) {
    // No need to bother anyone with this error.
    return;
  }

  // We sometimes get the same error twice. No idea why this is, but
  // we don't want errors to be reported twice.
  if (_lastError == error) {
    return;
  }

  _lastError = error;

  if (stackTrace == null && (error as dynamic).stackTrace is StackTrace) {
    stackTrace = (error as dynamic).stackTrace;
  }

  dev.log("Unhandled error.", error: error, stackTrace: stackTrace);

  bool reportError = true;
  if (util.isDebugMode()) {
    reportError = false;
  }
  if (error is UserFacingException) {
    reportError = false;
  }
  if (error is AuthenticationException) {
    // Do not report failed login attempts.
    reportError = false;
  }
  if (error is SocketException) {
    // Do not report network issues on the client side.
    final loc = util.getLocalizations();
    if (loc != null) {
      error = new UserFacingException(loc.error_connectivity);
    }
    reportError = false;
  }

  showErrorDialog(error);
  if (reportError) {
    // Reports are batched and sent on the next app start.
    // We just handle errors here, by setting the forceCrash flag,
    // they can be converted to real crashes.
    await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: false);
  }
}

void showErrorDialog(Object error) {
  if (error != null) {
    var ctx = util.getMainContext();
    if (ctx != null) {
      try {
        showDialog(
            context: ctx,
            builder: (ctx) {
              String errorMessage;
              if (error is ServiceException) {
                errorMessage = error.error?.errorMessage;
              }
              if (errorMessage == null) {
                errorMessage = error.toString();
              }

              return AlertDialog(
                title: Text("Error"),
                content: Text(errorMessage, style: Theme.of(ctx).primaryTextTheme.subtitle2),
                actions: [
                  RaisedButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(ctx);
                    },
                  ),
                ],
              );
            });
      } catch (e) {
        // For some reason, this sometimes fails with "'package:flutter/src/widgets/navigator.dart': Failed assertion: line 1782 pos 12: '!_debugLocked': is not true.".
        debugPrint("Error showing error dialog.");
      }
    }
  }
}

Future reportErrorManually(dynamic error, {StackTrace stackTrace}) async {
  if (stackTrace == null) {
    stackTrace = StackTrace.current;
  }
  if (util.isDebugMode()) {
    // In development mode, simply print to console.
    FlutterError.dumpErrorToConsole(
        FlutterErrorDetails(exception: error, stack: stackTrace, library: 'Gallery Gambling'));
  } else {
    await FlutterCrashlytics().reportCrash(error, stackTrace);
  }
}
